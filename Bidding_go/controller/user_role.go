package controllers

import (
	G "Bidding_go/global"
	"Bidding_go/model"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type CreateUserRoleInput struct {
	UserID uint `json:"user_id" form:"user_id"`
	RoleID uint `json:"role_id" form:"role_id"`
}

type UpdateUserRoleInput struct {
	ID     uint `json:"id" form:"id" gorm:"primary_key"`
	UserID uint `json:"user_id" form:"user_id"`
	RoleID uint `json:"role_id" form:"role_id"`
}

// GET /userroles
// Get all userroles
func ListUserRoles(c *gin.Context) {
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	u, _ := strconv.Atoi(c.Query("user_id"))
	userIDParams := uint(u)
	userID := auth.UserID
	if userIDParams > 0 && userIDParams != auth.UserID {
		if !isPermission(c, auth) {
			return
		}
		userID = userIDParams
	}

	page, _ := strconv.Atoi(c.Query("page"))
	num, _ := strconv.Atoi(c.Query("page_num"))

	sql := fmt.Sprintf(G.LISTUSERROLESQL, userID, (page-1)*num, num)
	userroles, err := model.Model.ListUserRole(sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, userroles)
	c.JSON(status, response)
}

// POST /userroles
// Create new userrole
func CreateUserRole(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	var input CreateUserRoleInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.UserRole
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		fmt.Println(err)
		errors := []string{}
		for key, value := range e {
			errors = append(errors, key+" : "+value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	// Create userrole
	userrole := model.UserRole{
		UserID: input.UserID,
		RoleID: input.RoleID,
	}

	sql := fmt.Sprintf(G.CREATEUSERROLESQL,
		userrole.UserID,
		userrole.RoleID,
	)

	data, err := model.Model.CreateUserRole(&userrole, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, data)
	c.JSON(status, response)
}

// GET /userroles/:id
// Find a userrole
func FindUserRole(c *gin.Context) { // Get model if exist
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.Query("id"))
	if isBadRequestID(c, errID) {
		return
	}

	u, _ := strconv.Atoi(c.Query("user_id"))
	userIDParams := uint(u)
	userID := auth.UserID
	if userIDParams > 0 && userIDParams != auth.UserID {
		if !isPermission(c, auth) {
			return
		}
		userID = userIDParams
	}

	sql1 := fmt.Sprintf(G.FINDUSERROLESQL, i, userID)
	userrole, errF := model.Model.FindUserRole(sql1)
	if isBadRequest(c, errF) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, userrole)
	c.JSON(status, response)
}

// PATCH /userroles/:id
// Update a userrole
func UpdateUserRole(c *gin.Context) {
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	// Get model if exist
	var input UpdateUserRoleInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.UserRole
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		errors := []string{}
		for _, value := range e {
			errors = append(errors, value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	userID := auth.UserID

	userrole := model.UserRole{
		ID:     input.ID,
		UserID: input.UserID,
		RoleID: input.RoleID,
	}

	sql := fmt.Sprintf(G.UPDATEUSERROLESQL,
		userrole.UserID,
		userrole.RoleID,
		userrole.ID,
	)

	data, err := model.Model.UpdateUserRole(userID, &userrole, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, data)
	c.JSON(status, response)
}

// DELETE /userroles/:id
// Delete a userrole
func DeleteUserRole(c *gin.Context) {
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.PostForm("id"))
	if isBadRequestID(c, errID) {
		return
	}

	u, _ := strconv.Atoi(c.PostForm("user_id"))
	userIDParams := uint(u)
	userID := auth.UserID
	if userIDParams > 0 && userIDParams != auth.UserID {
		if !isPermission(c, auth) {
			return
		}
		userID = userIDParams
	}

	sql := fmt.Sprintf(G.DELETEUSERROLESQL, i, userID)
	success, errF := model.Model.DeleteUserRole(userID, uint(i), sql)
	if isBadRequest(c, errF) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, success)
	c.JSON(status, response)
}
