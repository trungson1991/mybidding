package controllers

import (
	G "Bidding_go/global"
	"Bidding_go/model"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type CreateUserFavoriteInput struct {
	UserID    uint `json:"user_id" form:"user_id"`
	ProductID uint `json:"product_id" form:"product_id"`
	Status    int  `json:"status" form:"status"`
}

type UpdateUserFavoriteInput struct {
	ID        uint `json:"id" form:"id" gorm:"primary_key"`
	UserID    uint `json:"user_id" form:"user_id"`
	ProductID uint `json:"product_id" form:"product_id"`
	Status    int  `json:"status" form:"status"`
}

type GETUserFavorite struct {
	ID      uint          `json:"id" form:"id" gorm:"primary_key"`
	User    model.User    `json:"user" form:"user"`
	Product model.Product `json:"product" form:"product"`
	Status  int           `json:"status" form:"status"`
}

// GET /userfavorites
// Get all userfavorites
func ListUserFavorites(c *gin.Context) {
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	u, _ := strconv.Atoi(c.Query("user_id"))
	userIDParams := uint(u)
	userID := auth.UserID
	if userIDParams > 0 && userIDParams != auth.UserID {
		if !isPermission(c, auth) {
			return
		}
		userID = userIDParams
	}

	page, _ := strconv.Atoi(c.Query("page"))
	num, _ := strconv.Atoi(c.Query("page_num"))

	sql := fmt.Sprintf(G.LISTUSERFAVORITESQL, userID, (page-1)*num, num)
	userfavorites, err := model.Model.ListUserFavorite(sql)
	if isServerError(c, err) {
		return
	}

	var getUserfavorites []GETUserFavorite
	for _, item := range userfavorites {
		sql1 := fmt.Sprintf(G.GETUSERBYIDSQL, item.UserID)
		user, err := model.Model.GetUser(sql1)
		if isServerError(c, err) {
			return
		}

		sql2 := fmt.Sprintf(G.FINDPRODUCTSQL, item.ProductID)
		product, err := model.Model.FindProduct(sql2)
		if isServerError(c, err) {
			return
		}

		i := GETUserFavorite{
			ID:      item.ID,
			User:    *user,
			Product: *product,
			Status:  item.Status,
		}

		getUserfavorites = append(getUserfavorites, i)
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, getUserfavorites)
	c.JSON(status, response)
}

// POST /userfavorites
// Create new userfavorite
func CreateUserFavorite(c *gin.Context) {
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	var input CreateUserFavoriteInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.UserFavorite
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		fmt.Println(err)
		errors := []string{}
		for key, value := range e {
			errors = append(errors, key+" : "+value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	userID := auth.UserID
	sql1 := fmt.Sprintf(G.FINDUSERFAVORITEPRODUCRRSQL, input.ProductID, userID)
	_, errF := model.Model.FindUserFavorite(sql1)
	if errF == nil {
		status := http.StatusBadRequest
		response := model.CreateResponse(status, []string{G.RecordFound}, nil)
		c.JSON(status, response)
		return
	}

	// Create userfavorite
	userfavorite := model.UserFavorite{
		UserID:    userID,
		ProductID: input.ProductID,
		Status:    input.Status,
	}

	sql2 := fmt.Sprintf(G.CREATEUSERFAVORITESQL,
		userfavorite.UserID,
		userfavorite.ProductID,
		userfavorite.Status,
	)

	data, err := model.Model.CreateUserFavorite(&userfavorite, sql2)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, data)
	c.JSON(status, response)
}

// GET /userfavorites/:id
// Find a userfavorite
func FindUserFavorite(c *gin.Context) { // Get model if exist
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.Query("id"))
	if isBadRequestID(c, errID) {
		return
	}

	u, _ := strconv.Atoi(c.Query("user_id"))
	userIDParams := uint(u)
	userID := auth.UserID
	if userIDParams > 0 && userIDParams != auth.UserID {
		if !isPermission(c, auth) {
			return
		}
		userID = userIDParams
	}

	sql1 := fmt.Sprintf(G.FINDUSERFAVORITESQL, i, userID)
	userfavorite, errF := model.Model.FindUserFavorite(sql1)
	if isBadRequest(c, errF) {
		return
	}

	sql2 := fmt.Sprintf(G.GETUSERBYIDSQL, userfavorite.UserID)
	user, err := model.Model.GetUser(sql2)
	if isServerError(c, err) {
		return
	}

	sql3 := fmt.Sprintf(G.FINDPRODUCTSQL, userfavorite.ProductID)
	product, err := model.Model.FindProduct(sql3)
	if isServerError(c, err) {
		return
	}

	res := GETUserFavorite{
		ID:      userfavorite.ID,
		User:    *user,
		Product: *product,
		Status:  userfavorite.Status,
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, res)
	c.JSON(status, response)
}

// PATCH /userfavorites/:id
// Update a userfavorite
func UpdateUserFavorite(c *gin.Context) {
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	// Get model if exist
	var input UpdateUserFavoriteInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.UserFavorite
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		errors := []string{}
		for _, value := range e {
			errors = append(errors, value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	userfavorite := model.UserFavorite{
		ID:        input.ID,
		UserID:    auth.UserID,
		ProductID: input.ProductID,
		Status:    input.Status,
	}

	sql := fmt.Sprintf(G.UPDATEUSERFAVORITESQL,
		userfavorite.UserID,
		userfavorite.ProductID,
		userfavorite.Status,
		userfavorite.ID,
	)

	data, err := model.Model.UpdateUserFavorite(userfavorite.UserID, &userfavorite, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, data)
	c.JSON(status, response)
}

// DELETE /userfavorites/:id
// Delete a userfavorite
func DeleteUserFavorite(c *gin.Context) {
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.PostForm("id"))
	if isBadRequestID(c, errID) {
		return
	}

	u, _ := strconv.Atoi(c.PostForm("user_id"))
	userIDParams := uint(u)
	userID := auth.UserID
	if userIDParams > 0 && userIDParams != auth.UserID {
		if !isPermission(c, auth) {
			return
		}
		userID = userIDParams
	}

	sql := fmt.Sprintf(G.DELETEUSERFAVORITESQL, i, userID)
	success, errF := model.Model.DeleteUserFavorite(userID, uint(i), sql)
	if isBadRequest(c, errF) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, success)
	c.JSON(status, response)
}
