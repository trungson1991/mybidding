package model

type Response struct {
	Status  int         `json:"status_code"`
	Message []string    `json:"message"`
	Data    interface{} `json:"data"`
}

func CreateResponse(status int, message []string, data interface{}) interface{} {
	r := Response{
		Status:  status,
		Message: message,
		Data:    data,
	}
	return r
}

type ListResponse struct {
	Data    interface{} `json:"data" form:"data"`
	Page    int         `json:"page" form:"page"`
	PageNum int         `json:"page_num" form:"page_num"`
}
