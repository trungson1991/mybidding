package controllers

import (
	G "Bidding_go/global"
	"Bidding_go/model"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type CreatePaymentTypeInput struct {
	PaymentCode string `json:"payment_code" form:"payment_code"`
	PaymentName string `json:"payment_name" form:"payment_name"`
	Description string `json:"description" form:"description"`
	Image       string `json:"image" form:"image"`
}

type UpdatePaymentTypeInput struct {
	ID          uint   `json:"id" form:"id" gorm:"primary_key"`
	PaymentCode string `json:"payment_code" form:"payment_code"`
	PaymentName string `json:"payment_name" form:"payment_name"`
	Description string `json:"description" form:"description"`
	Image       string `json:"image" form:"image"`
}

// GET /paymenttypes
// Get all paymenttypes
func ListPaymentTypes(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	page, _ := strconv.Atoi(c.Query("page"))
	num, _ := strconv.Atoi(c.Query("page_num"))

	sql := fmt.Sprintf(G.LISTPAYMENTTYPESQL, (page-1)*num, num)
	paymenttypes, err := model.Model.ListPaymentType(sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, paymenttypes)
	c.JSON(status, response)
}

// POST /paymenttypes
// Create new paymenttype
func CreatePaymentType(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	var input CreatePaymentTypeInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.PaymentType
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		fmt.Println(err)
		errors := []string{}
		for key, value := range e {
			errors = append(errors, key+" : "+value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	// Create paymenttype
	paymenttype := model.PaymentType{
		PaymentCode: input.PaymentCode,
		PaymentName: input.PaymentName,
		Description: input.Description,
		Image:       input.Image,
	}

	sql := fmt.Sprintf(G.CREATEPAYMENTTYPESQL,
		paymenttype.PaymentCode,
		paymenttype.PaymentName,
		paymenttype.Description,
		paymenttype.Image,
	)

	data, err := model.Model.CreatePaymentType(&paymenttype, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, data)
	c.JSON(status, response)
}

// GET /paymenttypes/:id
// Find a paymenttype
func FindPaymentType(c *gin.Context) { // Get model if exist
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.Query("id"))
	if isBadRequestID(c, errID) {
		return
	}

	sql1 := fmt.Sprintf(G.FINDPAYMENTTYPESQL, i)
	paymenttype, errF := model.Model.FindPaymentType(sql1)
	if isBadRequest(c, errF) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, paymenttype)
	c.JSON(status, response)
}

// PATCH /paymenttypes/:id
// Update a paymenttype
func UpdatePaymentType(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	// Get model if exist
	var input UpdatePaymentTypeInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.PaymentType
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		errors := []string{}
		for _, value := range e {
			errors = append(errors, value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	paymenttype := model.PaymentType{
		ID:          input.ID,
		PaymentCode: input.PaymentCode,
		PaymentName: input.PaymentName,
		Description: input.Description,
		Image:       input.Image,
	}

	sql := fmt.Sprintf(G.UPDATEPAYMENTTYPESQL,
		paymenttype.PaymentCode,
		paymenttype.PaymentName,
		paymenttype.Description,
		paymenttype.Image,
		paymenttype.ID,
	)

	data, err := model.Model.UpdatePaymentType(&paymenttype, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, data)
	c.JSON(status, response)
}

// DELETE /paymenttypes/:id
// Delete a paymenttype
func DeletePaymentType(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.PostForm("id"))
	if isBadRequestID(c, errID) {
		return
	}

	sql := fmt.Sprintf(G.DELETEPAYMENTTYPESQL, i)
	success, errF := model.Model.DeletePaymentType(uint(i), sql)
	if isBadRequest(c, errF) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, success)
	c.JSON(status, response)
}
