package controllers

import (
	G "Bidding_go/global"
	"Bidding_go/model"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type CreateRoleInput struct {
	Name        string `json:"name" form:"name"`
	DisplayName string `json:"display_name" form:"display_name"`
}

type UpdateRoleInput struct {
	ID          uint   `json:"id" form:"id" gorm:"primary_key"`
	Name        string `json:"name" form:"name"`
	DisplayName string `json:"display_name" form:"display_name"`
}

// GET /roles
// Get all roles
func ListRoles(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	page, _ := strconv.Atoi(c.Query("page"))
	num, _ := strconv.Atoi(c.Query("page_num"))

	sql := fmt.Sprintf(G.LISTROLESQL, (page-1)*num, num)
	roles, err := model.Model.ListRole(sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, roles)
	c.JSON(status, response)
}

// POST /roles
// Create new role
func CreateRole(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	var input CreateRoleInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.Role
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		fmt.Println(err)
		errors := []string{}
		for key, value := range e {
			errors = append(errors, key+" : "+value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	// Create role
	role := model.Role{
		Name:        input.Name,
		DisplayName: input.DisplayName,
	}

	sql := fmt.Sprintf(G.CREATEROLESQL,
		role.Name,
		role.DisplayName,
	)

	data, err := model.Model.CreateRole(&role, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, data)
	c.JSON(status, response)
}

// GET /roles/:id
// Find a role
func FindRole(c *gin.Context) { // Get model if exist
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.Query("id"))
	if isBadRequestID(c, errID) {
		return
	}

	sql := fmt.Sprintf(G.FINDROLESQL, i)
	role, errF := model.Model.FindRole(sql)
	if isBadRequest(c, errF) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, role)
	c.JSON(status, response)
}

// PATCH /roles/:id
// Update a role
func UpdateRole(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	// Get model if exist
	var input UpdateRoleInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.Role
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		errors := []string{}
		for _, value := range e {
			errors = append(errors, value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}
	role := model.Role{
		ID:          input.ID,
		Name:        input.Name,
		DisplayName: input.DisplayName,
	}

	sql := fmt.Sprintf(G.UPDATEROLESQL,
		role.Name,
		role.DisplayName,
		role.ID,
	)

	data, err := model.Model.UpdateRole(&role, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, data)
	c.JSON(status, response)
}

// DELETE /roles/:id
// Delete a role
func DeleteRole(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.PostForm("id"))
	if isBadRequestID(c, errID) {
		return
	}

	sql := fmt.Sprintf(G.DELETEROLESQL, i)
	success, errF := model.Model.DeleteRole(uint(i), sql)
	if isBadRequest(c, errF) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, success)
	c.JSON(status, response)
}
