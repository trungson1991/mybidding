package model

import (
	"fmt"
	"time"

	G "Bidding_go/global"

	"gorm.io/gorm"
)

type PaymentType struct {
	ID          uint           `json:"id" form:"id" gorm:"primary_key"`
	PaymentCode string         `json:"payment_code" form:"payment_code"`
	PaymentName string         `json:"payment_name" form:"payment_name"`
	Description string         `json:"description" form:"description"`
	Image       string         `json:"image" form:"image"`
	CreatedAt   time.Time      `json:"create_time" form:"create_time"`
	UpdatedAt   time.Time      `json:"update_time" form:"update_time"`
	DeletedAt   gorm.DeletedAt `json:"delete_time" form:"delete_time"`
}

func ValidatePaymentType(object *PaymentType) []string {
	strErrs := []string{}

	return strErrs
}

func (s *Server) ListPaymentType(sql string) ([]PaymentType, []string) {
	strErrs := []string{}
	var items []PaymentType
	err := s.DB.Debug().Raw(sql).Find(&items).Error

	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}

	return items, nil
}

func (s *Server) CreatePaymentType(object *PaymentType, sql string) (*PaymentType, []string) {
	strErrs := []string{}

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Create(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("CreatedAt", time.Now())
	return object, nil
}

func (s *Server) FindPaymentType(sql string) (*PaymentType, []string) {
	strErrs := []string{}
	var item *PaymentType
	if err := s.DB.Debug().Raw(sql).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	return item, nil
}

func (s *Server) UpdatePaymentType(object *PaymentType, sql string) (*PaymentType, []string) {
	strErrs := []string{}
	var item PaymentType
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHPAYMENTTYPESQL,
		object.ID,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	e := ValidatePaymentType(object)
	strErrs = append(strErrs, e...)

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Scan(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("UpdatedAt", time.Now())
	return object, nil
}

func (s *Server) DeletePaymentType(id uint, sql string) (bool, []string) {
	var item *PaymentType
	strErrs := []string{}
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHPAYMENTTYPESQL,
		id,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return false, strErrs
	}

	err := s.DB.Debug().Delete(&item).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return false, strErrs
	}

	return true, nil
}
