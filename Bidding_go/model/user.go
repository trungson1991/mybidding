package model

import (
	"fmt"
	"time"
	"unicode"

	"github.com/badoux/checkmail"
	"github.com/pkg/errors"
	"gorm.io/gorm"

	G "Bidding_go/global"
)

type User struct {
	ID         uint           `json:"id" form:"id" gorm:"primary_key"`
	UserName   string         `json:"user_name" form:"user_name"`
	Password   string         `json:"password" form:"password"`
	FirstName  string         `json:"first_name" form:"first_name"`
	LastName   string         `json:"last_name" form:"last_name"`
	Gender     int            `json:"gender" form:"gender"`
	Email      string         `json:"email" form:"email"`
	Birthday   time.Time      `json:"birthday" form:"birthday"`
	Address1   string         `json:"address1" form:"address1"`
	Address2   string         `json:"address2" form:"address2"`
	Phone      string         `json:"phone" form:"phone"`
	State      string         `json:"state" form:"state"`
	City       string         `json:"city" form:"city"`
	PortalCode string         `json:"portal_code" form:"portal_code"`
	Country    string         `json:"country" form:"country"`
	ActiveCode string         `json:"active_code" form:"active_code"`
	Status     int            `json:"status" form:"status"`
	Image      string         `json:"image" form:"image"`
	Role       int8           `json:"role" form:"role"`
	CreatedAt  time.Time      `json:"create_time" form:"create_time"`
	UpdatedAt  time.Time      `json:"update_time" form:"update_time"`
	DeletedAt  gorm.DeletedAt `json:"delete_time" form:"delete_time"`
}

func (s *Server) ValidateEmail(email *string) (*User, error) {
	if email == nil || *email == "" {
		return nil, errors.New(G.RequiredEmail)
	}

	if err := checkmail.ValidateFormat(*email); err != nil {
		return nil, errors.New(G.InvalidEmail)
	}

	var u User
	if err := s.DB.Raw(fmt.Sprintf(G.VALIDATEEMAILSQL, *email)).First(&u).Error; err == nil {
		return &u, errors.New(G.RecordFound)
	}

	return nil, nil
}

func (s *Server) ValidateUserName(userName *string) (*User, error) {
	if userName == nil || *userName == "" {
		return nil, errors.New(G.RequiredUserName)
	}
	if len(*userName) < 6 {
		return nil, errors.New(G.InvalidUserName)
	}

	var u User
	if err := s.DB.Raw(fmt.Sprintf(G.VALIDATEUSERNAMESQL, *userName)).First(&u).Error; err == nil {
		return &u, errors.New(G.RecordFound)
	}

	return nil, nil
}

func ValidatePassword(pass string) error {
	var (
		upp, low, num, sym bool
		tot                uint8
	)

	for _, char := range pass {
		switch {
		case unicode.IsUpper(char):
			upp = true
			tot++
		case unicode.IsLower(char):
			low = true
			tot++
		case unicode.IsNumber(char):
			num = true
			tot++
		case unicode.IsPunct(char) || unicode.IsSymbol(char):
			sym = true
			tot++
		default:
			return errors.New(G.PasswordNotCorrectFormat)
		}
	}

	if !upp || !low || !num || sym || tot < 6 {
		return errors.New(G.PasswordNotCorrectFormat)
	}

	return nil
}

func (s *Server) ListUsers(sql string) ([]User, []string) {
	strErrs := []string{}
	var users []User

	err := s.DB.Debug().Raw(sql).Find(&users).Error

	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}

	return users, nil
}

func (s *Server) CreateUser(user *User, sql string) (*User, []string) {
	strErrs := []string{}
	_, emailErr := s.ValidateEmail(&user.Email)
	if emailErr != nil {
		strErrs = append(strErrs, emailErr.Error())
	}

	_, userNameErr := s.ValidateUserName(&user.UserName)
	if userNameErr != nil {
		strErrs = append(strErrs, userNameErr.Error())
	}

	passwordErr := ValidatePassword(user.Password)
	if passwordErr != nil {
		strErrs = append(strErrs, passwordErr.Error())
	}

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Create(&user).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&user).Update("CreatedAt", time.Now())
	return user, nil
}

func (s *Server) GetUserInfo(userToken string) (*User, []string) {
	strErrs := []string{}
	u, err := s.GetUserWith(userToken)
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}

	return u, nil
}

func (s *Server) UpdateUser(user *User, userToken string, sql string) (*User, []string) {
	strErrs := []string{}
	u, err := s.GetUserWith(userToken)
	if err != nil {
		strErrs = append(strErrs, err.Error())
	}

	if len(user.Email) > 0 {
		ur, emailErr := s.ValidateEmail(&user.Email)
		if ur == nil && emailErr == nil {
			u.Email = user.Email
		} else if ur == nil && emailErr != nil {
			strErrs = append(strErrs, emailErr.Error())
		} else {
			if ur.Email == u.Email {
				u.Email = user.Email
			} else {
				strErrs = append(strErrs, emailErr.Error())
			}
		}
	}

	if len(user.UserName) > 0 {
		ur, userNameErr := s.ValidateUserName(&user.UserName)
		if ur == nil && userNameErr == nil {
			u.UserName = user.UserName
		} else if ur == nil && userNameErr != nil {
			strErrs = append(strErrs, userNameErr.Error())
		} else {
			if ur.UserName == u.UserName {
				u.UserName = user.UserName
			} else {
				strErrs = append(strErrs, userNameErr.Error())
			}
		}
	}

	if len(user.Password) > 0 {
		passwordErr := ValidatePassword(user.Password)
		if passwordErr != nil {
			strErrs = append(strErrs, passwordErr.Error())
		} else {
			u.Password = user.Password
		}
	}
	u.Image = user.Image
	u.FirstName = user.FirstName
	u.LastName = user.LastName

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	er := s.DB.Debug().Raw(sql).Scan(&u).Error
	if er != nil {
		strErrs = append(strErrs, er.Error())
		return nil, strErrs
	}
	s.DB.Model(&u).UpdateColumn("UpdatedAt", time.Now())
	return u, nil
}

func (s *Server) GetUser(sql string) (*User, []string) {
	strErrs := []string{}
	user := &User{}
	err := s.DB.Debug().Raw(fmt.Sprint(sql)).Take(&user).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	return user, nil
}
