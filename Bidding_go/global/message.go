package global

// Message
const (
	ConnectDatabaseError = "Connect database error"
	HandleDifferently    = "Handle differently"
	ImageIsIncorrect     = "Image is incorrect"
	ImageIsTooLarge      = "Image is too large"

	ErrorGettingEvn         = "error getting evn"
	ErrorConnectDatabase    = "error connect database"
	UnexpectedSigningMethod = "Unexpected signing method"
	YouNeedToBeAuthorized   = "You need to be authorized to access this route"

	RequiredEmail            = "Required email"
	InvalidEmail             = "Invalid email"
	RecordFound              = "Record found"
	RequiredUserName         = "Required username"
	InvalidUserName          = "Invalid username"
	PasswordNotCorrectFormat = "The password is not in the correct format"
	RoleRequired             = "A valid role is required"
	NumRequired              = "A valid num is required"
	PleaseTryToLoginLater    = "Please try to login later"
	SuccessfullyLoggedOut    = "Successfully logged out"

	Unauthorized = "Unauthorized"

	NoPermissionToCreate = "no permission to create"
	NoPermissionToUpdate = "no permission to update"
	NoPermissionToDelete = "no permission to delete"
	RecordNotFound       = "Record not found"
)
