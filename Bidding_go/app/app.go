package app

import (
	"log"
	"os"

	G "Bidding_go/global"
	M "Bidding_go/model"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

func init() {
	// ex, e := os.Executable()
	// if e != nil {
	// 	panic(e)
	// }
	// exPath := filepath.Dir(ex)
	// err := godotenv.Load(exPath + "/.env")

	err := godotenv.Load()
	if err != nil {
		log.Fatalf("%v, %v", G.ErrorGettingEvn, err)
	}
}

var router = gin.Default()

func StartApp() {
	//var conn Connect
	dbDriver := os.Getenv("DB_DRIVER")
	username := os.Getenv("DB_USER")
	password := os.Getenv("DB_PASSWORD")
	host := os.Getenv("DB_HOST")
	database := os.Getenv("DB_NAME")
	dbPort := os.Getenv("DB_PORT")

	_, err := M.Model.Initialize(dbDriver, username, password, dbPort, host, database)
	if err != nil {
		log.Fatal("", G.ErrorConnectDatabase, err)
		return
	}

	route()

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080" //localhost
	}
	log.Fatal(router.Run(":" + port))
}
