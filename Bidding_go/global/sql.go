package global

var DateFormatSQL = "2006-01-02 15:04:05"

// SQL
const (
	LISTUSERSQL          = "SELECT * FROM users WHERE `deleted_at` IS NULL LIMIT %d,%d"
	VALIDATEEMAILSQL     = "SELECT * FROM users WHERE `email` = '%s' AND `deleted_at` IS NULL"
	VALIDATEUSERNAMESQL  = "SELECT * FROM users WHERE `user_name` = '%s' AND `deleted_at` IS NULL"
	GETUSERBYEMAILSQL    = "SELECT * FROM users WHERE `email` = '%s' AND `deleted_at` IS NULL"
	GETUSERBYUSERNAMESQL = "SELECT * FROM users WHERE `user_name` = '%s' AND `deleted_at` IS NULL"
	GETUSERBYIDSQL       = "SELECT * FROM users WHERE `id` = %d AND `deleted_at` IS NULL"
	UPDATEUSERSQL        = "UPDATE users SET `user_name` = '%s',`password` = '%s',`first_name` = '%s',`last_name` = '%s',`gender` = %d,`email` = '%s',`birthday` = '%s',`address1` = '%s',`address2` = '%s',`phone` = '%s',`state` = '%s',`city` = '%s',`portal_code` = '%s',`country` = '%s',`active_code` = '%s',`status` = %d,`image` = '%s',`role` = %d WHERE `id` = %d AND `deleted_at` IS NULL"
	CREATEUSERSQL        = "INSERT INTO users (`user_name`,`password`,`first_name`,`last_name`,`gender`,`email`,`birthday`,`address1`,`address2`,`phone`,`state`,`city`,`portal_code`,`country`,`active_code`,`status`,`image`,`role`) VALUES ('%s','%s','%s','%s',%d,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',%d,'%s',%d)"

	// Bidding
	LISTBIDDINGSQL   = "SELECT * FROM biddings WHERE `deleted_at` IS NULL LIMIT %d,%d"
	FINDBIDDINGSQL   = "SELECT * FROM biddings WHERE `id` = %d AND `deleted_at` IS NULL"
	SEARCHBIDDINGSQL = "SELECT * FROM biddings WHERE `id` = %d AND `deleted_at` IS NULL"
	CREATEBIDDINGSQL = "INSERT INTO biddings (`product_id`,`bidding_code`,`bidding_user_winner_id`,`bidding_title`,`description`,`short_description`,`start_time`,`stop_time`,`notif_time`,`status`) VALUES (%d,'%s',%d,'%s','%s','%s','%s','%s','%s',%d)"
	UPDATEBIDDINGSQL = "UPDATE biddings SET `product_id` = %d,`bidding_code` = '%s',`bidding_user_winner_id` = %d,`bidding_title` = '%s',`description` = '%s',`short_description` = '%s',`start_time` = '%s',`stop_time` = '%s',`notif_time` = '%s',`status` = %d WHERE id = %d AND `deleted_at` IS NULL"
	DELETEBIDDINGSQL = "DELETE FROM biddings WHERE `id` = %d AND `deleted_at` IS NULL"

	// Bidding History
	LISTBIDDINGHISTORYSQL   = "SELECT * FROM bidding_histories WHERE `bidding_user_id` = %d AND `deleted_at` IS NULL LIMIT %d,%d"
	FINDBIDDINGHISTORYSQL   = "SELECT * FROM bidding_histories WHERE `id` = %d AND `bidding_user_id` = %d AND `deleted_at` IS NULL"
	SEARCHBIDDINGHISTORYSQL = "SELECT * FROM bidding_histories WHERE `id` = %d AND `bidding_user_id` = %d AND `deleted_at` IS NULL"
	CREATEBIDDINGHISTORYSQL = "INSERT INTO bidding_histories (`bidding_id`,`bidding_user_id`,`cost_order`,`status`) VALUES (%d,%d,'%s',%d)"
	UPDATEBIDDINGHISTORYSQL = "UPDATE bidding_histories SET `bidding_id` = %d,`bidding_user_id` = %d,`cost_order` = '%s',`status` = %d WHERE `id` = %d AND `deleted_at` IS NULL"
	DELETEBIDDINGHISTORYSQL = "DELETE FROM bidding_histories WHERE `id` = %d AND `deleted_at` IS NULL"

	// Bidding User
	LISTBIDDINGUSERSQL   = "SELECT * FROM bidding_users WHERE `employee_id` = %d AND `deleted_at` IS NULL LIMIT %d,%d"
	FINDBIDDINGUSERSQL   = "SELECT * FROM bidding_users WHERE `id` = %d AND `employee_id` = %d AND `deleted_at` IS NULL"
	SEARCHBIDDINGUSERSQL = "SELECT * FROM bidding_users WHERE `id` = %d AND `employee_id` = %d AND `deleted_at` IS NULL"
	CREATEBIDDINGUSERSQL = "INSERT INTO bidding_users (`employee_id`,`bidding_id`) VALUES (%d,%d)"
	UPDATEBIDDINGUSERSQL = "UPDATE bidding_users SET `employee_id` = %d,`bidding_id` = %d WHERE `id` = %d AND `deleted_at` IS NULL"
	DELETEBIDDINGUSERSQL = "DELETE FROM bidding_users WHERE `id` = %d AND `deleted_at` IS NULL"

	// Product
	LISTPRODUCTSQL   = "SELECT * FROM products WHERE `deleted_at` IS NULL LIMIT %d,%d"
	FINDPRODUCTSQL   = "SELECT * FROM products WHERE `id` = %d AND `deleted_at` IS NULL"
	SEARCHPRODUCTSQL = "SELECT * FROM products WHERE `id` = %d AND `deleted_at` IS NULL"
	CREATEPRODUCTSQL = "INSERT INTO products (`product_code`,`product_name`, `image`, `short_description`, `description`, `quantity_per_unit`, `is_featured`, `is_new`, `category_id`, `supplier_id`) VALUES ('%s','%s','%s','%s','%s','%s',%d,%d,%d,%d)"
	UPDATEPRODUCTSQL = "UPDATE products SET `product_code` = '%s',`product_name` = '%s',`image` = '%s',`short_description` = '%s',`description` = '%s',`quantity_per_unit` = '%s',`is_featured` = %d,`is_new` = %d,`category_id` = %d,`supplier_id` = %d WHERE `id` = %d AND `deleted_at` IS NULL"
	DELETEPRODUCTSQL = "DELETE FROM products WHERE `id` = %d AND `deleted_at` IS NULL"
	RANDOMPRODUCTSQL = "SELECT * FROM products WHERE `deleted_at` IS NULL ORDER BY RAND() LIMIT %d,%d"

	// Product Detail
	LISTPRODUCTDETAILSQL   = "SELECT * FROM product_details  WHERE `deleted_at` IS NULL LIMIT %d,%d"
	FINDPRODUCTDETAILSQL   = "SELECT * FROM product_details WHERE `id` = %d AND `deleted_at` IS NULL"
	SEARCHPRODUCTDETAILSQL = "SELECT * FROM product_details WHERE `id` = %d AND `deleted_at` IS NULL"
	CREATEPRODUCTDETAILSQL = "INSERT INTO product_details (`product_id`,`shop_id`, `employee_id`, `import_date`, `quantity`, `unit_price`, `type`) VALUES (%d,%d,%d,'%s','%s','%s','%s')"
	UPDATEPRODUCTDETAILSQL = "UPDATE product_details SET `product_id` = %d,`shop_id` = %d,`employee_id` = %d,`import_date` = '%s',`quantity` = '%s',`unit_price` = '%s',`type` = '%s' WHERE `id` = %d AND `deleted_at` IS NULL"
	DELETEPRODUCTDETAILSQL = "DELETE FROM product_details WHERE `id` = %d AND `deleted_at` IS NULL"

	// Product Review
	LISTPRODUCTREVIEWSQL   = "SELECT * FROM product_reviews WHERE `deleted_at` IS NULL LIMIT %d,%d"
	FINDPRODUCTREVIEWSQL   = "SELECT * FROM product_reviews WHERE `id` = %d AND `deleted_at` IS NULL"
	SEARCHPRODUCTREVIEWSQL = "SELECT * FROM product_reviews WHERE `id` = %d AND `deleted_at` IS NULL"
	CREATEPRODUCTREVIEWSQL = "INSERT INTO product_reviews (`product_id`,`user_id`, `rating`, `comment`) VALUES (%d,%d,'%s','%s')"
	UPDATEPRODUCTREVIEWSQL = "UPDATE product_reviews SET `product_id` = %d,`user_id` = %d,`rating` = '%s',`comment` = '%s' WHERE `id` = %d AND `deleted_at` IS NULL"
	DELETEPRODUCTREVIEWSQL = "DELETE FROM product_reviews WHERE `id` = %d AND `user_id` = %d AND `deleted_at` IS NULL"

	// Category
	LISTCATEGORYSQL   = "SELECT * FROM categories WHERE `deleted_at` IS NULL LIMIT %d,%d"
	FINDCATEGORYSQL   = "SELECT * FROM categories WHERE `id` = %d AND `deleted_at` IS NULL"
	SEARCHCATEGORYSQL = "SELECT * FROM categories WHERE `id` = %d AND `deleted_at` IS NULL"
	CREATECATEGORYSQL = "INSERT INTO categories (`category_code`,`category_name`,`description`,`image`) VALUES ('%s','%s','%s','%s')"
	UPDATECATEGORYSQL = "UPDATE categories SET `category_code` = '%s',`category_name` = '%s',`description` = '%s',`image` = '%s' WHERE `id` = %d AND `deleted_at` IS NULL"
	DELETECATEGORYSQL = "DELETE FROM categories WHERE `id` = %d AND `deleted_at` IS NULL"
	//2023-10-05 16:21:23.511
	// Supplier
	LISTSUPPLIERSQL   = "SELECT * FROM suppliers WHERE `deleted_at` IS NULL LIMIT %d,%d"
	FINDSUPPLIERSQL   = "SELECT * FROM suppliers WHERE `id` = %d AND `deleted_at` IS NULL"
	SEARCHSUPPLIERSQL = "SELECT * FROM suppliers WHERE `id` = %d AND `deleted_at` IS NULL"
	CREATESUPPLIERSQL = "INSERT INTO suppliers (`supplier_code`,`supplier_name`,`description`,`image`) VALUES ('%s','%s','%s','%s')"
	UPDATESUPPLIERSQL = "UPDATE suppliers SET `supplier_code` = '%s',`supplier_name` = '%s',`description` = '%s',`image` = '%s' WHERE `id` = %d AND `deleted_at` IS NULL"
	DELETESUPPLIERSQL = "DELETE FROM suppliers WHERE `id` = %d AND `deleted_at` IS NULL"

	// Order
	LISTORDERSQL   = "SELECT * FROM orders WHERE `employee_id` = %d AND `deleted_at` IS NULL LIMIT %d,%d"
	FINDORDERSQL   = "SELECT * FROM orders WHERE `id` = %d AND `employee_id` = %d AND `deleted_at` IS NULL"
	SEARCHORDERSQL = "SELECT * FROM orders WHERE `id` = %d AND `employee_id` = %d AND `deleted_at` IS NULL"
	CREATEORDERSQL = "INSERT INTO orders (`employee_id`,`customer_id`,`bidding_id`,`order_date`,`shipped_date`,`ship_name`,`ship_address1`,`ship_address2`,`ship_city`,`ship_state`,`ship_postal_code`,`ship_country`,`shipping_fee`,`paid_date`,`payment_type_id`,`order_status`,`type_order`) VALUES (%d,%d,%d,'%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')"
	UPDATEORDERSQL = "UPDATE orders SET `employee_id` = %d,`customer_id` = %d,`bidding_id` = %d,`order_date` = '%s',`shipped_date` = '%s',`ship_name` = '%s',`ship_address1` = '%s',`ship_address2` = '%s',`ship_city` = '%s',`ship_state` = '%s',`ship_postal_code` = '%s',`ship_country` = '%s',`shipping_fee` = '%s',`paid_date` = '%s',`payment_type_id` = '%s',`order_status` = '%s',`type_order = '%s'` WHERE `id` = %d AND `deleted_at` IS NULL"
	DELETEORDERSQL = "DELETE FROM orders WHERE `id` = %d AND `employee_id` = %d AND `deleted_at` IS NULL"

	// Order Detail
	LISTORDERDETAILSQL   = "SELECT * FROM order_details WHERE `deleted_at` IS NULL LIMIT %d,%d"
	FINDORDERDETAILSQL   = "SELECT * FROM order_details WHERE `id` = %d AND `deleted_at` IS NULL"
	SEARCHORDERDETAILSQL = "SELECT * FROM order_details WHERE `id` = %d AND `deleted_at` IS NULL"
	CREATEORDERDETAILSQL = "INSERT INTO order_details (`order_id`,`product_id`,`quantity`,`unit_price`,`discount`,`status`,`date_allocated`) VALUES (%d,%d,%d,%f,%f,%d,'%s')"
	UPDATEORDERDETAILSQL = "UPDATE order_details SET `order_id` = %d,`product_id` = %d,`quantity` = %d,`unit_price` = %f,`discount` = %f,`status` = %d,`date_allocated` = '%s' WHERE `id` = %d AND `deleted_at` IS NULL"
	DELETEORDERDETAILSQL = "DELETE FROM order_details WHERE `id` = %d AND `deleted_at` IS NULL"

	// Shop
	LISTSHOPSQL   = "SELECT * FROM shops WHERE `deleted_at` IS NULL LIMIT %d,%d"
	FINDSHOPSQL   = "SELECT * FROM shops WHERE `id` = %d AND `deleted_at` IS NULL"
	SEARCHSHOPSQL = "SELECT * FROM shops WHERE `id` = %d AND `deleted_at` IS NULL"
	CREATESHOPSQL = "INSERT INTO shops (`code`,`shop_name`,`description`,`image`) VALUES ('%s','%s','%s','%s')"
	UPDATESHOPSQL = "UPDATE shops SET `code` = '%s',`shop_name` = '%s',`description` = '%s',`image` = '%s' WHERE `id` = %d AND `deleted_at` IS NULL"
	DELETESHOPSQL = "DELETE FROM shops WHERE `id` = %d AND `employee_id` = %d AND `deleted_at` IS NULL"

	// Payment type
	LISTPAYMENTTYPESQL   = "SELECT * FROM payment_types WHERE `deleted_at` IS NULL LIMIT %d,%d"
	FINDPAYMENTTYPESQL   = "SELECT * FROM payment_types WHERE `id` = %d AND `deleted_at` IS NULL"
	SEARCHPAYMENTTYPESQL = "SELECT * FROM payment_types WHERE `id` = %d AND `deleted_at` IS NULL"
	CREATEPAYMENTTYPESQL = "INSERT INTO payment_types (`payment_code`,`payment_name`,`description`,`image`) VALUES ('%s','%s','%s','%s')"
	UPDATEPAYMENTTYPESQL = "UPDATE payment_types SET `payment_code` = '%s',`payment_name` = '%s',`description` = '%s',`image` = '%s' WHERE `id` = %d AND `deleted_at` IS NULL"
	DELETEPAYMENTTYPESQL = "DELETE FROM payment_types WHERE `id` = %d AND `deleted_at` IS NULL"

	// Role
	LISTROLESQL   = "SELECT * FROM roles WHERE `deleted_at` IS NULL LIMIT %d,%d"
	FINDROLESQL   = "SELECT * FROM roles WHERE `id` = %d AND `deleted_at` IS NULL"
	SEARCHROLESQL = "SELECT * FROM roles WHERE `id` = %d AND `deleted_at` IS NULL"
	CREATEROLESQL = "INSERT INTO roles (`name`,`display_name`) VALUES ('%s','%s')"
	UPDATEROLESQL = "UPDATE roles SET `name` = '%s',`display_name` = '%s' WHERE `id` = %d AND `deleted_at` IS NULL"
	DELETEROLESQL = "DELETE FROM roles WHERE `id` = %d AND `deleted_at` IS NULL"

	// User favorite
	LISTUSERFAVORITESQL         = "SELECT * FROM user_favorites WHERE `user_id` = %d AND `deleted_at` IS NULL LIMIT %d,%d"
	FINDUSERFAVORITESQL         = "SELECT * FROM user_favorites WHERE `id` = %d AND `user_id` = %d AND `deleted_at` IS NULL"
	FINDUSERFAVORITEPRODUCRRSQL = "SELECT * FROM user_favorites WHERE `product_id` = %d AND `user_id` = %d AND `deleted_at` IS NULL"
	SEARCHUSERFAVORITESQL       = "SELECT * FROM user_favorites WHERE `id` = %d AND `user_id` = %d AND `deleted_at` IS NULL"
	CREATEUSERFAVORITESQL       = "INSERT INTO user_favorites (`user_id`,`product_id`,`status`) VALUES (%d,%d,%d)"
	UPDATEUSERFAVORITESQL       = "UPDATE user_favorites SET `user_id` = %d,`product_id` = %d,`status` = %d WHERE `id` = %d AND `deleted_at` IS NULL"
	DELETEUSERFAVORITESQL       = "DELETE FROM user_favorites WHERE `id` = %d AND `user_id` = %d AND `deleted_at` IS NULL"

	// User role
	LISTUSERROLESQL   = "SELECT * FROM user_roles WHERE `user_id` = %d AND `deleted_at` IS NULL LIMIT %d,%d"
	FINDUSERROLESQL   = "SELECT * FROM user_roles WHERE `id` = %d AND `user_id` = %d AND `deleted_at` IS NULL"
	SEARCHUSERROLESQL = "SELECT * FROM user_roles WHERE `id` = %d AND `user_id` = %d AND `deleted_at` IS NULL"
	CREATEUSERROLESQL = "INSERT INTO user_roles (`user_id`,`role_id`) VALUES (%d,%d)"
	UPDATEUSERROLESQL = "UPDATE user_roles SET `user_id` = %d,`role_id` = %d WHERE `id` = %d AND `deleted_at` IS NULL"
	DELETEUSERROLESQL = "DELETE FROM user_roles WHERE `id` = %d AND `user_id` = %d AND `deleted_at` IS NULL"
)
