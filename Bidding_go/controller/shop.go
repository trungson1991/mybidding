package controllers

import (
	G "Bidding_go/global"
	"Bidding_go/model"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type CreateShopInput struct {
	Code        string `json:"code" form:"code"`
	ShopName    string `json:"shop_name" form:"shop_name"`
	Description string `json:"description" form:"description"`
}

type UpdateShopInput struct {
	ID          uint   `json:"id" form:"id" gorm:"primary_key"`
	Code        string `json:"code" form:"code"`
	ShopName    string `json:"shop_name" form:"shop_name"`
	Description string `json:"description" form:"description"`
}

// GET /shops
// Get all shops
func ListShops(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	page, _ := strconv.Atoi(c.Query("page"))
	num, _ := strconv.Atoi(c.Query("page_num"))

	sql := fmt.Sprintf(G.LISTSHOPSQL, (page-1)*num, num)
	shops, err := model.Model.ListShop(sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, shops)
	c.JSON(status, response)
}

// POST /shops
// Create new shop
func CreateShop(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	var input CreateShopInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.Shop
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		fmt.Println(err)
		errors := []string{}
		for key, value := range e {
			errors = append(errors, key+" : "+value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	fileName, errUpload := model.UploadImage(c)
	var strErrUpload string
	if errUpload != nil {
		strErrUpload = errUpload.Error()
	}

	var imageName string = ""
	if fileName != nil {
		imageName = *fileName
	}

	// Create shop
	shop := model.Shop{
		Code:        input.Code,
		ShopName:    input.ShopName,
		Description: input.Description,
		Image:       imageName,
	}

	sql := fmt.Sprintf(G.CREATESHOPSQL,
		shop.Code,
		shop.ShopName,
		shop.Description,
		shop.Image,
	)

	data, err := model.Model.CreateShop(&shop, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	var response interface{}
	if len(strErrUpload) > 0 {
		err2 := []string{strErrUpload}
		response = model.CreateResponse(status, err2, data)
	} else {
		response = model.CreateResponse(status, nil, data)
	}
	c.JSON(status, response)
}

// GET /shops/:id
// Find a shop
func FindShop(c *gin.Context) { // Get model if exist
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.Query("id"))
	if isBadRequestID(c, errID) {
		return
	}

	sql := fmt.Sprintf(G.FINDSHOPSQL, i)
	shop, errF := model.Model.FindShop(sql)
	if isBadRequest(c, errF) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, shop)
	c.JSON(status, response)
}

// PATCH /shops/:id
// Update a shop
func UpdateShop(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	// Get model if exist
	var input UpdateShopInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.Shop
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		errors := []string{}
		for _, value := range e {
			errors = append(errors, value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	fileName, errUpload := model.UploadImage(c)
	var strErrUpload string
	if errUpload != nil {
		strErrUpload = errUpload.Error()
	}

	var imageName string = ""
	if fileName != nil {
		imageName = *fileName
	}

	shop := model.Shop{
		ID:          input.ID,
		Code:        input.Code,
		ShopName:    input.ShopName,
		Description: input.Description,
		Image:       imageName,
	}

	sql := fmt.Sprintf(G.UPDATESHOPSQL,
		shop.Code,
		shop.ShopName,
		shop.Description,
		shop.Image,
		shop.ID,
	)

	data, err := model.Model.UpdateShop(&shop, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	var response interface{}
	if len(strErrUpload) > 0 {
		err2 := []string{strErrUpload}
		response = model.CreateResponse(status, err2, data)
	} else {
		response = model.CreateResponse(status, nil, data)
	}
	c.JSON(status, response)
}

// DELETE /shops/:id
// Delete a shop
func DeleteShop(c *gin.Context) {
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.PostForm("id"))
	if isBadRequestID(c, errID) {
		return
	}

	u, _ := strconv.Atoi(c.PostForm("user_id"))
	userIDParams := uint(u)
	userID := auth.UserID
	if userIDParams > 0 && userIDParams != auth.UserID {
		if !isPermission(c, auth) {
			return
		}
		userID = userIDParams
	}

	sql := fmt.Sprintf(G.DELETESHOPSQL, i, userID)
	success, errF := model.Model.DeleteShop(uint(i), sql)
	if isBadRequest(c, errF) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, success)
	c.JSON(status, response)
}
