package model

import (
	"fmt"
	"time"

	G "Bidding_go/global"

	"gorm.io/gorm"
)

type BiddingUser struct {
	ID         uint           `json:"id" form:"id" gorm:"primary_key"`
	EmployeeID uint           `json:"employee_id" form:"employee_id"`
	BiddingID  uint           `json:"bidding_id" form:"bidding_id"`
	CreatedAt  time.Time      `json:"create_time" form:"create_time"`
	UpdatedAt  time.Time      `json:"update_time" form:"update_time"`
	DeletedAt  gorm.DeletedAt `json:"delete_time" form:"delete_time"`
}

func ValidateBiddingUser(object *BiddingUser) []string {
	strErrs := []string{}

	return strErrs
}

func (s *Server) ListBiddingUser(sql string) ([]BiddingUser, []string) {
	strErrs := []string{}
	var items []BiddingUser
	err := s.DB.Debug().Raw(sql).Find(&items).Error

	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}

	return items, nil
}

func (s *Server) CreateBiddingUser(object *BiddingUser, sql string) (*BiddingUser, []string) {
	strErrs := []string{}

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Create(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("CreatedAt", time.Now())
	return object, nil
}

func (s *Server) FindBiddingUser(sql string) (*BiddingUser, []string) {
	strErrs := []string{}
	var item *BiddingUser
	if err := s.DB.Debug().Raw(sql).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	return item, nil
}

func (s *Server) UpdateBiddingUser(userID uint, object *BiddingUser, sql string) (*BiddingUser, []string) {
	strErrs := []string{}
	var item BiddingUser
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHBIDDINGUSERSQL,
		object.ID,
		userID,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	e := ValidateBiddingUser(object)
	strErrs = append(strErrs, e...)

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Scan(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("UpdatedAt", time.Now())
	return object, nil
}

func (s *Server) DeleteBiddingUser(userID uint, id uint, sql string) (bool, []string) {
	var item *BiddingUser
	strErrs := []string{}
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHBIDDINGUSERSQL,
		id,
		userID,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return false, strErrs
	}

	err := s.DB.Debug().Delete(&item).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return false, strErrs
	}

	return true, nil
}
