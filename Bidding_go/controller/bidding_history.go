package controllers

import (
	G "Bidding_go/global"
	"Bidding_go/model"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type CreateBiddingHistoryInput struct {
	BiddingID     uint   `json:"bidding_id" form:"bidding_id"`
	BiddingUserID uint   `json:"bidding_user_id" form:"bidding_user_id"`
	CostOrder     string `json:"cost_order" form:"cost_order"`
	Status        int    `json:"status" form:"status"`
}

type UpdateBiddingHistoryInput struct {
	ID            uint   `json:"id" form:"id" gorm:"primary_key"`
	BiddingID     uint   `json:"bidding_id" form:"bidding_id"`
	BiddingUserID uint   `json:"bidding_user_id" form:"bidding_user_id"`
	CostOrder     string `json:"cost_order" form:"cost_order"`
	Status        int    `json:"status" form:"status"`
}

type GETBiddingHistory struct {
	ID          uint          `json:"id" form:"id" gorm:"primary_key"`
	Bidding     model.Bidding `json:"bidding" form:"bidding"`
	BiddingUser model.User    `json:"bidding_user" form:"bidding_user"`
	CostOrder   string        `json:"cost_order" form:"cost_order"`
	Status      int           `json:"status" form:"status"`
}

// GET /biddinghistorys
// Get all biddinghistorys
func ListBiddingHistorys(c *gin.Context) {
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	u, _ := strconv.Atoi(c.Query("user_id"))
	userIDParams := uint(u)
	userID := auth.UserID
	if userIDParams > 0 && userIDParams != auth.UserID {
		if !isPermission(c, auth) {
			return
		}
		userID = userIDParams
	}

	page, _ := strconv.Atoi(c.Query("page"))
	num, _ := strconv.Atoi(c.Query("page_num"))

	sql := fmt.Sprintf(G.LISTBIDDINGHISTORYSQL, userID, (page-1)*num, num)
	biddinghistorys, err := model.Model.ListBiddingHistory(sql)
	if isServerError(c, err) {
		return
	}

	var getBiddingHistorys []GETBiddingHistory
	for _, item := range biddinghistorys {
		sql := fmt.Sprintf(G.GETUSERBYIDSQL, item.BiddingUserID)
		user, err := model.Model.GetUser(sql)
		if isServerError(c, err) {
			return
		}

		sql1 := fmt.Sprintf(G.FINDBIDDINGSQL, item.BiddingID)
		bidding, err := model.Model.FindBidding(sql1)
		if isServerError(c, err) {
			return
		}

		i := GETBiddingHistory{
			ID:          item.ID,
			Bidding:     *bidding,
			BiddingUser: *user,
			CostOrder:   item.CostOrder,
			Status:      item.Status,
		}

		getBiddingHistorys = append(getBiddingHistorys, i)
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, getBiddingHistorys)
	c.JSON(status, response)
}

// POST /biddinghistorys
// Create new biddinghistory
func CreateBiddingHistory(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	var input CreateBiddingHistoryInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.BiddingHistory
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		fmt.Println(err)
		errors := []string{}
		for key, value := range e {
			errors = append(errors, key+" : "+value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	// Create biddinghistory
	biddinghistory := model.BiddingHistory{
		BiddingID:     input.BiddingID,
		BiddingUserID: input.BiddingUserID,
		CostOrder:     input.CostOrder,
		Status:        input.Status,
	}

	sql := fmt.Sprintf(G.CREATEBIDDINGHISTORYSQL,
		biddinghistory.BiddingID,
		biddinghistory.BiddingUserID,
		biddinghistory.CostOrder,
		biddinghistory.Status,
	)

	data, err := model.Model.CreateBiddingHistory(&biddinghistory, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, data)
	c.JSON(status, response)
}

// GET /biddinghistorys/:id
// Find a biddinghistory
func FindBiddingHistory(c *gin.Context) { // Get model if exist
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.Query("id"))
	if isBadRequestID(c, errID) {
		return
	}

	u, _ := strconv.Atoi(c.Query("user_id"))
	userIDParams := uint(u)
	userID := auth.UserID
	if userIDParams > 0 && userIDParams != auth.UserID {
		if !isPermission(c, auth) {
			return
		}
		userID = userIDParams
	}

	sql1 := fmt.Sprintf(G.FINDBIDDINGHISTORYSQL, i, userID)
	biddinghistory, errF := model.Model.FindBiddingHistory(sql1)
	if isBadRequest(c, errF) {
		return
	}

	sql2 := fmt.Sprintf(G.GETUSERBYIDSQL, biddinghistory.BiddingUserID)
	user, err := model.Model.GetUser(sql2)
	if isServerError(c, err) {
		return
	}

	sql3 := fmt.Sprintf(G.FINDBIDDINGSQL, biddinghistory.BiddingID)
	bidding, err := model.Model.FindBidding(sql3)
	if isServerError(c, err) {
		return
	}

	res := GETBiddingHistory{
		ID:          biddinghistory.ID,
		Bidding:     *bidding,
		BiddingUser: *user,
		CostOrder:   biddinghistory.CostOrder,
		Status:      biddinghistory.Status,
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, res)
	c.JSON(status, response)
}

// PATCH /biddinghistorys/:id
// Update a biddinghistory
func UpdateBiddingHistory(c *gin.Context) {
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	// Get model if exist
	var input UpdateBiddingHistoryInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.BiddingHistory
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		errors := []string{}
		for _, value := range e {
			errors = append(errors, value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	userID := auth.UserID

	biddinghistory := model.BiddingHistory{
		ID:            input.ID,
		BiddingID:     input.BiddingID,
		BiddingUserID: input.BiddingUserID,
		CostOrder:     input.CostOrder,
		Status:        input.Status,
	}

	sql := fmt.Sprintf(G.UPDATEBIDDINGHISTORYSQL,
		biddinghistory.BiddingID,
		biddinghistory.BiddingUserID,
		biddinghistory.CostOrder,
		biddinghistory.Status,
		biddinghistory.ID,
	)

	data, err := model.Model.UpdateBiddingHistory(userID, &biddinghistory, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, data)
	c.JSON(status, response)
}

// DELETE /biddinghistorys/:id
// Delete a biddinghistory
func DeleteBiddingHistory(c *gin.Context) {
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.PostForm("id"))
	if isBadRequestID(c, errID) {
		return
	}

	u, _ := strconv.Atoi(c.PostForm("user_id"))
	userIDParams := uint(u)
	userID := auth.UserID
	if userIDParams > 0 && userIDParams != auth.UserID {
		if !isPermission(c, auth) {
			return
		}
		userID = userIDParams
	}

	sql := fmt.Sprintf(G.DELETEBIDDINGHISTORYSQL, i)
	success, errF := model.Model.DeleteBiddingHistory(userID, uint(i), sql)
	if isBadRequest(c, errF) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, success)
	c.JSON(status, response)
}
