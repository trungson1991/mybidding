package app

import (
	middlewares "Bidding_go/auth"
	controllers "Bidding_go/controller"
)

const (
	listUsers  = "/user_list"
	infoUsers  = "/user_info"
	register   = "/user_register"
	login      = "/user_login"
	logout     = "/user_logout"
	updateUser = "/user_update"

	home        = "/home"
	displayList = "/display_list"

	listBiddingHistory   = "/bidding/history_list"
	createBiddingHistory = "/bidding/history_create"
	findBiddingHistory   = "/bidding/history_find"
	updateBiddingHistory = "/bidding/history_update"
	deleteBiddingHistory = "/bidding/history_delete"

	listBiddingUser   = "/bidding/user_list"
	createBiddingUser = "/bidding/user_create"
	findBiddingUser   = "/bidding/user_find"
	updateBiddingUser = "/bidding/user_update"
	deleteBiddingUser = "/bidding/user_delete"

	listBidding   = "/bidding_list"
	createBidding = "/bidding_create"
	findBidding   = "/bidding_find"
	updateBidding = "/bidding_update"
	deleteBidding = "/bidding_delete"

	listCategory   = "/category_list"
	createCategory = "/category_create"
	findCategory   = "/category_find"
	updateCategory = "/category_update"
	deleteCategory = "/category_delete"

	listOrderDetail   = "/order/detail_list"
	createOrderDetail = "/order/detail_create"
	findOrderDetail   = "/order/detail_find"
	updateOrderDetail = "/order/detail_update"
	deleteOrderDetail = "/order/detail_delete"

	listOrder   = "/order_list"
	createOrder = "/order_create"
	findOrder   = "/order_find"
	updateOrder = "/order_update"
	deleteOrder = "/order_delete"

	listPaymentType   = "/paymenttype_list"
	createPaymentType = "/paymenttype_create"
	findPaymentType   = "/paymenttype_find"
	updatePaymentType = "/paymenttype_update"
	deletePaymentType = "/paymenttype_delete"

	listProductDetail   = "/product/detail_list"
	createProductDetail = "/product/detail_create"
	findProductDetail   = "/product/detail_find"
	updateProductDetail = "/product/detail_update"
	deleteProductDetail = "/product/detail_delete"

	listProductReview   = "/product/review_list"
	createProductReview = "/product/review_create"
	findProductReview   = "/product/review_find"
	updateProductReview = "/product/review_update"
	deleteProductReview = "/product/review_delete"

	listProduct   = "/product_list"
	createProduct = "/product_create"
	findProduct   = "/product_find"
	updateProduct = "/product_update"
	deleteProduct = "/product_delete"

	listRole   = "/role_list"
	createRole = "/role_create"
	findRole   = "/role_find"
	updateRole = "/role_update"
	deleteRole = "/role_delete"

	listShop   = "/shop_list"
	createShop = "/shop_create"
	findShop   = "/shop_find"
	updateShop = "/shop_update"
	deleteShop = "/shop_delete"

	listSupplier   = "/supplier_list"
	createSupplier = "/supplier_create"
	findSupplier   = "/supplier_find"
	updateSupplier = "/supplier_update"
	deleteSupplier = "/supplier_delete"

	listUserFavorite   = "/user/favorite_list"
	createUserFavorite = "/user/favorite_create"
	findUserFavorite   = "/user/favorite_find"
	updateUserFavorite = "/user/favorite_update"
	deleteUserFavorite = "/user/favorite_delete"

	listUserRole   = "/user/role_list"
	createUserRole = "/user/role_create"
	findUserRole   = "/user/role_find"
	updateUserRole = "/user/role_update"
	deleteUserRole = "/user/role_delete"
)

func route() {

	v1 := router.Group("/v1")
	{
		// user
		v1.GET(listUsers, middlewares.TokenAuthMiddleware(), controllers.ListUsers)
		v1.GET(infoUsers, middlewares.TokenAuthMiddleware(), controllers.GetUserInfo)
		v1.POST(register, controllers.CreateUser)
		v1.POST(login, controllers.Login)
		v1.POST(logout, middlewares.TokenAuthMiddleware(), controllers.LogOut)
		v1.PATCH(updateUser, middlewares.TokenAuthMiddleware(), controllers.UpdateUser)

		v1.GET(home, middlewares.TokenAuthMiddleware(), controllers.GetHome)
		v1.GET(displayList, middlewares.TokenAuthMiddleware(), controllers.GetDisplayList)

		// bidding history
		v1.GET(listBiddingHistory, middlewares.TokenAuthMiddleware(), controllers.ListBiddingHistorys)
		v1.POST(createBiddingHistory, middlewares.TokenAuthMiddleware(), controllers.CreateBiddingHistory)
		v1.GET(findBiddingHistory, middlewares.TokenAuthMiddleware(), controllers.FindBiddingHistory)
		v1.PATCH(updateBiddingHistory, middlewares.TokenAuthMiddleware(), controllers.UpdateBiddingHistory)
		v1.DELETE(deleteBiddingHistory, middlewares.TokenAuthMiddleware(), controllers.DeleteBiddingHistory)

		// bidding user
		v1.GET(listBiddingUser, middlewares.TokenAuthMiddleware(), controllers.ListBiddingUsers)
		v1.POST(createBiddingUser, middlewares.TokenAuthMiddleware(), controllers.CreateBiddingUser)
		v1.GET(findBiddingUser, middlewares.TokenAuthMiddleware(), controllers.FindBiddingUser)
		v1.PATCH(updateBiddingUser, middlewares.TokenAuthMiddleware(), controllers.UpdateBiddingUser)
		v1.DELETE(deleteBiddingUser, middlewares.TokenAuthMiddleware(), controllers.DeleteBiddingUser)

		// bidding
		v1.GET(listBidding, middlewares.TokenAuthMiddleware(), controllers.ListBiddings)
		v1.POST(createBidding, middlewares.TokenAuthMiddleware(), controllers.CreateBidding)
		v1.GET(findBidding, middlewares.TokenAuthMiddleware(), controllers.FindBidding)
		v1.PATCH(updateBidding, middlewares.TokenAuthMiddleware(), controllers.UpdateBidding)
		v1.DELETE(deleteBidding, middlewares.TokenAuthMiddleware(), controllers.DeleteBidding)

		// category
		v1.GET(listCategory, middlewares.TokenAuthMiddleware(), controllers.ListCategorys)
		v1.POST(createCategory, middlewares.TokenAuthMiddleware(), controllers.CreateCategory)
		v1.GET(findCategory, middlewares.TokenAuthMiddleware(), controllers.FindCategory)
		v1.PATCH(updateCategory, middlewares.TokenAuthMiddleware(), controllers.UpdateCategory)
		v1.DELETE(deleteCategory, middlewares.TokenAuthMiddleware(), controllers.DeleteCategory)

		// order detail
		v1.GET(listOrderDetail, middlewares.TokenAuthMiddleware(), controllers.ListOrderDetails)
		v1.POST(createOrderDetail, middlewares.TokenAuthMiddleware(), controllers.CreateOrderDetail)
		v1.GET(findOrderDetail, middlewares.TokenAuthMiddleware(), controllers.FindOrderDetail)
		v1.PATCH(updateOrderDetail, middlewares.TokenAuthMiddleware(), controllers.UpdateOrderDetail)
		v1.DELETE(deleteOrderDetail, middlewares.TokenAuthMiddleware(), controllers.DeleteOrderDetail)

		// order
		v1.GET(listOrder, middlewares.TokenAuthMiddleware(), controllers.ListOrders)
		v1.POST(createOrder, middlewares.TokenAuthMiddleware(), controllers.CreateOrder)
		v1.GET(findOrder, middlewares.TokenAuthMiddleware(), controllers.FindOrder)
		v1.PATCH(updateOrder, middlewares.TokenAuthMiddleware(), controllers.UpdateOrder)
		v1.DELETE(deleteOrder, middlewares.TokenAuthMiddleware(), controllers.DeleteOrder)

		// payment type
		v1.GET(listPaymentType, middlewares.TokenAuthMiddleware(), controllers.ListPaymentTypes)
		v1.POST(createPaymentType, middlewares.TokenAuthMiddleware(), controllers.CreatePaymentType)
		v1.GET(findPaymentType, middlewares.TokenAuthMiddleware(), controllers.FindPaymentType)
		v1.PATCH(updatePaymentType, middlewares.TokenAuthMiddleware(), controllers.UpdatePaymentType)
		v1.DELETE(deletePaymentType, middlewares.TokenAuthMiddleware(), controllers.DeletePaymentType)

		// product detail
		v1.GET(listProductDetail, middlewares.TokenAuthMiddleware(), controllers.ListProductDetails)
		v1.POST(createProductDetail, middlewares.TokenAuthMiddleware(), controllers.CreateProductDetail)
		v1.GET(findProductDetail, middlewares.TokenAuthMiddleware(), controllers.FindProductDetail)
		v1.PATCH(updateProductDetail, middlewares.TokenAuthMiddleware(), controllers.UpdateProductDetail)
		v1.DELETE(deleteProductDetail, middlewares.TokenAuthMiddleware(), controllers.DeleteProductDetail)

		// product review
		v1.GET(listProductReview, middlewares.TokenAuthMiddleware(), controllers.ListProductReviews)
		v1.POST(createProductReview, middlewares.TokenAuthMiddleware(), controllers.CreateProductReview)
		v1.GET(findProductReview, middlewares.TokenAuthMiddleware(), controllers.FindProductReview)
		v1.PATCH(updateProductReview, middlewares.TokenAuthMiddleware(), controllers.UpdateProductReview)
		v1.DELETE(deleteProductReview, middlewares.TokenAuthMiddleware(), controllers.DeleteProductReview)

		// product
		v1.GET(listProduct, middlewares.TokenAuthMiddleware(), controllers.ListProducts)
		v1.POST(createProduct, middlewares.TokenAuthMiddleware(), controllers.CreateProduct)
		v1.GET(findProduct, middlewares.TokenAuthMiddleware(), controllers.FindProduct)
		v1.PATCH(updateProduct, middlewares.TokenAuthMiddleware(), controllers.UpdateProduct)
		v1.DELETE(deleteProduct, middlewares.TokenAuthMiddleware(), controllers.DeleteProduct)

		// role
		v1.GET(listRole, middlewares.TokenAuthMiddleware(), controllers.ListRoles)
		v1.POST(createRole, middlewares.TokenAuthMiddleware(), controllers.CreateRole)
		v1.GET(findRole, middlewares.TokenAuthMiddleware(), controllers.FindRole)
		v1.PATCH(updateRole, middlewares.TokenAuthMiddleware(), controllers.UpdateRole)
		v1.DELETE(deleteRole, middlewares.TokenAuthMiddleware(), controllers.DeleteRole)

		// shop
		v1.GET(listShop, middlewares.TokenAuthMiddleware(), controllers.ListShops)
		v1.POST(createShop, middlewares.TokenAuthMiddleware(), controllers.CreateShop)
		v1.GET(findShop, middlewares.TokenAuthMiddleware(), controllers.FindShop)
		v1.PATCH(updateShop, middlewares.TokenAuthMiddleware(), controllers.UpdateShop)
		v1.DELETE(deleteShop, middlewares.TokenAuthMiddleware(), controllers.DeleteShop)

		// supplier
		v1.GET(listSupplier, middlewares.TokenAuthMiddleware(), controllers.ListSuppliers)
		v1.POST(createSupplier, middlewares.TokenAuthMiddleware(), controllers.CreateSupplier)
		v1.GET(findSupplier, middlewares.TokenAuthMiddleware(), controllers.FindSupplier)
		v1.PATCH(updateSupplier, middlewares.TokenAuthMiddleware(), controllers.UpdateSupplier)
		v1.DELETE(deleteSupplier, middlewares.TokenAuthMiddleware(), controllers.DeleteSupplier)

		// user favorite
		v1.GET(listUserFavorite, middlewares.TokenAuthMiddleware(), controllers.ListUserFavorites)
		v1.POST(createUserFavorite, middlewares.TokenAuthMiddleware(), controllers.CreateUserFavorite)
		v1.GET(findUserFavorite, middlewares.TokenAuthMiddleware(), controllers.FindUserFavorite)
		v1.PATCH(updateUserFavorite, middlewares.TokenAuthMiddleware(), controllers.UpdateUserFavorite)
		v1.DELETE(deleteUserFavorite, middlewares.TokenAuthMiddleware(), controllers.DeleteUserFavorite)

		// product role
		v1.GET(listUserRole, middlewares.TokenAuthMiddleware(), controllers.ListUserRoles)
		v1.POST(createUserRole, middlewares.TokenAuthMiddleware(), controllers.CreateUserRole)
		v1.GET(findUserRole, middlewares.TokenAuthMiddleware(), controllers.FindUserRole)
		v1.PATCH(updateUserRole, middlewares.TokenAuthMiddleware(), controllers.UpdateUserRole)
		v1.DELETE(deleteUserRole, middlewares.TokenAuthMiddleware(), controllers.DeleteUserRole)
	}
}
