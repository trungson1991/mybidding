package model

import (
	"fmt"
	"time"

	G "Bidding_go/global"

	"gorm.io/gorm"
)

type Category struct {
	ID           uint           `json:"id" form:"id" gorm:"primary_key"`
	CategoryCode string         `json:"category_code" form:"category_code"`
	CategoryName string         `json:"category_name" form:"category_name"`
	Description  string         `json:"description" form:"description"`
	Image        string         `json:"image" form:"image"`
	CreatedAt    time.Time      `json:"create_time" form:"create_time"`
	UpdatedAt    time.Time      `json:"update_time" form:"update_time"`
	DeletedAt    gorm.DeletedAt `json:"delete_time" form:"delete_time"`
}

func ValidateCategory(object *Category) []string {
	strErrs := []string{}

	return strErrs
}

func (s *Server) ListCategory(sql string) ([]Category, []string) {
	strErrs := []string{}
	var items []Category
	err := s.DB.Debug().Raw(sql).Find(&items).Error

	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}

	return items, nil
}

func (s *Server) CreateCategory(object *Category, sql string) (*Category, []string) {
	strErrs := []string{}

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Create(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("CreatedAt", time.Now())
	return object, nil
}

func (s *Server) FindCategory(sql string) (*Category, []string) {
	strErrs := []string{}
	var item *Category
	if err := s.DB.Debug().Raw(sql).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	return item, nil
}

func (s *Server) UpdateCategory(object *Category, sql string) (*Category, []string) {
	strErrs := []string{}
	var item Category
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHCATEGORYSQL,
		object.ID,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	e := ValidateCategory(object)
	strErrs = append(strErrs, e...)

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Scan(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("UpdatedAt", time.Now())
	return object, nil
}

func (s *Server) DeleteCategory(id uint, sql string) (bool, []string) {
	var item *Category
	strErrs := []string{}
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHCATEGORYSQL,
		id,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return false, strErrs
	}

	err := s.DB.Debug().Delete(&item).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return false, strErrs
	}

	return true, nil
}
