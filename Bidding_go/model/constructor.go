package model

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/user"
	"reflect"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"

	G "Bidding_go/global"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

func ListOfErrors(obj interface{}, e error) map[string]string {
	ve, ok := e.(validator.ValidationErrors)
	InvalidFields := make(map[string]string)

	if !ok {
		InvalidFields[G.Error] = G.HandleDifferently
		return InvalidFields
	}

	for _, e := range ve {
		field, _ := reflect.TypeOf(obj).Elem().FieldByName(e.Field())
		jsonTag := string(field.Tag.Get(G.Json))
		InvalidFields[jsonTag] = e.Tag()
	}

	return InvalidFields
}

func ValidateDate(dateString string) (*time.Time, error) {

	dateStamp, err := time.Parse("2006-01-02 15:04:05", dateString)

	if err != nil {
		println(err.Error())
		return nil, err
	}
	return &dateStamp, nil
}

var imageFolderpath string = "/bidding_resource/"

type Sizer interface {
	Size() int64
}

// image formats and magic numbers
var magicTable = map[string]string{
	"\xff\xd8\xff":      "image/jpeg",
	"\x89PNG\r\n\x1a\n": "image/png",
	"GIF87a":            "image/gif",
	"GIF89a":            "image/gif",
}

func UploadImage(c *gin.Context) (*string, error) {
	r := c.Request
	file, header, err := r.FormFile("image")
	if err != nil {
		return nil, err
	}

	// Create a buffer to store the header of the file in
	fileHeader := make([]byte, 512)

	// Copy the headers into the FileHeader buffer
	if _, err := file.Read(fileHeader); err != nil {
		return nil, err
	}

	// set position back to start.
	if _, err := file.Seek(0, 0); err != nil {
		return nil, err
	}

	log.Printf("Name: %#v\n", header.Filename)
	log.Printf("Size: %#v\n", file.(Sizer).Size())
	log.Printf("MIME: %#v\n", http.DetectContentType(fileHeader))

	if !ValidTypeImage(http.DetectContentType(fileHeader)) {
		return nil, errors.New(G.ImageIsIncorrect)
	}

	if !ValidSizeImage(file.(Sizer).Size()) {
		return nil, errors.New(G.ImageIsTooLarge)
	}

	// convert file to size
	// wrapped := io.LimitReader(file, 10*1024*1024)

	// _, _, er := image.Decode(wrapped)
	// if er != nil {
	// 	log.Fatal(er)
	// 	return nil, er
	// }

	myself, error := user.Current()
	if error != nil {
		panic(error)
	}
	homedir := myself.HomeDir
	forderpath := homedir + imageFolderpath
	err1 := os.MkdirAll(forderpath, os.ModePerm)
	if err1 != nil {
		log.Println(err1)
	}
	filename := strconv.FormatInt(time.Now().Unix(), 10) + "_" + header.Filename
	filepath := forderpath + filename
	fmt.Println("imageUploadName: " + filepath)

	out, err := os.Create(filepath)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	defer out.Close()
	_, err = io.Copy(out, file)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	return &filename, nil
}

func ValidSizeImage(size int64) bool {
	var maxsize int64 = 10
	return size <= maxsize*1024*1024
}

func ValidTypeImage(mime string) bool {
	for _, m := range magicTable {
		if strings.HasPrefix(mime, m) {
			return true
		}
	}
	return false
}
