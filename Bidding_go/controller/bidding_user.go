package controllers

import (
	G "Bidding_go/global"
	"Bidding_go/model"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type CreateBiddingUserInput struct {
	EmployeeID uint `json:"employee_id" form:"employee_id"`
	BiddingID  uint `json:"bidding_id" form:"bidding_id"`
}

type UpdateBiddingUserInput struct {
	ID         uint `json:"id" form:"id" gorm:"primary_key"`
	EmployeeID uint `json:"employee_id" form:"employee_id"`
	BiddingID  uint `json:"bidding_id" form:"bidding_id"`
}

type GETBiddingUser struct {
	ID       uint          `json:"id" form:"id" gorm:"primary_key"`
	Employee model.User    `json:"employee" form:"employee"`
	Bidding  model.Bidding `json:"bidding" form:"bidding"`
}

// GET /biddingusers
// Get all biddingusers
func ListBiddingUsers(c *gin.Context) {
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	u, _ := strconv.Atoi(c.Query("user_id"))
	userIDParams := uint(u)
	userID := auth.UserID
	if userIDParams > 0 && userIDParams != auth.UserID {
		if !isPermission(c, auth) {
			return
		}
		userID = userIDParams
	}

	page, _ := strconv.Atoi(c.Query("page"))
	num, _ := strconv.Atoi(c.Query("page_num"))

	sql := fmt.Sprintf(G.LISTBIDDINGUSERSQL, userID, (page-1)*num, num)
	biddingusers, err := model.Model.ListBiddingUser(sql)
	if err != nil {
		status := http.StatusInternalServerError
		response := model.CreateResponse(status, err, nil)
		c.JSON(status, response)
		return
	}

	var getBiddingUser []GETBiddingUser
	for _, item := range biddingusers {
		sql := fmt.Sprintf(G.GETUSERBYIDSQL, item.EmployeeID)
		user, err := model.Model.GetUser(sql)

		if isServerError(c, err) {
			return
		}

		sql1 := fmt.Sprintf(G.FINDBIDDINGSQL, item.BiddingID)
		bidding, err := model.Model.FindBidding(sql1)
		if isServerError(c, err) {
			return
		}

		i := GETBiddingUser{
			ID:       item.ID,
			Employee: *user,
			Bidding:  *bidding,
		}

		getBiddingUser = append(getBiddingUser, i)
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, getBiddingUser)
	c.JSON(status, response)
}

// POST /biddingusers
// Create new biddinguser
func CreateBiddingUser(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	var input CreateBiddingUserInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.BiddingUser
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		fmt.Println(err)
		errors := []string{}
		for key, value := range e {
			errors = append(errors, key+" : "+value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	// Create biddinguser
	biddinguser := model.BiddingUser{
		EmployeeID: input.EmployeeID,
		BiddingID:  input.BiddingID,
	}

	sql := fmt.Sprintf(G.CREATEBIDDINGUSERSQL,
		biddinguser.EmployeeID,
		biddinguser.BiddingID,
	)

	data, err := model.Model.CreateBiddingUser(&biddinguser, sql)

	if err != nil {
		status := http.StatusInternalServerError
		response := model.CreateResponse(status, err, nil)
		c.JSON(status, response)
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, data)
	c.JSON(status, response)
}

// GET /biddingusers/:id
// Find a biddinguser
func FindBiddingUser(c *gin.Context) { // Get model if exist
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.Query("id"))
	if isBadRequestID(c, errID) {
		return
	}

	u, _ := strconv.Atoi(c.Query("user_id"))
	userIDParams := uint(u)
	userID := auth.UserID
	if userIDParams > 0 && userIDParams != auth.UserID {
		if !isPermission(c, auth) {
			return
		}
		userID = userIDParams
	}

	sql1 := fmt.Sprintf(G.FINDBIDDINGUSERSQL, i, userID)
	biddinguser, errF := model.Model.FindBiddingUser(sql1)
	if isBadRequest(c, errF) {
		return
	}

	sql2 := fmt.Sprintf(G.GETUSERBYIDSQL, biddinguser.EmployeeID)
	user, err := model.Model.GetUser(sql2)

	if err != nil {
		status := http.StatusInternalServerError
		response := model.CreateResponse(status, err, nil)
		c.JSON(status, response)
		return
	}

	sql3 := fmt.Sprintf(G.FINDBIDDINGSQL, biddinguser.BiddingID)
	bidding, err := model.Model.FindBidding(sql3)
	if err != nil {
		status := http.StatusInternalServerError
		response := model.CreateResponse(status, err, nil)
		c.JSON(status, response)
		return
	}

	res := GETBiddingUser{
		ID:       biddinguser.ID,
		Employee: *user,
		Bidding:  *bidding,
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, res)
	c.JSON(status, response)
}

// PATCH /biddingusers/:id
// Update a biddinguser
func UpdateBiddingUser(c *gin.Context) {
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	// Get model if exist
	var input UpdateBiddingUserInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.BiddingUser
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		errors := []string{}
		for _, value := range e {
			errors = append(errors, value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	userID := auth.UserID

	biddinguser := model.BiddingUser{
		ID:         input.ID,
		EmployeeID: input.EmployeeID,
		BiddingID:  input.BiddingID,
	}

	sql := fmt.Sprintf(G.UPDATEBIDDINGUSERSQL,
		biddinguser.EmployeeID,
		biddinguser.BiddingID,
		biddinguser.ID,
	)

	data, err := model.Model.UpdateBiddingUser(userID, &biddinguser, sql)
	if err != nil {
		status := http.StatusInternalServerError
		response := model.CreateResponse(status, err, nil)
		c.JSON(status, response)
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, data)
	c.JSON(status, response)
}

// DELETE /biddingusers/:id
// Delete a biddinguser
func DeleteBiddingUser(c *gin.Context) {
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.PostForm("id"))
	if isBadRequestID(c, errID) {
		return
	}

	u, _ := strconv.Atoi(c.PostForm("user_id"))
	userIDParams := uint(u)
	userID := auth.UserID
	if userIDParams > 0 && userIDParams != auth.UserID {
		if !isPermission(c, auth) {
			return
		}
		userID = userIDParams
	}

	sql := fmt.Sprintf(G.DELETEBIDDINGUSERSQL, i)
	success, errF := model.Model.DeleteBiddingUser(userID, uint(i), sql)
	if isBadRequest(c, errF) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, success)
	c.JSON(status, response)
}
