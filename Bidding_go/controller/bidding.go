package controllers

import (
	G "Bidding_go/global"
	"Bidding_go/model"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

type CreateBiddingInput struct {
	ProductID           uint      `json:"product_id" form:"product_id"`
	BiddingCode         string    `json:"bidding_code" form:"bidding_code"`
	BiddingUserWinnerID uint      `json:"bidding_user_winner_id" form:"bidding_user_winner_id"`
	BiddingTitle        string    `json:"bidding_title" form:"bidding_title"`
	Description         string    `json:"description" form:"description"`
	ShortDescription    string    `json:"short_description" form:"short_description"`
	StartTime           time.Time `json:"start_time" form:"start_time"`
	StopTime            time.Time `json:"stop_time" form:"stop_time"`
	NotifTime           time.Time `json:"notif_time" form:"notif_time"`
	Status              int       `json:"status" form:"status"`
}

type UpdateBiddingInput struct {
	ID                  uint      `json:"id" form:"id" gorm:"primary_key"`
	ProductID           uint      `json:"product_id" form:"product_id"`
	BiddingCode         string    `json:"bidding_code" form:"bidding_code"`
	BiddingUserWinnerID uint      `json:"bidding_user_winner_id" form:"bidding_user_winner_id"`
	BiddingTitle        string    `json:"bidding_title" form:"bidding_title"`
	Description         string    `json:"description" form:"description"`
	ShortDescription    string    `json:"short_description" form:"short_description"`
	StartTime           time.Time `json:"start_time" form:"start_time"`
	StopTime            time.Time `json:"stop_time" form:"stop_time"`
	NotifTime           time.Time `json:"notif_time" form:"notif_time"`
	Status              int       `json:"status" form:"status"`
}

type GETBidding struct {
	ID                uint          `json:"id" form:"id" gorm:"primary_key"`
	Product           model.Product `json:"product" form:"product"`
	BiddingCode       string        `json:"bidding_code" form:"bidding_code"`
	BiddingUserWinner model.User    `json:"bidding_user_winner" form:"bidding_user_winner"`
	BiddingTitle      string        `json:"bidding_title" form:"bidding_title"`
	Description       string        `json:"description" form:"description"`
	ShortDescription  string        `json:"short_description" form:"short_description"`
	StartTime         time.Time     `json:"start_time" form:"start_time"`
	StopTime          time.Time     `json:"stop_time" form:"stop_time"`
	NotifTime         time.Time     `json:"notif_time" form:"notif_time"`
	Status            int           `json:"status" form:"status"`
}

// GET /biddings
// Get all biddings
func ListBiddings(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	page, _ := strconv.Atoi(c.Query("page"))
	num, _ := strconv.Atoi(c.Query("page_num"))

	sql := fmt.Sprintf(G.LISTBIDDINGSQL, (page-1)*num, num)
	biddings, err := model.Model.ListBidding(sql)
	if err != nil {
		status := http.StatusInternalServerError
		response := model.CreateResponse(status, err, nil)
		c.JSON(status, response)
		return
	}

	var getBiddings []GETBidding
	for _, item := range biddings {
		sql := fmt.Sprintf(G.GETUSERBYIDSQL, item.BiddingUserWinnerID)
		user, err := model.Model.GetUser(sql)

		if isServerError(c, err) {
			return
		}

		sql1 := fmt.Sprintf(G.FINDPRODUCTSQL, item.ProductID)
		product, err := model.Model.FindProduct(sql1)
		if isServerError(c, err) {
			return
		}

		i := GETBidding{
			ID:                item.ID,
			Product:           *product,
			BiddingCode:       item.BiddingCode,
			BiddingUserWinner: *user,
			BiddingTitle:      item.BiddingTitle,
			Description:       item.Description,
			ShortDescription:  item.ShortDescription,
			StartTime:         item.StartTime,
			StopTime:          item.StopTime,
			NotifTime:         item.NotifTime,
			Status:            item.Status,
		}

		getBiddings = append(getBiddings, i)
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, getBiddings)
	c.JSON(status, response)
}

// POST /biddings
// Create new bidding
func CreateBidding(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	var input CreateBiddingInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.Bidding
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		fmt.Println(err)
		errors := []string{}
		for key, value := range e {
			errors = append(errors, key+" : "+value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	// Create bidding
	bidding := model.Bidding{
		ProductID:           input.ProductID,
		BiddingCode:         input.BiddingCode,
		BiddingUserWinnerID: input.BiddingUserWinnerID,
		BiddingTitle:        input.BiddingTitle,
		Description:         input.Description,
		ShortDescription:    input.ShortDescription,
		StartTime:           input.StartTime,
		StopTime:            input.StopTime,
		NotifTime:           input.NotifTime,
		Status:              input.Status,
	}

	sql := fmt.Sprintf(G.CREATEBIDDINGSQL,
		bidding.ProductID,
		bidding.BiddingCode,
		bidding.BiddingUserWinnerID,
		bidding.BiddingTitle,
		bidding.Description,
		bidding.ShortDescription,
		bidding.StartTime.Format(G.DateFormatSQL),
		bidding.StopTime.Format(G.DateFormatSQL),
		bidding.NotifTime.Format(G.DateFormatSQL),
		bidding.Status)
	data, err := model.Model.CreateBidding(&bidding, sql)

	if err != nil {
		status := http.StatusInternalServerError
		response := model.CreateResponse(status, err, nil)
		c.JSON(status, response)
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, data)
	c.JSON(status, response)
}

// GET /biddings/:id
// Find a bidding
func FindBidding(c *gin.Context) { // Get model if exist
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.Query("id"))
	if isBadRequestID(c, errID) {
		return
	}

	sql1 := fmt.Sprintf(G.FINDBIDDINGSQL, i)
	bidding, errF := model.Model.FindBidding(sql1)
	if isBadRequest(c, errF) {
		return
	}

	sql2 := fmt.Sprintf(G.GETUSERBYIDSQL, bidding.BiddingUserWinnerID)
	user, err := model.Model.GetUser(sql2)
	if isServerError(c, err) {
		return
	}

	sql3 := fmt.Sprintf(G.FINDPRODUCTSQL, bidding.ProductID)
	product, err := model.Model.FindProduct(sql3)
	if isServerError(c, err) {
		return
	}

	res := GETBidding{
		ID:                bidding.ID,
		Product:           *product,
		BiddingCode:       bidding.BiddingCode,
		BiddingUserWinner: *user,
		BiddingTitle:      bidding.BiddingTitle,
		Description:       bidding.Description,
		ShortDescription:  bidding.ShortDescription,
		StartTime:         bidding.StartTime,
		StopTime:          bidding.StopTime,
		NotifTime:         bidding.NotifTime,
		Status:            bidding.Status,
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, res)
	c.JSON(status, response)
}

// PATCH /biddings/:id
// Update a bidding
func UpdateBidding(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	// Get model if exist
	var input UpdateBiddingInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.Bidding
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		errors := []string{}
		for _, value := range e {
			errors = append(errors, value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	bidding := model.Bidding{
		ID:                  input.ID,
		ProductID:           input.ProductID,
		BiddingCode:         input.BiddingCode,
		BiddingUserWinnerID: input.BiddingUserWinnerID,
		BiddingTitle:        input.BiddingTitle,
		Description:         input.Description,
		ShortDescription:    input.ShortDescription,
		StartTime:           input.StartTime,
		StopTime:            input.StopTime,
		NotifTime:           input.NotifTime,
		Status:              input.Status,
	}

	sql := fmt.Sprintf(G.UPDATEBIDDINGSQL,
		bidding.ProductID,
		bidding.BiddingCode,
		bidding.BiddingUserWinnerID,
		bidding.BiddingTitle,
		bidding.Description,
		bidding.ShortDescription,
		bidding.StartTime,
		bidding.StopTime,
		bidding.NotifTime,
		bidding.Status,
		bidding.ID)
	_, err := model.Model.UpdateBidding(&bidding, sql)
	if err != nil {
		status := http.StatusInternalServerError
		response := model.CreateResponse(status, err, nil)
		c.JSON(status, response)
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, nil)
	c.JSON(status, response)
}

// DELETE /biddings/:id
// Delete a bidding
func DeleteBidding(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.PostForm("id"))
	if isBadRequestID(c, errID) {
		return
	}

	sql := fmt.Sprintf(G.DELETEBIDDINGSQL, i)
	success, errF := model.Model.DeleteBidding(uint(i), sql)
	if isBadRequest(c, errF) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, success)
	c.JSON(status, response)
}
