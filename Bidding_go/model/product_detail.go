package model

import (
	"fmt"
	"time"

	G "Bidding_go/global"

	"gorm.io/gorm"
)

type ProductDetail struct {
	ID         uint           `json:"id" form:"id" gorm:"primary_key"`
	ProductID  uint           `json:"product_id" form:"product_id"`
	ShopID     uint           `json:"shop_id" form:"shop_id"`
	EmployeeID uint           `json:"employee_id" form:"employee_id"`
	ImportDate string         `json:"import_date" form:"import_date"`
	Quantity   string         `json:"quantity" form:"quantity"`
	UnitPrice  string         `json:"unit_price" form:"unit_price"`
	Type       string         `json:"type" form:"type"`
	CreatedAt  time.Time      `json:"create_time" form:"create_time"`
	UpdatedAt  time.Time      `json:"update_time" form:"update_time"`
	DeletedAt  gorm.DeletedAt `json:"delete_time" form:"delete_time"`
}

func ValidateProductDetail(object *ProductDetail) []string {
	strErrs := []string{}

	return strErrs
}

func (s *Server) ListProductDetail(sql string) ([]ProductDetail, []string) {
	strErrs := []string{}
	var items []ProductDetail
	err := s.DB.Debug().Raw(sql).Find(&items).Error

	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}

	return items, nil
}

func (s *Server) CreateProductDetail(object *ProductDetail, sql string) (*ProductDetail, []string) {
	strErrs := []string{}

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Create(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("CreatedAt", time.Now())
	return object, nil
}

func (s *Server) FindProductDetail(sql string) (*ProductDetail, []string) {
	strErrs := []string{}
	var item *ProductDetail
	if err := s.DB.Debug().Raw(sql).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	return item, nil
}

func (s *Server) UpdateProductDetail(object *ProductDetail, sql string) (*ProductDetail, []string) {
	strErrs := []string{}
	var item ProductDetail
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHPRODUCTDETAILSQL,
		object.ID,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	e := ValidateProductDetail(object)
	strErrs = append(strErrs, e...)

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Scan(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("UpdatedAt", time.Now())
	return object, nil
}

func (s *Server) DeleteProductDetail(id uint, sql string) (bool, []string) {
	var item *ProductDetail
	strErrs := []string{}
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHPRODUCTDETAILSQL,
		id,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return false, strErrs
	}

	err := s.DB.Debug().Delete(&item).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return false, strErrs
	}

	return true, nil
}
