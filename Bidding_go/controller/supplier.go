package controllers

import (
	G "Bidding_go/global"
	"Bidding_go/model"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type CreateSupplierInput struct {
	SupplierCode string `json:"supplier_code" form:"supplier_code"`
	SupplierName string `json:"supplier_name" form:"supplier_name"`
	Description  string `json:"description" form:"description"`
}

type UpdateSupplierInput struct {
	ID           uint   `json:"id" form:"id" gorm:"primary_key"`
	SupplierCode string `json:"supplier_code" form:"supplier_code"`
	SupplierName string `json:"supplier_name" form:"supplier_name"`
	Description  string `json:"description" form:"description"`
}

// GET /suppliers
// Get all suppliers
func ListSuppliers(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	page, _ := strconv.Atoi(c.Query("page"))
	num, _ := strconv.Atoi(c.Query("page_num"))

	sql := fmt.Sprintf(G.LISTSUPPLIERSQL, (page-1)*num, num)
	suppliers, err := model.Model.ListSupplier(sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, suppliers)
	c.JSON(status, response)
}

// POST /suppliers
// Create new supplier
func CreateSupplier(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	var input CreateSupplierInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.Supplier
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		fmt.Println(err)
		errors := []string{}
		for key, value := range e {
			errors = append(errors, key+" : "+value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	fileName, errUpload := model.UploadImage(c)
	var strErrUpload string
	if errUpload != nil {
		strErrUpload = errUpload.Error()
	}

	var imageName string = ""
	if fileName != nil {
		imageName = *fileName
	}

	// Create supplier
	supplier := model.Supplier{
		SupplierCode: input.SupplierCode,
		SupplierName: input.SupplierName,
		Description:  input.Description,
		Image:        imageName,
	}

	sql := fmt.Sprintf(G.CREATESUPPLIERSQL,
		supplier.SupplierCode,
		supplier.SupplierName,
		supplier.Description,
		supplier.Image,
	)

	data, err := model.Model.CreateSupplier(&supplier, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	var response interface{}
	if len(strErrUpload) > 0 {
		err2 := []string{strErrUpload}
		response = model.CreateResponse(status, err2, data)
	} else {
		response = model.CreateResponse(status, nil, data)
	}

	c.JSON(status, response)
}

// GET /suppliers/:id
// Find a supplier
func FindSupplier(c *gin.Context) { // Get model if exist
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.Query("id"))
	if isBadRequestID(c, errID) {
		return
	}

	sql := fmt.Sprintf(G.FINDSUPPLIERSQL, i)
	supplier, errF := model.Model.FindSupplier(sql)
	if isBadRequest(c, errF) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, supplier)
	c.JSON(status, response)
}

// PATCH /suppliers/:id
// Update a supplier
func UpdateSupplier(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	// Get model if exist
	var input UpdateSupplierInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.Supplier
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		errors := []string{}
		for _, value := range e {
			errors = append(errors, value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	fileName, errUpload := model.UploadImage(c)
	var strErrUpload string
	if errUpload != nil {
		strErrUpload = errUpload.Error()
	}

	var imageName string = ""
	if fileName != nil {
		imageName = *fileName
	}

	supplier := model.Supplier{
		ID:           input.ID,
		SupplierCode: input.SupplierCode,
		SupplierName: input.SupplierName,
		Description:  input.Description,
		Image:        imageName,
	}

	sql := fmt.Sprintf(G.UPDATESUPPLIERSQL,
		supplier.SupplierCode,
		supplier.SupplierName,
		supplier.Description,
		supplier.Image,
		supplier.ID,
	)

	data, err := model.Model.UpdateSupplier(&supplier, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	var response interface{}
	if len(strErrUpload) > 0 {
		err2 := []string{strErrUpload}
		response = model.CreateResponse(status, err2, data)
	} else {
		response = model.CreateResponse(status, nil, data)
	}
	c.JSON(status, response)
}

// DELETE /suppliers/:id
// Delete a supplier
func DeleteSupplier(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.PostForm("id"))
	if isBadRequestID(c, errID) {
		return
	}

	sql := fmt.Sprintf(G.DELETESUPPLIERSQL, i)
	success, errF := model.Model.DeleteSupplier(uint(i), sql)
	if isBadRequest(c, errF) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, success)
	c.JSON(status, response)
}
