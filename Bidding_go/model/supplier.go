package model

import (
	"fmt"
	"time"

	G "Bidding_go/global"

	"gorm.io/gorm"
)

type Supplier struct {
	ID           uint           `json:"id" form:"id" gorm:"primary_key"`
	SupplierCode string         `json:"supplier_code" form:"supplier_code"`
	SupplierName string         `json:"supplier_name" form:"supplier_name"`
	Description  string         `json:"description" form:"description"`
	Image        string         `json:"image" form:"image"`
	CreatedAt    time.Time      `json:"create_time" form:"create_time"`
	UpdatedAt    time.Time      `json:"update_time" form:"update_time"`
	DeletedAt    gorm.DeletedAt `json:"delete_time" form:"delete_time"`
}

func ValidateSupplier(object *Supplier) []string {
	strErrs := []string{}

	return strErrs
}

func (s *Server) ListSupplier(sql string) ([]Supplier, []string) {
	strErrs := []string{}
	var items []Supplier
	err := s.DB.Debug().Raw(sql).Find(&items).Error

	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}

	return items, nil
}

func (s *Server) CreateSupplier(object *Supplier, sql string) (*Supplier, []string) {
	strErrs := []string{}

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Create(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("CreatedAt", time.Now())
	return object, nil
}

func (s *Server) FindSupplier(sql string) (*Supplier, []string) {
	strErrs := []string{}
	var item *Supplier
	if err := s.DB.Debug().Raw(sql).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	return item, nil
}

func (s *Server) UpdateSupplier(object *Supplier, sql string) (*Supplier, []string) {
	strErrs := []string{}
	var item Supplier
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHSUPPLIERSQL,
		object.ID,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	e := ValidateSupplier(object)
	strErrs = append(strErrs, e...)

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Scan(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("UpdatedAt", time.Now())
	return object, nil
}

func (s *Server) DeleteSupplier(id uint, sql string) (bool, []string) {
	var item *Supplier
	strErrs := []string{}
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHSUPPLIERSQL,
		id,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return false, strErrs
	}

	err := s.DB.Debug().Delete(&item).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return false, strErrs
	}

	return true, nil
}
