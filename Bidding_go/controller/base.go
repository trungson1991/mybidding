package controllers

import (
	"Bidding_go/auth"
	G "Bidding_go/global"
	"Bidding_go/model"
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
)

func GetAuthWith(request *http.Request) (*model.Auth, error) {
	au, err := auth.ExtractTokenAuth(request)
	if err != nil {
		return nil, errors.New(G.Unauthorized)
	}

	foundAuth, err := model.Model.FetchAuth(au)
	if err != nil {
		return nil, errors.New(G.Unauthorized)
	}

	return foundAuth, nil
}

func CheckPermission(userToken string) bool {
	u, err := model.Model.GetUserWith(userToken)
	if err != nil {
		return false
	}

	if u.Role > 1 {
		return false
	}

	return true
}

func isServerError(c *gin.Context, err []string) bool {
	if err != nil {
		status := http.StatusInternalServerError
		response := model.CreateResponse(status, err, nil)
		c.JSON(status, response)
		return true
	}
	return false
}

func isPermission(c *gin.Context, auth *model.Auth) bool {
	if !CheckPermission(auth.AuthUUID) {
		status := http.StatusForbidden
		response := model.CreateResponse(status, []string{}, nil)
		c.JSON(status, response)
		return false
	}
	return true
}

func isBadRequest(c *gin.Context, err []string) bool {
	if err != nil {
		status := http.StatusBadRequest
		response := model.CreateResponse(status, err, nil)
		c.JSON(status, response)
		return true
	}
	return false
}

func isUnauthorized(c *gin.Context, err error) bool {
	if err != nil {
		status := http.StatusUnauthorized
		response := model.CreateResponse(status, []string{G.Unauthorized}, nil)
		c.JSON(status, response)
		return false
	}
	return true
}

func isBadRequestID(c *gin.Context, err error) bool {
	if err != nil {
		status := http.StatusBadRequest
		response := model.CreateResponse(status, []string{err.Error()}, nil)
		c.JSON(status, response)
		return true
	}
	return false
}

func Int(i interface{}) int {
	switch v := i.(type) {
	case int:
		return v
	}
	return 0
}
