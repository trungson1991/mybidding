package model

import (
	"fmt"
	"time"

	G "Bidding_go/global"

	"gorm.io/gorm"
)

type UserRole struct {
	ID        uint           `json:"id" form:"id" gorm:"primary_key"`
	UserID    uint           `json:"user_id" form:"user_id"`
	RoleID    uint           `json:"role_id" form:"role_id"`
	CreatedAt time.Time      `json:"create_time" form:"create_time"`
	UpdatedAt time.Time      `json:"update_time" form:"update_time"`
	DeletedAt gorm.DeletedAt `json:"delete_time" form:"delete_time"`
}

func ValidateUserRole(object *UserRole) []string {
	strErrs := []string{}

	return strErrs
}

func (s *Server) ListUserRole(sql string) ([]UserRole, []string) {
	strErrs := []string{}
	var items []UserRole
	err := s.DB.Debug().Raw(sql).Find(&items).Error

	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}

	return items, nil
}

func (s *Server) CreateUserRole(object *UserRole, sql string) (*UserRole, []string) {
	strErrs := []string{}

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Create(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("CreatedAt", time.Now())
	return object, nil
}

func (s *Server) FindUserRole(sql string) (*UserRole, []string) {
	strErrs := []string{}
	var item *UserRole
	if err := s.DB.Debug().Raw(sql).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	return item, nil
}

func (s *Server) UpdateUserRole(userID uint, object *UserRole, sql string) (*UserRole, []string) {
	strErrs := []string{}
	var item UserRole
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHUSERROLESQL,
		object.ID,
		userID,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	e := ValidateUserRole(object)
	strErrs = append(strErrs, e...)

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Scan(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("UpdatedAt", time.Now())
	return object, nil
}

func (s *Server) DeleteUserRole(userID uint, id uint, sql string) (bool, []string) {
	var item *UserRole
	strErrs := []string{}
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHBIDDINGHISTORYSQL,
		id,
		userID,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return false, strErrs
	}

	err := s.DB.Debug().Delete(&item).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return false, strErrs
	}

	return true, nil
}
