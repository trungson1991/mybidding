package model

import (
	"encoding/json"
	"fmt"
	"time"

	G "Bidding_go/global"

	"gorm.io/gorm"
)

type ProductReview struct {
	ID        uint           `json:"id" form:"id" gorm:"primary_key"`
	ProductID uint           `json:"product_id" form:"product_id"`
	UserID    uint           `json:"user_id" form:"user_id"`
	Rating    json.Number    `json:"rating" form:"rating"`
	Comment   string         `json:"comment" form:"comment"`
	CreatedAt time.Time      `json:"create_time" form:"create_time"`
	UpdatedAt time.Time      `json:"update_time" form:"update_time"`
	DeletedAt gorm.DeletedAt `json:"delete_time" form:"delete_time"`
}

func ValidateProductReview(object *ProductReview) []string {
	strErrs := []string{}

	return strErrs
}

func (s *Server) ListProductReview(sql string) ([]ProductReview, []string) {
	strErrs := []string{}
	var items []ProductReview
	err := s.DB.Debug().Raw(sql).Find(&items).Error

	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}

	return items, nil
}

func (s *Server) CreateProductReview(object *ProductReview, sql string) (*ProductReview, []string) {
	strErrs := []string{}

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Create(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("CreatedAt", time.Now())
	return object, nil
}

func (s *Server) FindProductReview(sql string) (*ProductReview, []string) {
	strErrs := []string{}
	var item *ProductReview
	if err := s.DB.Debug().Raw(sql).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	return item, nil
}

func (s *Server) UpdateProductReview(object *ProductReview, sql string) (*ProductReview, []string) {
	strErrs := []string{}
	var item ProductReview
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHPRODUCTREVIEWSQL,
		object.ID,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	e := ValidateProductReview(object)
	strErrs = append(strErrs, e...)

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Scan(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("UpdatedAt", time.Now())
	return object, nil
}

func (s *Server) DeleteProductReview(id uint, sql string) (bool, []string) {
	var item *ProductReview
	strErrs := []string{}
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHPRODUCTREVIEWSQL,
		id,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return false, strErrs
	}

	err := s.DB.Debug().Delete(&item).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return false, strErrs
	}

	return true, nil
}
