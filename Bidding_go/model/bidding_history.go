package model

import (
	"fmt"
	"time"

	G "Bidding_go/global"

	"gorm.io/gorm"
)

type BiddingHistory struct {
	ID            uint           `json:"id" form:"id" gorm:"primary_key"`
	BiddingID     uint           `json:"bidding_id" form:"bidding_id"`
	BiddingUserID uint           `json:"bidding_user_id" form:"bidding_user_id"`
	CostOrder     string         `json:"cost_order" form:"cost_order"`
	Status        int            `json:"status" form:"status"`
	CreatedAt     time.Time      `json:"create_time" form:"create_time"`
	UpdatedAt     time.Time      `json:"update_time" form:"update_time"`
	DeletedAt     gorm.DeletedAt `json:"delete_time" form:"delete_time"`
}

func ValidateBiddingHistory(object *BiddingHistory) []string {
	strErrs := []string{}

	return strErrs
}

func (s *Server) ListBiddingHistory(sql string) ([]BiddingHistory, []string) {
	strErrs := []string{}
	var items []BiddingHistory
	err := s.DB.Debug().Raw(sql).Find(&items).Error

	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}

	return items, nil
}

func (s *Server) CreateBiddingHistory(object *BiddingHistory, sql string) (*BiddingHistory, []string) {
	strErrs := []string{}

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Create(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("CreatedAt", time.Now())
	return object, nil
}

func (s *Server) FindBiddingHistory(sql string) (*BiddingHistory, []string) {
	strErrs := []string{}
	var item *BiddingHistory
	if err := s.DB.Debug().Raw(sql).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	return item, nil
}

func (s *Server) UpdateBiddingHistory(userID uint, object *BiddingHistory, sql string) (*BiddingHistory, []string) {
	strErrs := []string{}
	var item BiddingHistory
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHBIDDINGHISTORYSQL,
		object.ID,
		userID,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	e := ValidateBiddingHistory(object)
	strErrs = append(strErrs, e...)

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Scan(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("UpdatedAt", time.Now())
	return object, nil
}

func (s *Server) DeleteBiddingHistory(userID uint, id uint, sql string) (bool, []string) {
	var item *BiddingHistory
	strErrs := []string{}
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHBIDDINGHISTORYSQL,
		id,
		userID,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return false, strErrs
	}

	err := s.DB.Debug().Delete(&item).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return false, strErrs
	}

	return true, nil
}
