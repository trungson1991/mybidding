/*
 Navicat Premium Data Transfer

 Source Server         : db
 Source Server Type    : MySQL
 Source Server Version : 80032 (8.0.32)
 Source Host           : localhost:3306
 Source Schema         : bidding

 Target Server Type    : MySQL
 Target Server Version : 80032 (8.0.32)
 File Encoding         : 65001

 Date: 31/08/2023 08:19:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_bidding
-- ----------------------------
DROP TABLE IF EXISTS `tbl_bidding`;
CREATE TABLE `tbl_bidding` (
  `id` bigint DEFAULT NULL,
  `product_id` bigint DEFAULT NULL,
  `bidding_code` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `bidding_user_winner_id` bigint DEFAULT NULL,
  `bidding_title` text,
  `description` text,
  `short_description` text,
  `start_time` datetime DEFAULT NULL,
  `stop_time` datetime DEFAULT NULL,
  `notif_time` timestamp NULL DEFAULT NULL,
  `status` tinyint DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `product_id` (`product_id`),
  KEY `id` (`id`),
  KEY `bidding_user_winner_id` (`bidding_user_winner_id`),
  CONSTRAINT `tbl_bidding_ibfk_1` FOREIGN KEY (`id`) REFERENCES `tbl_bidding_history` (`bidding_id`),
  CONSTRAINT `tbl_bidding_ibfk_2` FOREIGN KEY (`id`) REFERENCES `tbl_bidding_user` (`bidding_id`),
  CONSTRAINT `tbl_bidding_ibfk_3` FOREIGN KEY (`id`) REFERENCES `tbl_shop_order` (`bidding_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tbl_bidding
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tbl_bidding_history
-- ----------------------------
DROP TABLE IF EXISTS `tbl_bidding_history`;
CREATE TABLE `tbl_bidding_history` (
  `id` bigint DEFAULT NULL,
  `bidding_id` bigint DEFAULT NULL,
  `bidding_user_id` bigint DEFAULT NULL,
  `cost_order` decimal(20,2) DEFAULT NULL,
  `status` tinyint DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `shop_order_id` (`bidding_id`),
  KEY `employee_id` (`bidding_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tbl_bidding_history
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tbl_bidding_user
-- ----------------------------
DROP TABLE IF EXISTS `tbl_bidding_user`;
CREATE TABLE `tbl_bidding_user` (
  `id` bigint DEFAULT NULL,
  `employee_id` bigint DEFAULT NULL,
  `bidding_id` bigint DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `employee_id` (`employee_id`),
  KEY `bidding_id` (`bidding_id`),
  KEY `id` (`id`),
  CONSTRAINT `tbl_bidding_user_ibfk_1` FOREIGN KEY (`id`) REFERENCES `tbl_bidding_history` (`bidding_user_id`),
  CONSTRAINT `tbl_bidding_user_ibfk_2` FOREIGN KEY (`id`) REFERENCES `tbl_bidding` (`bidding_user_winner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tbl_bidding_user
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tbl_role
-- ----------------------------
DROP TABLE IF EXISTS `tbl_role`;
CREATE TABLE `tbl_role` (
  `id` bigint DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tbl_role
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tbl_shop
-- ----------------------------
DROP TABLE IF EXISTS `tbl_shop`;
CREATE TABLE `tbl_shop` (
  `id` bigint DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `shop_name` varchar(255) DEFAULT NULL,
  `description` text,
  `image` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `id` (`id`),
  CONSTRAINT `tbl_shop_ibfk_1` FOREIGN KEY (`id`) REFERENCES `tbl_shop_product_detail` (`shop_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tbl_shop
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tbl_shop_category
-- ----------------------------
DROP TABLE IF EXISTS `tbl_shop_category`;
CREATE TABLE `tbl_shop_category` (
  `id` bigint DEFAULT NULL,
  `category_code` varchar(255) DEFAULT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `description` text,
  `image` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `id` (`id`),
  CONSTRAINT `tbl_shop_category_ibfk_1` FOREIGN KEY (`id`) REFERENCES `tbl_shop_product` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tbl_shop_category
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tbl_shop_order
-- ----------------------------
DROP TABLE IF EXISTS `tbl_shop_order`;
CREATE TABLE `tbl_shop_order` (
  `id` bigint DEFAULT NULL,
  `employee_id` bigint DEFAULT NULL,
  `customer_id` bigint DEFAULT NULL,
  `bidding_id` bigint DEFAULT NULL,
  `order_date` datetime DEFAULT NULL,
  `shipped_date` datetime DEFAULT NULL,
  `ship_name` varchar(255) DEFAULT NULL,
  `ship_address1` varchar(255) DEFAULT NULL,
  `ship_address2` varchar(255) DEFAULT NULL,
  `ship_city` varchar(255) DEFAULT NULL,
  `ship_state` varchar(255) DEFAULT NULL,
  `ship_postal_code` varchar(255) DEFAULT NULL,
  `ship_country` varchar(255) DEFAULT NULL,
  `shipping_fee` varchar(255) DEFAULT NULL,
  `payment_type_id` bigint DEFAULT NULL,
  `paid_date` datetime DEFAULT NULL,
  `order_status` varchar(255) DEFAULT NULL,
  `type_order` tinyint DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `payment_type_id` (`payment_type_id`),
  KEY `customer_id` (`customer_id`),
  KEY `emplotee_id` (`employee_id`),
  KEY `id` (`id`),
  KEY `bidding_id` (`bidding_id`),
  CONSTRAINT `tbl_shop_order_ibfk_1` FOREIGN KEY (`id`) REFERENCES `tbl_shop_order_detail` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tbl_shop_order
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tbl_shop_order_detail
-- ----------------------------
DROP TABLE IF EXISTS `tbl_shop_order_detail`;
CREATE TABLE `tbl_shop_order_detail` (
  `id` bigint DEFAULT NULL,
  `order_id` bigint DEFAULT NULL,
  `product_id` bigint DEFAULT NULL,
  `quantity` decimal(20,2) DEFAULT NULL,
  `unit_price` decimal(20,2) DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `status` tinyint DEFAULT NULL,
  `date_allocated` datetime DEFAULT NULL,
  KEY `product_id` (`product_id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tbl_shop_order_detail
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tbl_shop_payment_type
-- ----------------------------
DROP TABLE IF EXISTS `tbl_shop_payment_type`;
CREATE TABLE `tbl_shop_payment_type` (
  `id` bigint DEFAULT NULL,
  `payment_code` varchar(255) DEFAULT NULL,
  `payment_name` varchar(255) DEFAULT NULL,
  `description` text,
  `image` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `id` (`id`),
  CONSTRAINT `tbl_shop_payment_type_ibfk_1` FOREIGN KEY (`id`) REFERENCES `tbl_shop_order` (`payment_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tbl_shop_payment_type
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tbl_shop_product
-- ----------------------------
DROP TABLE IF EXISTS `tbl_shop_product`;
CREATE TABLE `tbl_shop_product` (
  `id` bigint DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `image` text,
  `short_description` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `quantity_per_unit` varchar(50) DEFAULT NULL,
  `is_featured` bit(1) DEFAULT NULL,
  `is_new` bit(1) DEFAULT NULL,
  `category_id` bigint DEFAULT NULL,
  `supplier_id` bigint DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `category_id` (`category_id`),
  KEY `supplier_id` (`supplier_id`),
  KEY `id` (`id`),
  CONSTRAINT `tbl_shop_product_ibfk_1` FOREIGN KEY (`id`) REFERENCES `tbl_shop_product_detail` (`product_id`),
  CONSTRAINT `tbl_shop_product_ibfk_2` FOREIGN KEY (`id`) REFERENCES `tbl_shop_product_review` (`product_id`),
  CONSTRAINT `tbl_shop_product_ibfk_3` FOREIGN KEY (`id`) REFERENCES `tbl_shop_order_detail` (`product_id`),
  CONSTRAINT `tbl_shop_product_ibfk_5` FOREIGN KEY (`id`) REFERENCES `tbl_user_favorite` (`product_id`),
  CONSTRAINT `tbl_shop_product_ibfk_6` FOREIGN KEY (`id`) REFERENCES `tbl_bidding` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tbl_shop_product
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tbl_shop_product_detail
-- ----------------------------
DROP TABLE IF EXISTS `tbl_shop_product_detail`;
CREATE TABLE `tbl_shop_product_detail` (
  `int` bigint DEFAULT NULL,
  `product_id` bigint DEFAULT NULL,
  `shop_id` bigint DEFAULT NULL,
  `employee_id` bigint DEFAULT NULL,
  `import_date` datetime DEFAULT NULL,
  `quantity` decimal(20,2) DEFAULT NULL,
  `unit_price` decimal(20,2) DEFAULT NULL,
  `type` tinyint DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `shop_id` (`shop_id`),
  KEY `employee_id` (`employee_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tbl_shop_product_detail
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tbl_shop_product_review
-- ----------------------------
DROP TABLE IF EXISTS `tbl_shop_product_review`;
CREATE TABLE `tbl_shop_product_review` (
  `id` bigint DEFAULT NULL,
  `product_id` bigint DEFAULT NULL,
  `user_id` bigint DEFAULT NULL,
  `rating` float DEFAULT NULL,
  `comment` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `product_id` (`product_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tbl_shop_product_review
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tbl_shop_supplier
-- ----------------------------
DROP TABLE IF EXISTS `tbl_shop_supplier`;
CREATE TABLE `tbl_shop_supplier` (
  `id` bigint DEFAULT NULL,
  `supplier_code` varchar(255) DEFAULT NULL,
  `supplier_name` varchar(255) DEFAULT NULL,
  `description` text,
  `image` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `id` (`id`),
  CONSTRAINT `tbl_shop_supplier_ibfk_1` FOREIGN KEY (`id`) REFERENCES `tbl_shop_product` (`supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tbl_shop_supplier
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tbl_user
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `id` bigint DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `gender` tinyint DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `address1` varchar(500) DEFAULT NULL,
  `address2` varchar(500) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `portal_code` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `active_code` varchar(255) DEFAULT NULL,
  `status` tinyint DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  KEY `id` (`id`),
  CONSTRAINT `tbl_user_ibfk_1` FOREIGN KEY (`id`) REFERENCES `tbl_user_role` (`user_id`),
  CONSTRAINT `tbl_user_ibfk_2` FOREIGN KEY (`id`) REFERENCES `tbl_shop_product_detail` (`employee_id`),
  CONSTRAINT `tbl_user_ibfk_3` FOREIGN KEY (`id`) REFERENCES `tbl_shop_order` (`employee_id`),
  CONSTRAINT `tbl_user_ibfk_5` FOREIGN KEY (`id`) REFERENCES `tbl_user_favorite` (`user_id`),
  CONSTRAINT `tbl_user_ibfk_6` FOREIGN KEY (`id`) REFERENCES `tbl_shop_product_review` (`user_id`),
  CONSTRAINT `tbl_user_ibfk_7` FOREIGN KEY (`id`) REFERENCES `tbl_bidding_user` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tbl_user_admin
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_admin`;
CREATE TABLE `tbl_user_admin` (
  `id` bigint DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `avatar` text,
  `role_id` bigint DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tbl_user_admin
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tbl_user_favorite
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_favorite`;
CREATE TABLE `tbl_user_favorite` (
  `id` bigint DEFAULT NULL,
  `user_id` bigint DEFAULT NULL,
  `product_id` bigint DEFAULT NULL,
  `status` tinyint DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `user_id` (`user_id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tbl_user_favorite
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tbl_user_role
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_role`;
CREATE TABLE `tbl_user_role` (
  `id` bigint DEFAULT NULL,
  `user_id` bigint DEFAULT NULL,
  `role_id` bigint DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `user_id` (`user_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `tbl_user_role_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `tbl_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tbl_user_role
-- ----------------------------
BEGIN;
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
