package controllers

import (
	G "Bidding_go/global"
	"Bidding_go/model"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type CreateProductInput struct {
	ProductCode     string `json:"product_code" form:"product_code"`
	ProductName     string `json:"product_name" form:"product_name"`
	ShotDescription string `json:"short_description" form:"short_description"`
	Description     string `json:"description" form:"description"`
	QuantityPerUnit string `json:"quantity_per_unit" form:"quantity_per_unit"`
	IsFeatured      int    `json:"is_featured" form:"is_featured"`
	IsNew           int    `json:"is_new" form:"is_new"`
	CategoryID      uint   `json:"category_id" form:"category_id"`
	SupplierID      uint   `json:"supplier_id" form:"supplier_id"`
}

type UpdateProductInput struct {
	ID              uint   `json:"id" form:"id" gorm:"primary_key"`
	ProductCode     string `json:"product_code" form:"product_code"`
	ProductName     string `json:"product_name" form:"product_name"`
	ShotDescription string `json:"short_description" form:"short_description"`
	Description     string `json:"description" form:"description"`
	QuantityPerUnit string `json:"quantity_per_unit" form:"quantity_per_unit"`
	IsFeatured      int    `json:"is_featured" form:"is_featured"`
	IsNew           int    `json:"is_new" form:"is_new"`
	CategoryID      uint   `json:"category_id" form:"category_id"`
	SupplierID      uint   `json:"supplier_id" form:"supplier_id"`
}

type GETProduct struct {
	ID              uint            `json:"id" form:"id" gorm:"primary_key"`
	ProductCode     string          `json:"product_code" form:"product_code"`
	ProductName     string          `json:"product_name" form:"product_name"`
	Image           string          `json:"image" form:"image"`
	ShotDescription string          `json:"short_description" form:"short_description"`
	Description     string          `json:"description" form:"description"`
	QuantityPerUnit string          `json:"quantity_per_unit" form:"quantity_per_unit"`
	IsFeatured      int             `json:"is_featured" form:"is_featured"`
	IsNew           int             `json:"is_new" form:"is_new"`
	Category        *model.Category `json:"category" form:"category"`
	Supplier        *model.Supplier `json:"supplier" form:"supplier"`
}

// GET /products
// Get all products
func ListProducts(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	page, _ := strconv.Atoi(c.Query("page"))
	num, _ := strconv.Atoi(c.Query("page_num"))

	sql := fmt.Sprintf(G.LISTPRODUCTSQL, (page-1)*num, num)
	products, err := model.Model.ListProduct(sql)
	if isServerError(c, err) {
		return
	}

	var getProducts []GETProduct
	for _, item := range products {
		sql1 := fmt.Sprintf(G.FINDCATEGORYSQL, item.CategoryID)
		category, err := model.Model.FindCategory(sql1)
		if err != nil {
			category = nil
		}

		sql2 := fmt.Sprintf(G.FINDSUPPLIERSQL, item.SupplierID)
		supplier, err := model.Model.FindSupplier(sql2)
		if err != nil {
			supplier = nil
		}

		i := GETProduct{
			ID:              item.ID,
			ProductCode:     item.ProductCode,
			ProductName:     item.ProductName,
			Image:           item.Image,
			ShotDescription: item.ShotDescription,
			Description:     item.Description,
			QuantityPerUnit: item.QuantityPerUnit,
			IsFeatured:      item.IsFeatured,
			IsNew:           item.IsNew,
			Category:        category,
			Supplier:        supplier,
		}

		getProducts = append(getProducts, i)
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, getProducts)
	c.JSON(status, response)
}

// POST /products
// Create new product
func CreateProduct(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	var input CreateProductInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.Product
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		fmt.Println(err)
		errors := []string{}
		for key, value := range e {
			errors = append(errors, key+" : "+value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	fileName, errUpload := model.UploadImage(c)
	var strErrUpload string
	if errUpload != nil {
		strErrUpload = errUpload.Error()
	}

	var imageName string = ""
	if fileName != nil {
		imageName = *fileName
	}

	// Create product
	product := model.Product{
		ProductCode:     input.ProductCode,
		ProductName:     input.ProductName,
		Image:           imageName,
		ShotDescription: input.ShotDescription,
		Description:     input.Description,
		QuantityPerUnit: input.QuantityPerUnit,
		IsFeatured:      input.IsFeatured,
		IsNew:           input.IsNew,
		CategoryID:      input.CategoryID,
		SupplierID:      input.SupplierID,
	}

	sql := fmt.Sprintf(G.CREATEPRODUCTSQL,
		product.ProductCode,
		product.ProductName,
		product.Image,
		product.ShotDescription,
		product.Description,
		product.QuantityPerUnit,
		product.IsFeatured,
		product.IsNew,
		product.CategoryID,
		product.SupplierID,
	)

	data, err := model.Model.CreateProduct(&product, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	var response interface{}
	if len(strErrUpload) > 0 {
		err2 := []string{strErrUpload}
		response = model.CreateResponse(status, err2, data)
	} else {
		response = model.CreateResponse(status, nil, data)
	}
	c.JSON(status, response)
}

// GET /products/:id
// Find a product
func FindProduct(c *gin.Context) { // Get model if exist
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.Query("id"))
	if isBadRequestID(c, errID) {
		return
	}

	sql1 := fmt.Sprintf(G.FINDPRODUCTSQL, i)
	product, errF := model.Model.FindProduct(sql1)
	if isBadRequest(c, errF) {
		return
	}

	sql2 := fmt.Sprintf(G.FINDCATEGORYSQL, product.CategoryID)
	category, err := model.Model.FindCategory(sql2)
	if err != nil {
		category = nil
	}

	sql3 := fmt.Sprintf(G.FINDSUPPLIERSQL, product.SupplierID)
	supplier, err := model.Model.FindSupplier(sql3)
	if err != nil {
		supplier = nil
	}

	res := GETProduct{
		ID:              product.ID,
		ProductCode:     product.ProductCode,
		ProductName:     product.ProductName,
		Image:           product.Image,
		ShotDescription: product.ShotDescription,
		Description:     product.Description,
		QuantityPerUnit: product.QuantityPerUnit,
		IsFeatured:      product.IsFeatured,
		IsNew:           product.IsNew,
		Category:        category,
		Supplier:        supplier,
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, res)
	c.JSON(status, response)
}

// PATCH /products/:id
// Update a product
func UpdateProduct(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	// Get model if exist
	var input UpdateProductInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.Product
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		errors := []string{}
		for _, value := range e {
			errors = append(errors, value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	fileName, errUpload := model.UploadImage(c)
	var strErrUpload string
	if errUpload != nil {
		strErrUpload = errUpload.Error()
	}

	var imageName string = ""
	if fileName != nil {
		imageName = *fileName
	}

	product := model.Product{
		ID:              input.ID,
		ProductCode:     input.ProductCode,
		ProductName:     input.ProductName,
		Image:           imageName,
		ShotDescription: input.ShotDescription,
		Description:     input.Description,
		QuantityPerUnit: input.QuantityPerUnit,
		IsFeatured:      input.IsFeatured,
		IsNew:           input.IsNew,
		CategoryID:      input.CategoryID,
		SupplierID:      input.SupplierID,
	}

	sql := fmt.Sprintf(G.UPDATEPRODUCTSQL,
		product.ProductCode,
		product.ProductName,
		product.Image,
		product.ShotDescription,
		product.Description,
		product.QuantityPerUnit,
		product.IsFeatured,
		product.IsNew,
		product.CategoryID,
		product.SupplierID,
		product.ID,
	)

	data, err := model.Model.UpdateProduct(&product, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	var response interface{}
	if len(strErrUpload) > 0 {
		err2 := []string{strErrUpload}
		response = model.CreateResponse(status, err2, data)
	} else {
		response = model.CreateResponse(status, nil, data)
	}
	c.JSON(status, response)
}

// DELETE /products/:id
// Delete a product
func DeleteProduct(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.PostForm("id"))
	if isBadRequestID(c, errID) {
		return
	}

	sql := fmt.Sprintf(G.DELETEPRODUCTSQL, i)
	success, errF := model.Model.DeleteProduct(uint(i), sql)
	if isBadRequest(c, errF) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, success)
	c.JSON(status, response)
}
