package model

import (
	"fmt"
	"time"

	G "Bidding_go/global"

	"gorm.io/gorm"
)

type Shop struct {
	ID          uint           `json:"id" form:"id" gorm:"primary_key"`
	Code        string         `json:"code" form:"code"`
	ShopName    string         `json:"shop_name" form:"shop_name"`
	Description string         `json:"description" form:"description"`
	Image       string         `json:"image" form:"image"`
	CreatedAt   time.Time      `json:"create_time" form:"create_time"`
	UpdatedAt   time.Time      `json:"update_time" form:"update_time"`
	DeletedAt   gorm.DeletedAt `json:"delete_time" form:"delete_time"`
}

func ValidateShop(object *Shop) []string {
	strErrs := []string{}

	return strErrs
}

func (s *Server) ListShop(sql string) ([]Shop, []string) {
	strErrs := []string{}
	var items []Shop
	err := s.DB.Debug().Raw(sql).Find(&items).Error

	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}

	return items, nil
}

func (s *Server) CreateShop(object *Shop, sql string) (*Shop, []string) {
	strErrs := []string{}

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Create(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("CreatedAt", time.Now())
	return object, nil
}

func (s *Server) FindShop(sql string) (*Shop, []string) {
	strErrs := []string{}
	var item *Shop
	if err := s.DB.Debug().Raw(sql).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	return item, nil
}

func (s *Server) UpdateShop(object *Shop, sql string) (*Shop, []string) {
	strErrs := []string{}
	var item Shop
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHSHOPSQL,
		object.ID,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	e := ValidateShop(object)
	strErrs = append(strErrs, e...)

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Scan(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("UpdatedAt", time.Now())
	return object, nil
}

func (s *Server) DeleteShop(id uint, sql string) (bool, []string) {
	var item *Shop
	strErrs := []string{}
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHSHOPSQL,
		id,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return false, strErrs
	}

	err := s.DB.Debug().Delete(&item).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return false, strErrs
	}

	return true, nil
}
