package controllers

import (
	G "Bidding_go/global"
	"Bidding_go/model"
	"fmt"
	"math/rand"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Home struct {
	Title       string          `json:"title" form:"title"`
	Description string          `json:"description" form:"description"`
	Index       int             `json:"index" form:"index"`
	Type        int             `json:"type" form:"type"`
	Direction   int             `json:"direction" form:"direction"`
	Items       []model.Product `json:"items" form:"items"`
}

type DisplayList struct {
	Title       string `json:"title" form:"title"`
	Description string `json:"description" form:"description"`
	Index       int    `json:"index" form:"index"`
	Type        int    `json:"type" form:"type"`
	Image       string `json:"image" form:"image"`
}

func GetHome(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	homes := []Home{}
	homeType, _ := strconv.Atoi(c.Query("type"))

	randomnum := 10
	i := 0
	for i <= 5 {
		products, eProduct := model.Model.ListProduct(fmt.Sprintf(G.RANDOMPRODUCTSQL, 0, randomnum))
		if eProduct != nil {
			break
		}

		in := []int{0, 1}
		randomIndex := rand.Intn(len(in))
		pickDirection := in[randomIndex]

		homes = append(homes, Home{
			Title:       "Sản phẩm giới thiệu",
			Description: "Chi tiết sản phẩm giới thiệu",
			Index:       i,
			Type:        homeType,
			Direction:   pickDirection,
			Items:       products,
		})

		i++
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, homes)
	c.JSON(status, response)
}

func GetDisplayList(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	displayList := []DisplayList{}
	displayList = append(displayList, DisplayList{
		Title:       "Home",
		Description: "Màn hình Home",
		Index:       0,
		Type:        0,
		Image:       "https://icons8.com/icon/xQqG6IgQ28re/home-screen",
	})

	displayList = append(displayList, DisplayList{
		Title:       "1 Yên",
		Description: "Màn hình 1 Yên",
		Index:       1,
		Type:        1,
		Image:       "https://icons8.com/icon/HmvCK0OC1HAQ/1",
	})

	displayList = append(displayList, DisplayList{
		Title:       "Giảm giá",
		Description: "Màn hình Giảm giá",
		Index:       2,
		Type:        2,
		Image:       "https://icons8.com/icon/119107/sale",
	})

	displayList = append(displayList, DisplayList{
		Title:       "Sản phẩm mới",
		Description: "Màn hình Sản phẩm mới",
		Index:       3,
		Type:        3,
		Image:       "https://icons8.com/icon/80765/new",
	})

	status := http.StatusOK
	response := model.CreateResponse(status, nil, displayList)
	c.JSON(status, response)
}
