package model

import (
	"Bidding_go/auth"
	"fmt"
	"time"

	"github.com/twinj/uuid"
	"gorm.io/gorm"
)

type Auth struct {
	ID          uint           `gorm:"primary_key;auto_increment" json:"id"`
	UserID      uint           `gorm:";not null;" json:"user_id"`
	AuthUUID    string         `gorm:"size:255;not null;" json:"auth_uuid"`
	DeviceToken string         `json:"device_token"`
	CreatedAt   time.Time      `json:"create_time"`
	UpdatedAt   time.Time      `json:"update_time"`
	DeletedAt   gorm.DeletedAt `json:"delete_time"`
}

func (s *Server) FetchAuth(authD *auth.AuthDetails) (*Auth, error) {
	au := &Auth{}
	err := s.DB.Debug().Where("user_id = ? AND auth_uuid = ?", authD.UserId, authD.AuthUuid).Take(&au).Error
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	return au, nil
}

func (s *Server) UpdateAuth(authD *auth.AuthDetails, deviceToken string) (*Auth, error) {
	// Get model if exist
	au := &Auth{}
	err := s.DB.Debug().Where("user_id = ? AND auth_uuid = ?", authD.UserId, authD.AuthUuid).Take(&au).Error
	if err != nil {
		return nil, err
	}
	au.DeviceToken = deviceToken

	er := s.DB.Debug().Updates(&au).Error
	if er != nil {
		return nil, er
	}

	return au, nil
}

// Once a user row in the auth table
func (s *Server) DeleteAuth(authD *auth.AuthDetails) error {
	au := &Auth{}
	db := s.DB.Debug().Where("user_id = ? AND auth_uuid = ?", authD.UserId, authD.AuthUuid).Take(&au).Delete(&au)
	if db.Error != nil {
		return db.Error
	}
	return nil
}

// Once the user signup/login, create a row in the auth table, with a new uuid
func (s *Server) CreateAuth(userId uint, deviceToken string) (*Auth, error) {
	// var auths []Auth
	// if s.DB.Debug().Where("device_token = ?", deviceToken).Find(&auths).Error == nil {
	// 	for _, auth := range auths {
	// 		if err := s.DB.Debug().Delete(&auth).Error; err != nil {
	// 			return nil, err
	// 		}
	// 	}
	// }
	au := &Auth{}
	au.AuthUUID = uuid.NewV4().String() //generate a new UUID each time
	au.UserID = userId
	au.DeviceToken = deviceToken
	err := s.DB.Debug().Create(&au).Error
	if err != nil {
		return nil, err
	}
	return au, nil
}

func (s *Server) GetUserWith(userToken string) (*User, error) {
	au := &Auth{}
	if err := s.DB.Debug().Where("auth_uuid = ?", userToken).First(&au).Error; err != nil {
		return nil, err
	}

	var u *User
	if err := s.DB.Debug().Where("id = ?", au.UserID).First(&u).Error; err != nil {
		return nil, err
	}

	return u, nil
}

func (s *Server) GetAllDeviceToken() (*[]Auth, error) {
	var auths []Auth
	if err := s.DB.Debug().Find(&auths).Error; err != nil {
		return nil, err
	}

	return &auths, nil
}
