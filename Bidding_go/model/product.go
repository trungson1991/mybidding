package model

import (
	"fmt"
	"time"

	G "Bidding_go/global"

	"gorm.io/gorm"
)

type Product struct {
	ID              uint           `json:"id" form:"id" gorm:"primary_key"`
	ProductCode     string         `json:"product_code" form:"product_code"`
	ProductName     string         `json:"product_name" form:"product_name"`
	Image           string         `json:"image" form:"image"`
	ShotDescription string         `json:"short_description" form:"short_description"`
	Description     string         `json:"description" form:"description"`
	QuantityPerUnit string         `json:"quantity_per_unit" form:"quantity_per_unit"`
	IsFeatured      int            `json:"is_featured" form:"is_featured"`
	IsNew           int            `json:"is_new" form:"is_new"`
	CategoryID      uint           `json:"category_id" form:"category_id"`
	SupplierID      uint           `json:"supplier_id" form:"supplier_id"`
	CreatedAt       time.Time      `json:"create_time" form:"create_time"`
	UpdatedAt       time.Time      `json:"update_time" form:"update_time"`
	DeletedAt       gorm.DeletedAt `json:"delete_time" form:"delete_time"`
}

func ValidateProduct(object *Product) []string {
	strErrs := []string{}

	return strErrs
}

func (s *Server) ListProduct(sql string) ([]Product, []string) {
	strErrs := []string{}
	var items []Product
	err := s.DB.Debug().Raw(sql).Find(&items).Error

	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}

	return items, nil
}

func (s *Server) CreateProduct(object *Product, sql string) (*Product, []string) {
	strErrs := []string{}

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Create(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("CreatedAt", time.Now())
	return object, nil
}

func (s *Server) FindProduct(sql string) (*Product, []string) {
	strErrs := []string{}
	var item *Product
	if err := s.DB.Debug().Raw(sql).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	return item, nil
}

func (s *Server) UpdateProduct(object *Product, sql string) (*Product, []string) {
	strErrs := []string{}
	var item Product
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHPRODUCTSQL,
		object.ID,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	e := ValidateProduct(object)
	strErrs = append(strErrs, e...)

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Scan(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("UpdatedAt", time.Now())
	return object, nil
}

func (s *Server) DeleteProduct(id uint, sql string) (bool, []string) {
	var item *Product
	strErrs := []string{}
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHPRODUCTSQL,
		id,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return false, strErrs
	}

	err := s.DB.Debug().Delete(&item).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return false, strErrs
	}

	return true, nil
}
