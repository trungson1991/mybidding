package auth

import (
	"net/http"

	G "Bidding_go/global"

	"github.com/gin-gonic/gin"
)

func TokenAuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		err := TokenValid(c.Request)
		if err != nil {
			c.JSON(http.StatusUnauthorized, G.YouNeedToBeAuthorized)
			c.Abort()
			return
		}
		c.Next()
	}
}

type sigInInterface interface {
	SignIn(AuthDetails) (string, error)
}

type signInStruct struct{}

var Authorize sigInInterface = &signInStruct{}

func (si *signInStruct) SignIn(authD AuthDetails) (string, error) {
	token, err := CreateToken(authD)
	if err != nil {
		return "", err
	}
	return token, nil
}
