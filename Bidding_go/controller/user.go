package controllers

import (
	"Bidding_go/auth"
	G "Bidding_go/global"
	"Bidding_go/model"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/badoux/checkmail"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type LoginInput struct {
	UserName string `json:"user_name" form:"user_name" binding:"required"`
	Password string `json:"password" form:"password" binding:"required"`
}

type UserCreateInput struct {
	UserName   string    `json:"user_name" form:"user_name"`
	Password   string    `json:"password" form:"password"`
	FirstName  string    `json:"first_name" form:"first_name"`
	LastName   string    `json:"last_name" form:"last_name"`
	Gender     int       `json:"gender" form:"gender"`
	Email      string    `json:"email" form:"email"`
	Birthday   time.Time `json:"birthday" form:"birthday"`
	Address1   string    `json:"address1" form:"address1"`
	Address2   string    `json:"address2" form:"address2"`
	Phone      string    `json:"phone" form:"phone"`
	State      string    `json:"state" form:"state"`
	City       string    `json:"city" form:"city"`
	PortalCode string    `json:"portal_code" form:"portal_code"`
	Country    string    `json:"country" form:"country"`
	ActiveCode string    `json:"active_code" form:"active_code"`
	Status     int       `json:"status" form:"status"`
	Role       int8      `json:"role" form:"role"`
}

type UserUpdateInput struct {
	UserName   string    `json:"user_name" form:"user_name"`
	Password   string    `json:"password" form:"password"`
	FirstName  string    `json:"first_name" form:"first_name"`
	LastName   string    `json:"last_name" form:"last_name"`
	Gender     int       `json:"gender" form:"gender"`
	Email      string    `json:"email" form:"email"`
	Birthday   time.Time `json:"birthday" form:"birthday"`
	Address1   string    `json:"address1" form:"address1"`
	Address2   string    `json:"address2" form:"address2"`
	Phone      string    `json:"phone" form:"phone"`
	State      string    `json:"state" form:"state"`
	City       string    `json:"city" form:"city"`
	PortalCode string    `json:"portal_code" form:"portal_code"`
	Country    string    `json:"country" form:"country"`
	ActiveCode string    `json:"active_code" form:"active_code"`
	Status     int       `json:"status" form:"status"`
	Role       int8      `json:"role" form:"role"`
}

type GETUser struct {
	ID         uint           `json:"id" form:"id" gorm:"primary_key"`
	UserName   string         `json:"user_name" form:"user_name"`
	FirstName  string         `json:"first_name" form:"first_name"`
	LastName   string         `json:"last_name" form:"last_name"`
	Gender     int            `json:"gender" form:"gender"`
	Email      string         `json:"email" form:"email"`
	Birthday   time.Time      `json:"birthday" form:"birthday"`
	Address1   string         `json:"address1" form:"address1"`
	Address2   string         `json:"address2" form:"address2"`
	Phone      string         `json:"phone" form:"phone"`
	State      string         `json:"state" form:"state"`
	City       string         `json:"city" form:"city"`
	PortalCode string         `json:"portal_code" form:"portal_code"`
	Country    string         `json:"country" form:"country"`
	ActiveCode string         `json:"active_code" form:"active_code"`
	Status     int            `json:"status" form:"status"`
	Image      string         `json:"image" form:"image"`
	Role       int8           `json:"role" form:"role"`
	CreatedAt  time.Time      `json:"create_time" form:"create_time"`
	UpdatedAt  time.Time      `json:"update_time" form:"update_time"`
	DeletedAt  gorm.DeletedAt `json:"delete_time" form:"delete_time"`
}

// GET /Users
// Get all Users
func ListUsers(c *gin.Context) {
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	if !isPermission(c, auth) {
		return
	}

	page, _ := strconv.Atoi(c.Query("page"))
	num, _ := strconv.Atoi(c.Query("page_num"))

	sql := fmt.Sprintf(G.LISTUSERSQL, (page-1)*num, num)
	users, err := model.Model.ListUsers(sql)
	if err != nil {
		status := http.StatusInternalServerError
		response := model.CreateResponse(status, err, nil)
		c.JSON(status, response)
		return
	}

	var getusers []GETUser
	for _, item := range users {
		// Create GETUser
		getuser := GETUser{
			ID:         item.ID,
			UserName:   item.UserName,
			FirstName:  item.FirstName,
			LastName:   item.LastName,
			Gender:     item.Gender,
			Email:      item.Email,
			Birthday:   item.Birthday,
			Address1:   item.Address1,
			Address2:   item.Address2,
			Phone:      item.Phone,
			State:      item.State,
			City:       item.City,
			PortalCode: item.PortalCode,
			Country:    item.Country,
			ActiveCode: item.ActiveCode,
			Status:     item.Status,
			Image:      item.Image,
			Role:       item.Role,
			CreatedAt:  item.CreatedAt,
			UpdatedAt:  item.UpdatedAt,
			DeletedAt:  item.DeletedAt,
		}
		getusers = append(getusers, getuser)
	}

	res := model.ListResponse{
		Data:    getusers,
		Page:    page,
		PageNum: num,
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, res)
	c.JSON(status, response)
}

func CreateUser(c *gin.Context) {
	var input UserCreateInput
	if err := c.ShouldBind(&input); err != nil {
		status := http.StatusUnprocessableEntity
		response := model.CreateResponse(status, []string{G.ConnectDatabaseError}, nil)
		c.JSON(status, response)
		return
	}

	fileName, errUpload := model.UploadImage(c)
	var strErrUpload string
	if errUpload != nil {
		strErrUpload = errUpload.Error()
	}

	var imageName string = ""
	if fileName != nil {
		imageName = *fileName
	}

	u := model.User{
		UserName:   input.UserName,
		Password:   input.Password,
		FirstName:  input.FirstName,
		LastName:   input.LastName,
		Gender:     input.Gender,
		Email:      input.Email,
		Birthday:   input.Birthday,
		Address1:   input.Address1,
		Address2:   input.Address2,
		Phone:      input.Phone,
		State:      input.State,
		City:       input.City,
		PortalCode: input.PortalCode,
		Country:    input.Country,
		ActiveCode: input.ActiveCode,
		Status:     input.Status,
		Image:      imageName,
		Role:       input.Role,
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if err != nil {
		panic(err)
	}

	sql := fmt.Sprintf(G.CREATEUSERSQL,
		u.UserName,
		hashedPassword,
		u.FirstName,
		u.LastName,
		u.Gender,
		u.Email,
		u.Birthday.Format(G.DateFormatSQL),
		u.Address1,
		u.Address2,
		u.Phone,
		u.State,
		u.City,
		u.PortalCode,
		u.Country,
		u.ActiveCode,
		u.Status,
		u.Image,
		u.Role)
	user, strErrs := model.Model.CreateUser(&u, sql)
	if strErrs != nil {
		num := len(strErrs)
		if num > 0 {
			status := http.StatusInternalServerError
			response := model.CreateResponse(status, strErrs, nil)
			c.JSON(status, response)
			return
		}
	}

	res := GETUser{
		UserName:   user.UserName,
		FirstName:  user.FirstName,
		LastName:   user.LastName,
		Gender:     user.Gender,
		Email:      user.Email,
		Birthday:   user.Birthday,
		Address1:   user.Address1,
		Address2:   user.Address2,
		Phone:      user.Phone,
		State:      user.State,
		City:       user.City,
		PortalCode: user.PortalCode,
		Country:    user.Country,
		ActiveCode: user.ActiveCode,
		Status:     user.Status,
		Image:      user.Image,
		Role:       user.Role,
		CreatedAt:  user.CreatedAt,
		UpdatedAt:  user.UpdatedAt,
		DeletedAt:  user.DeletedAt,
	}

	status := http.StatusCreated
	var response interface{}
	if len(strErrUpload) > 0 {
		err2 := []string{strErrUpload}
		response = model.CreateResponse(status, err2, nil)
	} else {
		response = model.CreateResponse(status, nil, res)
	}

	c.JSON(status, response)
}

func UpdateUser(c *gin.Context) {
	var input UserUpdateInput
	if err := c.ShouldBind(&input); err != nil {
		status := http.StatusUnprocessableEntity
		response := model.CreateResponse(status, []string{G.ConnectDatabaseError}, nil)
		c.JSON(status, response)
		return
	}

	auth, err := GetAuthWith(c.Request)
	if err != nil {
		status := http.StatusUnauthorized
		response := model.CreateResponse(status, []string{G.Unauthorized}, nil)
		c.JSON(status, response)
		return
	}

	fileName, errUpload := model.UploadImage(c)
	var strErrUpload string
	if errUpload != nil {
		strErrUpload = errUpload.Error()
	}

	var imageName string = ""
	if fileName != nil {
		imageName = *fileName
	}

	u := model.User{
		UserName:   input.UserName,
		Password:   input.Password,
		FirstName:  input.FirstName,
		LastName:   input.LastName,
		Gender:     input.Gender,
		Email:      input.Email,
		Birthday:   input.Birthday,
		Address1:   input.Address1,
		Address2:   input.Address2,
		Phone:      input.Phone,
		State:      input.State,
		City:       input.City,
		PortalCode: input.PortalCode,
		Country:    input.Country,
		ActiveCode: input.ActiveCode,
		Status:     input.Status,
		Image:      imageName,
		Role:       input.Role,
	}

	sql := fmt.Sprintf(G.UPDATEUSERSQL,
		u.UserName,
		u.Password,
		u.FirstName,
		u.LastName,
		u.Gender,
		u.Email,
		u.Birthday.Format(G.DateFormatSQL),
		u.Address1,
		u.Address2,
		u.Phone,
		u.State,
		u.City,
		u.PortalCode,
		u.Country,
		u.ActiveCode,
		u.Status,
		u.Image,
		u.Role,
		auth.UserID)

	user, strErrs := model.Model.UpdateUser(&u, auth.AuthUUID, sql)
	if strErrs != nil {
		num := len(strErrs)
		if num > 0 {
			status := http.StatusInternalServerError
			response := model.CreateResponse(status, strErrs, nil)
			c.JSON(status, response)
			return
		}
	}

	res := GETUser{
		UserName:   user.UserName,
		FirstName:  user.FirstName,
		LastName:   user.LastName,
		Gender:     user.Gender,
		Email:      user.Email,
		Birthday:   user.Birthday,
		Address1:   user.Address1,
		Address2:   user.Address2,
		Phone:      user.Phone,
		State:      user.State,
		City:       user.City,
		PortalCode: user.PortalCode,
		Country:    user.Country,
		ActiveCode: user.ActiveCode,
		Status:     user.Status,
		Image:      user.Image,
		Role:       user.Role,
		CreatedAt:  user.CreatedAt,
		UpdatedAt:  user.UpdatedAt,
		DeletedAt:  user.DeletedAt,
	}

	status := http.StatusOK
	var response interface{}
	if len(strErrUpload) > 0 {
		err2 := []string{strErrUpload}
		response = model.CreateResponse(status, err2, res)
	} else {
		response = model.CreateResponse(status, nil, res)
	}
	c.JSON(status, response)
}

func Login(c *gin.Context) {
	var input LoginInput
	if err := c.ShouldBind(&input); err != nil {
		status := http.StatusUnprocessableEntity
		response := model.CreateResponse(status, []string{err.Error()}, nil)
		c.JSON(status, response)
		return
	}

	var user *model.User
	var strErrs []string
	var sql string
	if err := checkmail.ValidateFormat(input.UserName); err != nil {
		sql = fmt.Sprintf(G.GETUSERBYUSERNAMESQL, input.UserName)
	} else {
		sql = fmt.Sprintf(G.GETUSERBYEMAILSQL, input.UserName)
	}

	user, strErrs = model.Model.GetUser(sql)
	errPassword := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(input.Password))
	if strErrs != nil || errPassword != nil {
		status := http.StatusNotFound
		response := model.CreateResponse(status, strErrs, nil)
		c.JSON(status, response)
		return
	}
	//since after the user logged out, we destroyed that record in the database so that same jwt token can't be used twice. We need to create the token again
	deviceToken := c.PostForm(G.DeviceToken)
	authData, err := model.Model.CreateAuth(user.ID, deviceToken)

	if err != nil {
		status := http.StatusInternalServerError
		response := model.CreateResponse(status, []string{err.Error()}, nil)
		c.JSON(status, response)
		return
	}
	var authD auth.AuthDetails
	authD.UserId = authData.UserID
	authD.AuthUuid = authData.AuthUUID

	user_token, loginErr := auth.Authorize.SignIn(authD)
	if loginErr != nil {
		status := http.StatusForbidden
		response := model.CreateResponse(status, []string{G.PleaseTryToLoginLater}, nil)
		c.JSON(status, response)
		return
	}

	data := map[string]interface{}{
		G.UserToken: user_token,
	}
	status := http.StatusOK
	response := model.CreateResponse(status, nil, data)
	c.JSON(status, response)
}

func GetUserInfo(c *gin.Context) {
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	u, _ := strconv.Atoi(c.Query("user_id"))
	userIDParams := uint(u)
	var user *model.User
	if userIDParams > 0 && userIDParams != auth.UserID {
		if !isPermission(c, auth) {
			return
		}
		sql := fmt.Sprintf(G.GETUSERBYIDSQL, userIDParams)
		u, errF := model.Model.GetUser(sql)
		if isBadRequest(c, errF) {
			return
		}
		user = u
	} else {
		u, err := model.Model.GetUserInfo(auth.AuthUUID)
		if err != nil {
			status := http.StatusForbidden
			response := model.CreateResponse(status, err, nil)
			c.JSON(status, response)
			return
		}
		user = u
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, user)
	c.JSON(status, response)
}

func LogOut(c *gin.Context) {
	au, err := auth.ExtractTokenAuth(c.Request)
	if err != nil {
		status := http.StatusUnauthorized
		response := model.CreateResponse(status, []string{G.Unauthorized}, nil)
		c.JSON(status, response)
		return
	}
	delErr := model.Model.DeleteAuth(au)
	if delErr != nil {
		status := http.StatusUnauthorized
		response := model.CreateResponse(status, []string{G.Unauthorized}, nil)
		c.JSON(status, response)
		return
	}
	status := http.StatusOK
	response := model.CreateResponse(status, []string{G.SuccessfullyLoggedOut}, nil)
	c.JSON(status, response)
}
