package model

import (
	"fmt"
	"time"

	G "Bidding_go/global"

	"gorm.io/gorm"
)

type Order struct {
	ID             uint           `json:"id" form:"id" gorm:"primary_key"`
	EmployeeID     uint           `json:"employee_id" form:"employee_id"`
	CustomerID     uint           `json:"customer_id" form:"customer_id"`
	BiddingID      uint           `json:"bidding_id" form:"bidding_id"`
	OrderDate      string         `json:"order_date" form:"order_date"`
	ShippedDate    string         `json:"shipped_date" form:"shipped_date"`
	ShipName       string         `json:"ship_name" form:"ship_name"`
	ShipAddress1   string         `json:"ship_address1" form:"ship_address1"`
	ShipAddress2   string         `json:"ship_address2" form:"ship_address2"`
	ShipCity       string         `json:"ship_city" form:"ship_city"`
	ShipState      string         `json:"ship_state" form:"ship_state"`
	ShipPostalCode string         `json:"ship_postal_code" form:"ship_postal_code"`
	ShipCountry    string         `json:"ship_country" form:"ship_country"`
	ShippingFee    string         `json:"shipping_fee" form:"shipping_fee"`
	PaidDate       string         `json:"paid_date" form:"paid_date"`
	PaymentTypeID  string         `json:"payment_type_id" form:"payment_type_id"`
	OrderStatus    string         `json:"order_status" form:"order_status"`
	TypeOrder      string         `json:"type_order" form:"type_order"`
	CreatedAt      time.Time      `json:"create_time" form:"create_time"`
	UpdatedAt      time.Time      `json:"update_time" form:"update_time"`
	DeletedAt      gorm.DeletedAt `json:"delete_time" form:"delete_time"`
}

func ValidateOrder(object *Order) []string {
	strErrs := []string{}

	return strErrs
}

func (s *Server) ListOrder(sql string) ([]Order, []string) {
	strErrs := []string{}
	var items []Order
	err := s.DB.Debug().Raw(sql).Find(&items).Error

	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}

	return items, nil
}

func (s *Server) CreateOrder(object *Order, sql string) (*Order, []string) {
	strErrs := []string{}

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Create(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("CreatedAt", time.Now())
	return object, nil
}

func (s *Server) FindOrder(sql string) (*Order, []string) {
	strErrs := []string{}
	var item *Order
	if err := s.DB.Debug().Raw(sql).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	return item, nil
}

func (s *Server) UpdateOrder(userID uint, object *Order, sql string) (*Order, []string) {
	strErrs := []string{}
	var item Order
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHORDERSQL,
		object.ID,
		userID,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	e := ValidateOrder(object)
	strErrs = append(strErrs, e...)

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Scan(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("UpdatedAt", time.Now())
	return object, nil
}

func (s *Server) DeleteOrder(userID uint, id uint, sql string) (bool, []string) {
	var item *Order
	strErrs := []string{}
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHORDERSQL,
		id,
		userID,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return false, strErrs
	}

	err := s.DB.Debug().Delete(&item).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return false, strErrs
	}

	return true, nil
}
