package model

import (
	"fmt"
	"time"

	G "Bidding_go/global"

	"gorm.io/gorm"
)

type OrderDetail struct {
	ID            uint           `json:"id" form:"id" gorm:"primary_key"`
	OrderID       uint           `json:"order_id" form:"order_id"`
	ProductID     uint           `json:"product_id" form:"product_id"`
	Quantity      int            `json:"quantity" form:"quantity"`
	UnitPrice     float64        `json:"unit_price" form:"unit_price"`
	Discount      float32        `json:"discount" form:"discount"`
	Status        int            `json:"status" form:"status"`
	DateAllocated time.Time      `json:"date_allocated" form:"date_allocated"`
	CreatedAt     time.Time      `json:"create_time" form:"create_time"`
	UpdatedAt     time.Time      `json:"update_time" form:"update_time"`
	DeletedAt     gorm.DeletedAt `json:"delete_time" form:"delete_time"`
}

func ValidateOrderDetail(object *OrderDetail) []string {
	strErrs := []string{}

	return strErrs
}

func (s *Server) ListOrderDetail(sql string) ([]OrderDetail, []string) {
	strErrs := []string{}
	var items []OrderDetail
	err := s.DB.Debug().Raw(sql).Find(&items).Error

	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}

	return items, nil
}

func (s *Server) CreateOrderDetail(object *OrderDetail, sql string) (*OrderDetail, []string) {
	strErrs := []string{}

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Create(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("CreatedAt", time.Now())
	return object, nil
}

func (s *Server) FindOrderDetail(sql string) (*OrderDetail, []string) {
	strErrs := []string{}
	var item *OrderDetail
	if err := s.DB.Debug().Raw(sql).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	return item, nil
}

func (s *Server) UpdateOrderDetail(object *OrderDetail, sql string) (*OrderDetail, []string) {
	strErrs := []string{}
	var item OrderDetail
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHORDERDETAILSQL,
		object.ID,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	e := ValidateOrderDetail(object)
	strErrs = append(strErrs, e...)

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Scan(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("UpdatedAt", time.Now())
	return object, nil
}

func (s *Server) DeleteOrderDetail(id uint, sql string) (bool, []string) {
	var item *OrderDetail
	strErrs := []string{}
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHORDERDETAILSQL,
		id,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return false, strErrs
	}

	err := s.DB.Debug().Delete(&item).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return false, strErrs
	}

	return true, nil
}
