package controllers

import (
	G "Bidding_go/global"
	"Bidding_go/model"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type CreateCategoryInput struct {
	CategoryCode string `json:"category_code" form:"category_code"`
	CategoryName string `json:"category_name" form:"category_name"`
	Description  string `json:"description" form:"description"`
}

type UpdateCategoryInput struct {
	ID           uint   `json:"id" form:"id" gorm:"primary_key"`
	CategoryCode string `json:"category_code" form:"category_code"`
	CategoryName string `json:"category_name" form:"category_name"`
	Description  string `json:"description" form:"description"`
}

// GET /categorys
// Get all categorys
func ListCategorys(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	page, _ := strconv.Atoi(c.Query("page"))
	num, _ := strconv.Atoi(c.Query("page_num"))

	sql := fmt.Sprintf(G.LISTCATEGORYSQL, (page-1)*num, num)
	categorys, err := model.Model.ListCategory(sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, categorys)
	c.JSON(status, response)
}

// POST /categorys
// Create new category
func CreateCategory(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	var input CreateCategoryInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.Category
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		errors := []string{}
		for key, value := range e {
			errors = append(errors, key+" : "+value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	fileName, errUpload := model.UploadImage(c)
	var strErrUpload string
	if errUpload != nil {
		strErrUpload = errUpload.Error()
	}

	var imageName string = ""
	if fileName != nil {
		imageName = *fileName
	}

	// Create category
	category := model.Category{
		CategoryCode: input.CategoryCode,
		CategoryName: input.CategoryName,
		Description:  input.Description,
		Image:        imageName,
	}

	sql := fmt.Sprintf(G.CREATECATEGORYSQL,
		category.CategoryCode,
		category.CategoryName,
		category.Description,
		category.Image,
	)
	data, err := model.Model.CreateCategory(&category, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	var response interface{}
	if len(strErrUpload) > 0 {
		err2 := []string{strErrUpload}
		response = model.CreateResponse(status, err2, data)
	} else {
		response = model.CreateResponse(status, nil, data)
	}
	c.JSON(status, response)
}

// GET /categorys/:id
// Find a category
func FindCategory(c *gin.Context) { // Get model if exist
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.Query("id"))
	if isBadRequestID(c, errID) {
		return
	}

	sql := fmt.Sprintf(G.FINDCATEGORYSQL, i)
	category, errF := model.Model.FindCategory(sql)
	if isBadRequest(c, errF) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, category)
	c.JSON(status, response)
}

// PATCH /categorys/:id
// Update a category
func UpdateCategory(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	// Get model if exist
	var input UpdateCategoryInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.Category
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		errors := []string{}
		for _, value := range e {
			errors = append(errors, value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	fileName, errUpload := model.UploadImage(c)
	var strErrUpload string
	if errUpload != nil {
		strErrUpload = errUpload.Error()
	}

	var imageName string = ""
	if fileName != nil {
		imageName = *fileName
	}

	category := model.Category{
		ID:           input.ID,
		CategoryCode: input.CategoryCode,
		CategoryName: input.CategoryName,
		Description:  input.Description,
		Image:        imageName,
	}

	sql := fmt.Sprintf(G.UPDATECATEGORYSQL,
		category.CategoryCode,
		category.CategoryName,
		category.Description,
		category.Image,
		category.ID,
	)

	data, err := model.Model.UpdateCategory(&category, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	var response interface{}
	if len(strErrUpload) > 0 {
		err2 := []string{strErrUpload}
		response = model.CreateResponse(status, err2, data)
	} else {
		response = model.CreateResponse(status, nil, data)
	}
	c.JSON(status, response)
}

// DELETE /categorys/:id
// Delete a category
func DeleteCategory(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.PostForm("id"))
	if isBadRequestID(c, errID) {
		return
	}

	sql := fmt.Sprintf(G.DELETECATEGORYSQL, i)
	success, errF := model.Model.DeleteCategory(uint(i), sql)
	if isBadRequest(c, errF) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, success)
	c.JSON(status, response)
}
