package controllers

import (
	G "Bidding_go/global"
	"Bidding_go/model"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type CreateOrderInput struct {
	EmployeeID     uint   `json:"employee_id" form:"employee_id"`
	CustomerID     uint   `json:"customer_id" form:"customer_id"`
	BiddingID      uint   `json:"bidding_id" form:"bidding_id"`
	OrderDate      string `json:"order_date" form:"order_date"`
	ShippedDate    string `json:"shipped_date" form:"shipped_date"`
	ShipName       string `json:"ship_name" form:"ship_name"`
	ShipAddress1   string `json:"ship_address1" form:"ship_address1"`
	ShipAddress2   string `json:"ship_address2" form:"ship_address2"`
	ShipCity       string `json:"ship_city" form:"ship_city"`
	ShipState      string `json:"ship_state" form:"ship_state"`
	ShipPostalCode string `json:"ship_postal_code" form:"ship_postal_code"`
	ShipCountry    string `json:"ship_country" form:"ship_country"`
	ShippingFee    string `json:"shipping_fee" form:"shipping_fee"`
	PaidDate       string `json:"paid_date" form:"paid_date"`
	PaymentTypeID  string `json:"payment_type_id" form:"payment_type_id"`
	OrderStatus    string `json:"order_status" form:"order_status"`
	TypeOrder      string `json:"type_order" form:"type_order"`
}

type UpdateOrderInput struct {
	ID             uint   `json:"id" form:"id" gorm:"primary_key"`
	EmployeeID     uint   `json:"employee_id" form:"employee_id"`
	CustomerID     uint   `json:"customer_id" form:"customer_id"`
	BiddingID      uint   `json:"bidding_id" form:"bidding_id"`
	OrderDate      string `json:"order_date" form:"order_date"`
	ShippedDate    string `json:"shipped_date" form:"shipped_date"`
	ShipName       string `json:"ship_name" form:"ship_name"`
	ShipAddress1   string `json:"ship_address1" form:"ship_address1"`
	ShipAddress2   string `json:"ship_address2" form:"ship_address2"`
	ShipCity       string `json:"ship_city" form:"ship_city"`
	ShipState      string `json:"ship_state" form:"ship_state"`
	ShipPostalCode string `json:"ship_postal_code" form:"ship_postal_code"`
	ShipCountry    string `json:"ship_country" form:"ship_country"`
	ShippingFee    string `json:"shipping_fee" form:"shipping_fee"`
	PaidDate       string `json:"paid_date" form:"paid_date"`
	PaymentTypeID  string `json:"payment_type_id" form:"payment_type_id"`
	OrderStatus    string `json:"order_status" form:"order_status"`
	TypeOrder      string `json:"type_order" form:"type_order"`
}

type GETOrder struct {
	ID             uint          `json:"id" form:"id" gorm:"primary_key"`
	Employee       model.User    `json:"employee" form:"employee"`
	Customer       model.User    `json:"customer" form:"customer"`
	Bidding        model.Bidding `json:"bidding" form:"bidding"`
	OrderDate      string        `json:"order_date" form:"order_date"`
	ShippedDate    string        `json:"shipped_date" form:"shipped_date"`
	ShipName       string        `json:"ship_name" form:"ship_name"`
	ShipAddress1   string        `json:"ship_address1" form:"ship_address1"`
	ShipAddress2   string        `json:"ship_address2" form:"ship_address2"`
	ShipCity       string        `json:"ship_city" form:"ship_city"`
	ShipState      string        `json:"ship_state" form:"ship_state"`
	ShipPostalCode string        `json:"ship_postal_code" form:"ship_postal_code"`
	ShipCountry    string        `json:"ship_country" form:"ship_country"`
	ShippingFee    string        `json:"shipping_fee" form:"shipping_fee"`
	PaidDate       string        `json:"paid_date" form:"paid_date"`
	PaymentTypeID  string        `json:"payment_type_id" form:"payment_type_id"`
	OrderStatus    string        `json:"order_status" form:"order_status"`
	TypeOrder      string        `json:"type_order" form:"type_order"`
}

// GET /orders
// Get all orders
func ListOrders(c *gin.Context) {
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	u, _ := strconv.Atoi(c.Query("user_id"))
	userIDParams := uint(u)
	userID := auth.UserID
	if userIDParams > 0 && userIDParams != auth.UserID {
		if !isPermission(c, auth) {
			return
		}
		userID = userIDParams
	}

	page, _ := strconv.Atoi(c.Query("page"))
	num, _ := strconv.Atoi(c.Query("page_num"))

	sql := fmt.Sprintf(G.LISTORDERSQL, userID, (page-1)*num, num)
	orders, err := model.Model.ListOrder(sql)
	if isServerError(c, err) {
		return
	}

	var getOrders []GETOrder
	for _, item := range orders {
		sql := fmt.Sprintf(G.GETUSERBYIDSQL, item.EmployeeID)
		employee, err := model.Model.GetUser(sql)
		if isServerError(c, err) {
			return
		}

		sql1 := fmt.Sprintf(G.GETUSERBYIDSQL, item.CustomerID)
		customer, err := model.Model.GetUser(sql1)
		if isServerError(c, err) {
			return
		}

		sql2 := fmt.Sprintf(G.FINDBIDDINGSQL, item.BiddingID)
		bidding, err := model.Model.FindBidding(sql2)
		if isServerError(c, err) {
			return
		}

		i := GETOrder{
			ID:             item.ID,
			Employee:       *employee,
			Customer:       *customer,
			Bidding:        *bidding,
			OrderDate:      item.OrderDate,
			ShippedDate:    item.ShippedDate,
			ShipName:       item.ShipName,
			ShipAddress1:   item.ShipAddress1,
			ShipAddress2:   item.ShipAddress2,
			ShipCity:       item.ShipCity,
			ShipState:      item.ShipState,
			ShipPostalCode: item.ShipPostalCode,
			ShipCountry:    item.ShipCountry,
			ShippingFee:    item.ShippingFee,
			PaidDate:       item.PaidDate,
			PaymentTypeID:  item.PaymentTypeID,
			OrderStatus:    item.OrderStatus,
			TypeOrder:      item.TypeOrder,
		}

		getOrders = append(getOrders, i)
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, getOrders)
	c.JSON(status, response)
}

// POST /orders
// Create new order
func CreateOrder(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	var input CreateOrderInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.Order
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		fmt.Println(err)
		errors := []string{}
		for key, value := range e {
			errors = append(errors, key+" : "+value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	// Create order
	order := model.Order{
		EmployeeID:     input.EmployeeID,
		CustomerID:     input.CustomerID,
		BiddingID:      input.BiddingID,
		OrderDate:      input.OrderDate,
		ShippedDate:    input.ShippedDate,
		ShipName:       input.ShipName,
		ShipAddress1:   input.ShipAddress1,
		ShipAddress2:   input.ShipAddress2,
		ShipCity:       input.ShipCity,
		ShipState:      input.ShipState,
		ShipPostalCode: input.ShipPostalCode,
		ShipCountry:    input.ShipCountry,
		ShippingFee:    input.ShippingFee,
		PaidDate:       input.PaidDate,
		PaymentTypeID:  input.PaymentTypeID,
		OrderStatus:    input.OrderStatus,
		TypeOrder:      input.TypeOrder,
	}

	sql := fmt.Sprintf(G.CREATEORDERSQL,
		order.EmployeeID,
		order.CustomerID,
		order.BiddingID,
		order.OrderDate,
		order.ShippedDate,
		order.ShipName,
		order.ShipAddress1,
		order.ShipAddress2,
		order.ShipCity,
		order.ShipState,
		order.ShipPostalCode,
		order.ShipCountry,
		order.ShippingFee,
		order.PaidDate,
		order.PaymentTypeID,
		order.OrderStatus,
		order.TypeOrder,
	)
	data, err := model.Model.CreateOrder(&order, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, data)
	c.JSON(status, response)
}

// GET /orders/:id
// Find a order
func FindOrder(c *gin.Context) { // Get model if exist
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.Query("id"))
	if isBadRequestID(c, errID) {
		return
	}

	u, _ := strconv.Atoi(c.Query("user_id"))
	userIDParams := uint(u)
	userID := auth.UserID
	if userIDParams > 0 && userIDParams != auth.UserID {
		if !isPermission(c, auth) {
			return
		}
		userID = userIDParams
	}

	sql1 := fmt.Sprintf(G.FINDORDERSQL, i, userID)
	order, errF := model.Model.FindOrder(sql1)
	if isBadRequest(c, errF) {
		return
	}

	sql2 := fmt.Sprintf(G.GETUSERBYIDSQL, order.EmployeeID)
	employee, err := model.Model.GetUser(sql2)
	if isServerError(c, err) {
		return
	}

	sql3 := fmt.Sprintf(G.GETUSERBYIDSQL, order.CustomerID)
	customer, err := model.Model.GetUser(sql3)
	if isServerError(c, err) {
		return
	}

	sql4 := fmt.Sprintf(G.FINDBIDDINGSQL, order.BiddingID)
	bidding, err := model.Model.FindBidding(sql4)
	if isServerError(c, err) {
		return
	}

	res := GETOrder{
		ID:             order.ID,
		Employee:       *employee,
		Customer:       *customer,
		Bidding:        *bidding,
		OrderDate:      order.OrderDate,
		ShippedDate:    order.ShippedDate,
		ShipName:       order.ShipName,
		ShipAddress1:   order.ShipAddress1,
		ShipAddress2:   order.ShipAddress2,
		ShipCity:       order.ShipCity,
		ShipState:      order.ShipState,
		ShipPostalCode: order.ShipPostalCode,
		ShipCountry:    order.ShipCountry,
		ShippingFee:    order.ShippingFee,
		PaidDate:       order.PaidDate,
		PaymentTypeID:  order.PaymentTypeID,
		OrderStatus:    order.OrderStatus,
		TypeOrder:      order.TypeOrder,
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, res)
	c.JSON(status, response)
}

// PATCH /orders/:id
// Update a order
func UpdateOrder(c *gin.Context) {
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	// Get model if exist
	var input UpdateOrderInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.Order
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		errors := []string{}
		for _, value := range e {
			errors = append(errors, value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	userID := auth.UserID

	order := model.Order{
		ID:             input.ID,
		EmployeeID:     input.EmployeeID,
		CustomerID:     input.CustomerID,
		BiddingID:      input.BiddingID,
		OrderDate:      input.OrderDate,
		ShippedDate:    input.ShippedDate,
		ShipName:       input.ShipName,
		ShipAddress1:   input.ShipAddress1,
		ShipAddress2:   input.ShipAddress2,
		ShipCity:       input.ShipCity,
		ShipState:      input.ShipState,
		ShipPostalCode: input.ShipPostalCode,
		ShipCountry:    input.ShipCountry,
		ShippingFee:    input.ShippingFee,
		PaidDate:       input.PaidDate,
		PaymentTypeID:  input.PaymentTypeID,
		OrderStatus:    input.OrderStatus,
		TypeOrder:      input.TypeOrder,
	}

	sql := fmt.Sprintf(G.UPDATEORDERSQL,
		order.EmployeeID,
		order.CustomerID,
		order.BiddingID,
		order.OrderDate,
		order.ShippedDate,
		order.ShipName,
		order.ShipAddress1,
		order.ShipAddress2,
		order.ShipCity,
		order.ShipState,
		order.ShipPostalCode,
		order.ShipCountry,
		order.ShippingFee,
		order.PaidDate,
		order.PaymentTypeID,
		order.OrderStatus,
		order.TypeOrder,
		order.ID,
	)

	data, err := model.Model.UpdateOrder(userID, &order, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, data)
	c.JSON(status, response)
}

// DELETE /orders/:id
// Delete a order
func DeleteOrder(c *gin.Context) {
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.PostForm("id"))
	if isBadRequestID(c, errID) {
		return
	}

	u, _ := strconv.Atoi(c.PostForm("user_id"))
	userIDParams := uint(u)
	userID := auth.UserID
	if userIDParams > 0 && userIDParams != auth.UserID {
		if !isPermission(c, auth) {
			return
		}
		userID = userIDParams
	}

	sql := fmt.Sprintf(G.DELETEORDERSQL, i, userID)
	success, errF := model.Model.DeleteOrder(userID, uint(i), sql)
	if isBadRequest(c, errF) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, success)
	c.JSON(status, response)
}
