package controllers

import (
	G "Bidding_go/global"
	"Bidding_go/model"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

type CreateOrderDetailInput struct {
	OrderID       uint      `json:"order_id" form:"order_id"`
	ProductID     uint      `json:"product_id" form:"product_id"`
	Quantity      int       `json:"quantity" form:"quantity"`
	UnitPrice     float64   `json:"unit_price" form:"unit_price"`
	Discount      float32   `json:"discount" form:"discount"`
	Status        int       `json:"status" form:"status"`
	DateAllocated time.Time `json:"date_allocated" form:"date_allocated"`
}

type UpdateOrderDetailInput struct {
	ID            uint      `json:"id" form:"id" gorm:"primary_key"`
	OrderID       uint      `json:"order_id" form:"order_id"`
	ProductID     uint      `json:"product_id" form:"product_id"`
	Quantity      int       `json:"quantity" form:"quantity"`
	UnitPrice     float64   `json:"unit_price" form:"unit_price"`
	Discount      float32   `json:"discount" form:"discount"`
	Status        int       `json:"status" form:"status"`
	DateAllocated time.Time `json:"date_allocated" form:"date_allocated"`
}

type GETOrderDetail struct {
	ID            uint          `json:"id" form:"id" gorm:"primary_key"`
	Order         model.Order   `json:"order" form:"order"`
	Product       model.Product `json:"product" form:"product"`
	Quantity      int           `json:"quantity" form:"quantity"`
	UnitPrice     float64       `json:"unit_price" form:"unit_price"`
	Discount      float32       `json:"discount" form:"discount"`
	Status        int           `json:"status" form:"status"`
	DateAllocated time.Time     `json:"date_allocated" form:"date_allocated"`
}

// GET /orderdetails
// Get all orderdetails
func ListOrderDetails(c *gin.Context) {
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	u, _ := strconv.Atoi(c.Query("user_id"))
	userIDParams := uint(u)
	userID := auth.UserID
	if userIDParams > 0 && userIDParams != auth.UserID {
		if !isPermission(c, auth) {
			return
		}
		userID = userIDParams
	}

	page, _ := strconv.Atoi(c.Query("page"))
	num, _ := strconv.Atoi(c.Query("page_num"))

	sql := fmt.Sprintf(G.LISTORDERDETAILSQL, (page-1)*num, num)
	orderdetails, err := model.Model.ListOrderDetail(sql)
	if err != nil {
		status := http.StatusInternalServerError
		response := model.CreateResponse(status, err, nil)
		c.JSON(status, response)
		return
	}

	var getOrderDetails []GETOrderDetail
	for _, item := range orderdetails {
		sql1 := fmt.Sprintf(G.FINDORDERSQL, item.OrderID, userID)
		order, err := model.Model.FindOrder(sql1)
		if isServerError(c, err) {
			return
		}

		sql2 := fmt.Sprintf(G.FINDPRODUCTSQL, item.ProductID)
		product, err := model.Model.FindProduct(sql2)
		if isServerError(c, err) {
			return
		}

		i := GETOrderDetail{
			ID:            item.ID,
			Order:         *order,
			Product:       *product,
			Quantity:      item.Quantity,
			UnitPrice:     item.UnitPrice,
			Discount:      item.Discount,
			Status:        item.Status,
			DateAllocated: item.DateAllocated,
		}

		getOrderDetails = append(getOrderDetails, i)
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, getOrderDetails)
	c.JSON(status, response)
}

// POST /orderdetails
// Create new orderdetail
func CreateOrderDetail(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	var input CreateOrderDetailInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.OrderDetail
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		fmt.Println(err)
		errors := []string{}
		for key, value := range e {
			errors = append(errors, key+" : "+value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	// Create orderdetail
	orderdetail := model.OrderDetail{
		OrderID:       input.OrderID,
		ProductID:     input.ProductID,
		Quantity:      input.Quantity,
		UnitPrice:     input.UnitPrice,
		Discount:      input.Discount,
		Status:        input.Status,
		DateAllocated: input.DateAllocated,
	}

	sql := fmt.Sprintf(G.CREATEORDERDETAILSQL,
		orderdetail.OrderID,
		orderdetail.ProductID,
		orderdetail.Quantity,
		orderdetail.UnitPrice,
		orderdetail.Discount,
		orderdetail.Status,
		orderdetail.DateAllocated,
	)
	data, err := model.Model.CreateOrderDetail(&orderdetail, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, data)
	c.JSON(status, response)
}

// GET /orderdetails/:id
// Find a orderdetail
func FindOrderDetail(c *gin.Context) { // Get model if exist
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.Query("id"))
	if isBadRequestID(c, errID) {
		return
	}

	u, _ := strconv.Atoi(c.Query("user_id"))
	userIDParams := uint(u)
	userID := auth.UserID
	if userIDParams > 0 && userIDParams != auth.UserID {
		if !isPermission(c, auth) {
			return
		}
		userID = userIDParams
	}

	sql1 := fmt.Sprintf(G.FINDORDERDETAILSQL, i)
	orderdetail, errF := model.Model.FindOrderDetail(sql1)
	if isBadRequest(c, errF) {
		return
	}

	sql2 := fmt.Sprintf(G.FINDORDERSQL, i, userID)
	order, errF := model.Model.FindOrder(sql2)
	if isBadRequest(c, errF) {
		return
	}

	sql3 := fmt.Sprintf(G.FINDPRODUCTSQL, i)
	product, errF := model.Model.FindProduct(sql3)
	if isBadRequest(c, errF) {
		return
	}

	res := GETOrderDetail{
		ID:            orderdetail.ID,
		Order:         *order,
		Product:       *product,
		Quantity:      orderdetail.Quantity,
		UnitPrice:     orderdetail.UnitPrice,
		Discount:      orderdetail.Discount,
		Status:        orderdetail.Status,
		DateAllocated: orderdetail.DateAllocated,
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, res)
	c.JSON(status, response)
}

// PATCH /orderdetails/:id
// Update a orderdetail
func UpdateOrderDetail(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	// Get model if exist
	var input UpdateOrderDetailInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.OrderDetail
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		errors := []string{}
		for _, value := range e {
			errors = append(errors, value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	orderdetail := model.OrderDetail{
		ID:            input.ID,
		OrderID:       input.OrderID,
		ProductID:     input.ProductID,
		Quantity:      input.Quantity,
		UnitPrice:     input.UnitPrice,
		Discount:      input.Discount,
		Status:        input.Status,
		DateAllocated: input.DateAllocated,
	}

	sql := fmt.Sprintf(G.UPDATEORDERDETAILSQL,
		orderdetail.OrderID,
		orderdetail.ProductID,
		orderdetail.Quantity,
		orderdetail.UnitPrice,
		orderdetail.Discount,
		orderdetail.Status,
		orderdetail.DateAllocated,
		orderdetail.ID,
	)

	data, err := model.Model.UpdateOrderDetail(&orderdetail, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, data)
	c.JSON(status, response)
}

// DELETE /orderdetails/:id
// Delete a orderdetail
func DeleteOrderDetail(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.PostForm("id"))
	if isBadRequestID(c, errID) {
		return
	}

	sql := fmt.Sprintf(G.DELETEORDERDETAILSQL, i)
	success, errF := model.Model.DeleteOrderDetail(uint(i), sql)
	if isBadRequest(c, errF) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, success)
	c.JSON(status, response)
}
