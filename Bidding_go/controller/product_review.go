package controllers

import (
	G "Bidding_go/global"
	"Bidding_go/model"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type CreateProductReviewInput struct {
	ProductID uint        `json:"product_id" form:"product_id"`
	UserID    uint        `json:"user_id" form:"user_id"`
	Rating    json.Number `json:"rating" form:"rating"`
	Comment   string      `json:"comment" form:"comment"`
}

type UpdateProductReviewInput struct {
	ID        uint        `json:"id" form:"id" gorm:"primary_key"`
	ProductID uint        `json:"product_id" form:"product_id"`
	UserID    uint        `json:"user_id" form:"user_id"`
	Rating    json.Number `json:"rating" form:"rating"`
	Comment   string      `json:"comment" form:"comment"`
}

type GETProductReview struct {
	ID      uint          `json:"id" form:"id" gorm:"primary_key"`
	User    model.User    `json:"user" form:"user"`
	Product model.Product `json:"product" form:"product"`
	Rating  json.Number   `json:"rating" form:"rating"`
	Comment string        `json:"comment" form:"comment"`
}

// GET /productreviews
// Get all productreviews
func ListProductReviews(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	page, _ := strconv.Atoi(c.Query("page"))
	num, _ := strconv.Atoi(c.Query("page_num"))

	sql := fmt.Sprintf(G.LISTPRODUCTREVIEWSQL, (page-1)*num, num)
	productreviews, err := model.Model.ListProductReview(sql)
	if isServerError(c, err) {
		return
	}

	var getProductReviews []GETProductReview
	for _, item := range productreviews {
		sql1 := fmt.Sprintf(G.GETUSERBYIDSQL, item.UserID)
		user, err := model.Model.GetUser(sql1)
		if err != nil {
			continue
		}

		sql2 := fmt.Sprintf(G.FINDPRODUCTSQL, item.ProductID)
		product, err := model.Model.FindProduct(sql2)
		if err != nil {
			continue
		}

		i := GETProductReview{
			ID:      item.ID,
			User:    *user,
			Product: *product,
			Rating:  item.Rating,
			Comment: item.Comment,
		}

		getProductReviews = append(getProductReviews, i)
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, getProductReviews)
	c.JSON(status, response)
}

// POST /productreviews
// Create new productreview
func CreateProductReview(c *gin.Context) {
	auth, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	u, _ := strconv.Atoi(c.PostForm("user_id"))
	userIDParams := uint(u)
	userID := auth.UserID
	if userIDParams > 0 && userIDParams != auth.UserID {
		if !isPermission(c, auth) {
			return
		}
		userID = userIDParams
		sql := fmt.Sprintf(G.GETUSERBYIDSQL, userID)
		_, errF := model.Model.GetUser(sql)
		if isBadRequest(c, errF) {
			return
		}
	}

	var input CreateProductReviewInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.ProductReview
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		fmt.Println(err)
		errors := []string{}
		for key, value := range e {
			errors = append(errors, key+" : "+value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	// Create productreview
	productreview := model.ProductReview{
		ProductID: input.ProductID,
		UserID:    userID,
		Rating:    input.Rating,
		Comment:   input.Comment,
	}

	sql1 := fmt.Sprintf(G.FINDPRODUCTSQL, input.ProductID)
	_, errF := model.Model.FindProduct(sql1)
	if isBadRequest(c, errF) {
		return
	}

	sql2 := fmt.Sprintf(G.CREATEPRODUCTREVIEWSQL,
		productreview.ProductID,
		productreview.UserID,
		productreview.Rating,
		productreview.Comment,
	)

	data, err := model.Model.CreateProductReview(&productreview, sql2)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, data)
	c.JSON(status, response)
}

// GET /productreviews/:id
// Find a productreview
func FindProductReview(c *gin.Context) { // Get model if exist
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.Query("id"))
	if isBadRequestID(c, errID) {
		return
	}

	sql1 := fmt.Sprintf(G.FINDPRODUCTREVIEWSQL, i)
	productreview, errF := model.Model.FindProductReview(sql1)
	if isBadRequest(c, errF) {
		return
	}

	sql2 := fmt.Sprintf(G.GETUSERBYIDSQL, productreview.UserID)
	user, errF := model.Model.GetUser(sql2)
	if isBadRequest(c, errF) {
		return
	}

	sql3 := fmt.Sprintf(G.FINDPRODUCTSQL, productreview.ProductID)
	product, errF := model.Model.FindProduct(sql3)
	if isBadRequest(c, errF) {
		return
	}

	res := GETProductReview{
		ID:      productreview.ID,
		User:    *user,
		Product: *product,
		Rating:  productreview.Rating,
		Comment: productreview.Comment,
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, res)
	c.JSON(status, response)
}

// PATCH /productreviews/:id
// Update a productreview
func UpdateProductReview(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	// Get model if exist
	var input UpdateProductReviewInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.ProductReview
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		errors := []string{}
		for _, value := range e {
			errors = append(errors, value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	productreview := model.ProductReview{
		ID:        input.ID,
		ProductID: input.ProductID,
		UserID:    input.UserID,
		Rating:    input.Rating,
		Comment:   input.Comment,
	}

	sql := fmt.Sprintf(G.UPDATEPRODUCTREVIEWSQL,
		productreview.ProductID,
		productreview.UserID,
		productreview.Rating,
		productreview.Comment,
		productreview.ID,
	)

	data, err := model.Model.UpdateProductReview(&productreview, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, data)
	c.JSON(status, response)
}

// DELETE /productreviews/:id
// Delete a productreview
func DeleteProductReview(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.PostForm("id"))
	if isBadRequestID(c, errID) {
		return
	}

	sql := fmt.Sprintf(G.DELETEBIDDINGHISTORYSQL, i)
	success, errF := model.Model.DeleteProductReview(uint(i), sql)
	if isBadRequest(c, errF) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, success)
	c.JSON(status, response)
}
