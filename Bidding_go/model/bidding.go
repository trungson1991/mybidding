package model

import (
	"fmt"
	"time"

	G "Bidding_go/global"

	"gorm.io/gorm"
)

type Bidding struct {
	ID                  uint           `json:"id" form:"id" gorm:"primary_key"`
	ProductID           uint           `json:"product_id" form:"product_id"`
	BiddingCode         string         `json:"bidding_code" form:"bidding_code"`
	BiddingUserWinnerID uint           `json:"bidding_user_winner_id" form:"bidding_user_winner_id"`
	BiddingTitle        string         `json:"bidding_title" form:"bidding_title"`
	Description         string         `json:"description" form:"description"`
	ShortDescription    string         `json:"short_description" form:"short_description"`
	StartTime           time.Time      `json:"start_time" form:"start_time"`
	StopTime            time.Time      `json:"stop_time" form:"stop_time"`
	NotifTime           time.Time      `json:"notif_time" form:"notif_time"`
	Status              int            `json:"status" form:"status"`
	CreatedAt           time.Time      `json:"create_time" form:"create_time"`
	UpdatedAt           time.Time      `json:"update_time" form:"update_time"`
	DeletedAt           gorm.DeletedAt `json:"delete_time" form:"delete_time"`
}

func ValidateBidding(object *Bidding) []string {
	strErrs := []string{}

	return strErrs
}

func (s *Server) ListBidding(sql string) ([]Bidding, []string) {
	strErrs := []string{}
	var items []Bidding
	err := s.DB.Debug().Raw(sql).Find(&items).Error

	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}

	return items, nil
}

func (s *Server) CreateBidding(object *Bidding, sql string) (*Bidding, []string) {
	strErrs := []string{}

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Create(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("CreatedAt", time.Now())
	return object, nil
}

func (s *Server) FindBidding(sql string) (*Bidding, []string) {
	strErrs := []string{}
	var item *Bidding
	if err := s.DB.Debug().Raw(sql).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	return item, nil
}

func (s *Server) UpdateBidding(object *Bidding, sql string) (*Bidding, []string) {
	strErrs := []string{}
	var item Bidding
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHBIDDINGSQL,
		object.ID,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	e := ValidateBidding(object)
	strErrs = append(strErrs, e...)

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Scan(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("UpdatedAt", time.Now())
	return object, nil
}

func (s *Server) DeleteBidding(id uint, sql string) (bool, []string) {
	var item *Bidding
	strErrs := []string{}
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHBIDDINGSQL,
		id,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return false, strErrs
	}

	err := s.DB.Debug().Delete(&item).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return false, strErrs
	}

	return true, nil
}
