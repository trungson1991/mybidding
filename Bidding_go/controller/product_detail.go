package controllers

import (
	G "Bidding_go/global"
	"Bidding_go/model"
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type CreateProductDetailInput struct {
	ProductID  uint   `json:"product_id" form:"product_id"`
	ShopID     uint   `json:"shop_id" form:"shop_id"`
	EmployeeID uint   `json:"employee_id" form:"employee_id"`
	ImportDate string `json:"import_date" form:"import_date"`
	Quantity   string `json:"quantity" form:"quantity"`
	UnitPrice  string `json:"unit_price" form:"unit_price"`
	Type       string `json:"type" form:"type"`
}

type UpdateProductDetailInput struct {
	ID         uint   `json:"id" form:"id" gorm:"primary_key"`
	ProductID  uint   `json:"product_id" form:"product_id"`
	ShopID     uint   `json:"shop_id" form:"shop_id"`
	EmployeeID uint   `json:"employee_id" form:"employee_id"`
	ImportDate string `json:"import_date" form:"import_date"`
	Quantity   string `json:"quantity" form:"quantity"`
	UnitPrice  string `json:"unit_price" form:"unit_price"`
	Type       string `json:"type" form:"type"`
}

type GETProductDetail struct {
	ID         uint          `json:"id" form:"id" gorm:"primary_key"`
	Product    model.Product `json:"product" form:"product"`
	Shop       model.Shop    `json:"shop" form:"shop"`
	Employee   model.User    `json:"employee" form:"employee"`
	ImportDate string        `json:"import_date" form:"import_date"`
	Quantity   string        `json:"quantity" form:"quantity"`
	UnitPrice  string        `json:"unit_price" form:"unit_price"`
	Type       string        `json:"type" form:"type"`
}

// GET /productdetails
// Get all productdetails
func ListProductDetails(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	page, _ := strconv.Atoi(c.Query("page"))
	num, _ := strconv.Atoi(c.Query("page_num"))

	sql := fmt.Sprintf(G.LISTPRODUCTDETAILSQL, (page-1)*num, num)
	productdetails, err := model.Model.ListProductDetail(sql)
	if isServerError(c, err) {
		return
	}

	var getProductDetails []GETProductDetail
	for _, item := range productdetails {
		sql1 := fmt.Sprintf(G.GETUSERBYIDSQL, item.EmployeeID)
		user, err := model.Model.GetUser(sql1)
		if isServerError(c, err) {
			return
		}

		sql2 := fmt.Sprintf(G.FINDPRODUCTSQL, item.EmployeeID)
		product, err := model.Model.FindProduct(sql2)
		if isServerError(c, err) {
			return
		}

		sql3 := fmt.Sprintf(G.FINDSHOPSQL, item.EmployeeID)
		shop, err := model.Model.FindShop(sql3)
		if isServerError(c, err) {
			return
		}

		i := GETProductDetail{
			ID:         item.ID,
			Product:    *product,
			Shop:       *shop,
			Employee:   *user,
			ImportDate: item.ImportDate,
			Quantity:   item.Quantity,
			UnitPrice:  item.UnitPrice,
			Type:       item.Type,
		}

		getProductDetails = append(getProductDetails, i)
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, getProductDetails)
	c.JSON(status, response)
}

// POST /productdetails
// Create new productdetail
func CreateProductDetail(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	var input CreateProductDetailInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.ProductDetail
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		fmt.Println(err)
		errors := []string{}
		for key, value := range e {
			errors = append(errors, key+" : "+value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	// Create productdetail
	productdetail := model.ProductDetail{
		ProductID:  input.ProductID,
		ShopID:     input.ShopID,
		EmployeeID: input.EmployeeID,
		ImportDate: input.ImportDate,
		Quantity:   input.Quantity,
		UnitPrice:  input.UnitPrice,
		Type:       input.Type,
	}

	sql := fmt.Sprintf(G.CREATEPRODUCTDETAILSQL,
		productdetail.ProductID,
		productdetail.ShopID,
		productdetail.EmployeeID,
		productdetail.ImportDate,
		productdetail.Quantity,
		productdetail.UnitPrice,
		productdetail.Type,
	)

	data, err := model.Model.CreateProductDetail(&productdetail, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, data)
	c.JSON(status, response)
}

// GET /productdetails/:id
// Find a productdetail
func FindProductDetail(c *gin.Context) { // Get model if exist
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.Query("id"))
	if isBadRequestID(c, errID) {
		return
	}

	sql := fmt.Sprintf(G.FINDPRODUCTDETAILSQL, i)
	productdetail, errF := model.Model.FindProductDetail(sql)
	if isBadRequest(c, errF) {
		return
	}

	sql1 := fmt.Sprintf(G.GETUSERBYIDSQL, productdetail.EmployeeID)
	user, err := model.Model.GetUser(sql1)
	if isServerError(c, err) {
		return
	}

	sql2 := fmt.Sprintf(G.FINDPRODUCTSQL, productdetail.ProductID)
	product, err := model.Model.FindProduct(sql2)
	if isServerError(c, err) {
		return
	}

	sql3 := fmt.Sprintf(G.FINDSHOPSQL, productdetail.ShopID)
	shop, err := model.Model.FindShop(sql3)
	if isServerError(c, err) {
		return
	}

	res := GETProductDetail{
		ID:         productdetail.ID,
		Product:    *product,
		Shop:       *shop,
		Employee:   *user,
		ImportDate: productdetail.ImportDate,
		Quantity:   productdetail.Quantity,
		UnitPrice:  productdetail.UnitPrice,
		Type:       productdetail.Type,
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, res)
	c.JSON(status, response)
}

// PATCH /productdetails/:id
// Update a productdetail
func UpdateProductDetail(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	// Get model if exist
	var input UpdateProductDetailInput
	if err := c.ShouldBind(&input); err != nil {
		var p model.ProductDetail
		status := http.StatusBadRequest

		e := model.ListOfErrors(&p, err)
		errors := []string{}
		for _, value := range e {
			errors = append(errors, value)
		}
		response := model.CreateResponse(status, errors, nil)
		c.JSON(status, response)
		return
	}

	productdetail := model.ProductDetail{
		ID:         input.ID,
		ProductID:  input.ProductID,
		ShopID:     input.ShopID,
		EmployeeID: input.EmployeeID,
		ImportDate: input.ImportDate,
		Quantity:   input.Quantity,
		UnitPrice:  input.UnitPrice,
		Type:       input.Type,
	}

	sql := fmt.Sprintf(G.UPDATEPRODUCTDETAILSQL,
		productdetail.ProductID,
		productdetail.ShopID,
		productdetail.EmployeeID,
		productdetail.ImportDate,
		productdetail.Quantity,
		productdetail.UnitPrice,
		productdetail.Type,
		productdetail.ID,
	)

	data, err := model.Model.UpdateProductDetail(&productdetail, sql)
	if isServerError(c, err) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, data)
	c.JSON(status, response)
}

// DELETE /productdetails/:id
// Delete a productdetail
func DeleteProductDetail(c *gin.Context) {
	_, er := GetAuthWith(c.Request)
	if !isUnauthorized(c, er) {
		return
	}

	i, errID := strconv.Atoi(c.PostForm("id"))
	if isBadRequestID(c, errID) {
		return
	}

	sql := fmt.Sprintf(G.DELETEPRODUCTDETAILSQL, i)
	success, errF := model.Model.DeleteProductDetail(uint(i), sql)
	if isBadRequest(c, errF) {
		return
	}

	status := http.StatusOK
	response := model.CreateResponse(status, nil, success)
	c.JSON(status, response)
}
