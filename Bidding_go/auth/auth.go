package auth

import (
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"

	G "Bidding_go/global"

	"github.com/dgrijalva/jwt-go"
)

type AuthDetails struct {
	AuthUuid string
	UserId   uint
}

func CreateToken(authD AuthDetails) (string, error) {
	claims := jwt.MapClaims{}
	claims[G.Authorized] = true
	claims[G.AuthUuid] = authD.AuthUuid
	claims[G.UserID] = authD.UserId
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString([]byte(os.Getenv(G.APISecret)))
}

func TokenValid(r *http.Request) error {
	token, err := VerifyToken(r)
	if err != nil {
		return err
	}
	if _, ok := token.Claims.(jwt.Claims); !ok && !token.Valid {
		return err
	}
	return nil
}

func VerifyToken(r *http.Request) (*jwt.Token, error) {
	tokenString := ExtractToken(r)
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		//does this token conform to "SigningMethodHMAC" ?
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("%v: %v", G.UnexpectedSigningMethod, token.Header["alg"])
		}
		return []byte(os.Getenv(G.APISecret)), nil
	})
	if err != nil {
		return nil, err
	}
	return token, nil
}

func ExtractToken(r *http.Request) string {
	keys := r.URL.Query()
	token := keys.Get(G.UserToken)
	if token != "" {
		return token
	}
	bearToken := r.Header.Get(G.Authorization)
	strArr := strings.Split(bearToken, " ")
	if len(strArr) == 2 {
		return strArr[1]
	}
	return ""
}

func ExtractTokenAuth(r *http.Request) (*AuthDetails, error) {
	token, err := VerifyToken(r)
	if err != nil {
		return nil, err
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		authUuid, ok := claims[G.AuthUuid].(string)
		if !ok {
			return nil, err
		}
		userId, err := strconv.ParseUint(fmt.Sprintf("%.f", claims[G.UserID]), 10, 64)
		if err != nil {
			return nil, err
		}
		return &AuthDetails{
			AuthUuid: authUuid,
			UserId:   uint(userId),
		}, nil
	}
	return nil, err
}
