package model

import (
	"fmt"
	"time"

	G "Bidding_go/global"

	"gorm.io/gorm"
)

type Role struct {
	ID          uint           `json:"id" form:"id" gorm:"primary_key"`
	Name        string         `json:"name" form:"name"`
	DisplayName string         `json:"display_name" form:"display_name"`
	CreatedAt   time.Time      `json:"create_time" form:"create_time"`
	UpdatedAt   time.Time      `json:"update_time" form:"update_time"`
	DeletedAt   gorm.DeletedAt `json:"delete_time" form:"delete_time"`
}

func ValidateRole(object *Role) []string {
	strErrs := []string{}

	return strErrs
}

func (s *Server) ListRole(sql string) ([]Role, []string) {
	strErrs := []string{}
	var items []Role
	err := s.DB.Debug().Raw(sql).Find(&items).Error

	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}

	return items, nil
}

func (s *Server) CreateRole(object *Role, sql string) (*Role, []string) {
	strErrs := []string{}

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Create(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("CreatedAt", time.Now())
	return object, nil
}

func (s *Server) FindRole(sql string) (*Role, []string) {
	strErrs := []string{}
	var item *Role
	if err := s.DB.Debug().Raw(sql).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	return item, nil
}

func (s *Server) UpdateRole(object *Role, sql string) (*Role, []string) {
	strErrs := []string{}
	var item Role
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHROLESQL,
		object.ID,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return nil, strErrs
	}

	e := ValidateRole(object)
	strErrs = append(strErrs, e...)

	num := len(strErrs)
	if num > 0 {
		return nil, strErrs
	}

	err := s.DB.Debug().Raw(sql).Scan(&object).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return nil, strErrs
	}
	s.DB.Model(&object).UpdateColumn("UpdatedAt", time.Now())
	return &item, nil
}

func (s *Server) DeleteRole(id uint, sql string) (bool, []string) {
	var item *Role
	strErrs := []string{}
	if err := s.DB.Debug().Raw(fmt.Sprintf(G.SEARCHROLESQL,
		id,
	)).First(&item).Error; err != nil {
		strErrs = append(strErrs, G.RecordNotFound)
		return false, strErrs
	}

	err := s.DB.Debug().Delete(&item).Error
	if err != nil {
		strErrs = append(strErrs, err.Error())
		return false, strErrs
	}

	return true, nil
}
