package model

import (
	"Bidding_go/auth"
	"database/sql"
	"fmt"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

type Server struct {
	DB *gorm.DB
	modelInterface
}

var (
	Model modelInterface = &Server{}
)

type modelInterface interface {
	Initialize(Dbdriver, DbUser, DbPassword, DbPort, DbHost, DbName string) (*gorm.DB, error)

	// Auth
	FetchAuth(*auth.AuthDetails) (*Auth, error)
	DeleteAuth(*auth.AuthDetails) error
	CreateAuth(userId uint, deviceToken string) (*Auth, error)
	UpdateAuth(authD *auth.AuthDetails, deviceToken string) (*Auth, error)
	GetAllDeviceToken() (*[]Auth, error)
	GetUserWith(userToken string) (*User, error)

	// User
	ListUsers(sql string) ([]User, []string)
	CreateUser(*User, string) (*User, []string)
	UpdateUser(user *User, userToken string, sql string) (*User, []string)
	GetUserInfo(userToken string) (*User, []string)
	GetUser(sql string) (*User, []string)

	// Role
	ListRole(sql string) ([]Role, []string)
	CreateRole(object *Role, sql string) (*Role, []string)
	FindRole(sql string) (*Role, []string)
	UpdateRole(object *Role, sql string) (*Role, []string)
	DeleteRole(id uint, sql string) (bool, []string)

	// Bidding
	ListBidding(sql string) ([]Bidding, []string)
	CreateBidding(object *Bidding, sql string) (*Bidding, []string)
	FindBidding(sql string) (*Bidding, []string)
	UpdateBidding(object *Bidding, sql string) (*Bidding, []string)
	DeleteBidding(id uint, sql string) (bool, []string)

	// Bidding History
	ListBiddingHistory(sql string) ([]BiddingHistory, []string)
	CreateBiddingHistory(object *BiddingHistory, sql string) (*BiddingHistory, []string)
	FindBiddingHistory(sql string) (*BiddingHistory, []string)
	UpdateBiddingHistory(userID uint, object *BiddingHistory, sql string) (*BiddingHistory, []string)
	DeleteBiddingHistory(userID uint, id uint, sql string) (bool, []string)

	// Bidding User
	ListBiddingUser(sql string) ([]BiddingUser, []string)
	CreateBiddingUser(object *BiddingUser, sql string) (*BiddingUser, []string)
	FindBiddingUser(sql string) (*BiddingUser, []string)
	UpdateBiddingUser(userID uint, object *BiddingUser, sql string) (*BiddingUser, []string)
	DeleteBiddingUser(userID uint, id uint, sql string) (bool, []string)

	// Category
	ListCategory(sql string) ([]Category, []string)
	CreateCategory(object *Category, sql string) (*Category, []string)
	FindCategory(sql string) (*Category, []string)
	UpdateCategory(object *Category, sql string) (*Category, []string)
	DeleteCategory(id uint, sql string) (bool, []string)

	// Order
	ListOrder(sql string) ([]Order, []string)
	CreateOrder(object *Order, sql string) (*Order, []string)
	FindOrder(sql string) (*Order, []string)
	UpdateOrder(userID uint, object *Order, sql string) (*Order, []string)
	DeleteOrder(userID uint, id uint, sql string) (bool, []string)

	// Order Detail
	ListOrderDetail(sql string) ([]OrderDetail, []string)
	CreateOrderDetail(object *OrderDetail, sql string) (*OrderDetail, []string)
	FindOrderDetail(sql string) (*OrderDetail, []string)
	UpdateOrderDetail(object *OrderDetail, sql string) (*OrderDetail, []string)
	DeleteOrderDetail(id uint, sql string) (bool, []string)

	// Payment type
	ListPaymentType(sql string) ([]PaymentType, []string)
	CreatePaymentType(object *PaymentType, sql string) (*PaymentType, []string)
	FindPaymentType(sql string) (*PaymentType, []string)
	UpdatePaymentType(object *PaymentType, sql string) (*PaymentType, []string)
	DeletePaymentType(id uint, sql string) (bool, []string)

	// ProductReview
	ListProductReview(sql string) ([]ProductReview, []string)
	CreateProductReview(object *ProductReview, sql string) (*ProductReview, []string)
	FindProductReview(sql string) (*ProductReview, []string)
	UpdateProductReview(object *ProductReview, sql string) (*ProductReview, []string)
	DeleteProductReview(id uint, sql string) (bool, []string)

	// Product
	ListProduct(sql string) ([]Product, []string)
	CreateProduct(object *Product, sql string) (*Product, []string)
	FindProduct(sql string) (*Product, []string)
	UpdateProduct(object *Product, sql string) (*Product, []string)
	DeleteProduct(id uint, sql string) (bool, []string)

	// Product Detail
	ListProductDetail(sql string) ([]ProductDetail, []string)
	CreateProductDetail(object *ProductDetail, sql string) (*ProductDetail, []string)
	FindProductDetail(sql string) (*ProductDetail, []string)
	UpdateProductDetail(object *ProductDetail, sql string) (*ProductDetail, []string)
	DeleteProductDetail(id uint, sql string) (bool, []string)

	// Shop
	ListShop(sql string) ([]Shop, []string)
	CreateShop(object *Shop, sql string) (*Shop, []string)
	FindShop(sql string) (*Shop, []string)
	UpdateShop(object *Shop, sql string) (*Shop, []string)
	DeleteShop(id uint, sql string) (bool, []string)

	// Supplier
	ListSupplier(sql string) ([]Supplier, []string)
	CreateSupplier(object *Supplier, sql string) (*Supplier, []string)
	FindSupplier(sql string) (*Supplier, []string)
	UpdateSupplier(object *Supplier, sql string) (*Supplier, []string)
	DeleteSupplier(id uint, sql string) (bool, []string)

	// UserFavorite
	ListUserFavorite(sql string) ([]UserFavorite, []string)
	CreateUserFavorite(object *UserFavorite, sql string) (*UserFavorite, []string)
	FindUserFavorite(sql string) (*UserFavorite, []string)
	UpdateUserFavorite(userID uint, object *UserFavorite, sql string) (*UserFavorite, []string)
	DeleteUserFavorite(userID uint, id uint, sql string) (bool, []string)

	// User Role
	ListUserRole(sql string) ([]UserRole, []string)
	CreateUserRole(object *UserRole, sql string) (*UserRole, []string)
	FindUserRole(sql string) (*UserRole, []string)
	UpdateUserRole(userID uint, object *UserRole, sql string) (*UserRole, []string)
	DeleteUserRole(userID uint, id uint, sql string) (bool, []string)
}

func (s *Server) Initialize(Dbdriver, DbUser, DbPassword, DbPort, DbHost, DbName string) (*gorm.DB, error) {
	var err error
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", DbUser, DbPassword, DbHost, DbPort, DbName)
	sqlDB, err := sql.Open("mysql", dsn)

	if err != nil {
		return nil, err
	}

	database, err := gorm.Open(mysql.New(mysql.Config{
		Conn: sqlDB,
	}), &gorm.Config{})

	if err != nil {
		return nil, err
	}

	database.Debug().AutoMigrate(
		&Auth{},
		&User{},
		&UserRole{},
		&UserFavorite{},
		&Supplier{},
		&Shop{},
		&Role{},
		&Product{},
		&ProductReview{},
		&ProductDetail{},
		&PaymentType{},
		&Order{},
		&OrderDetail{},
		&Category{},
		&Bidding{},
		&BiddingUser{},
		&BiddingHistory{},
	)

	s.DB = database
	return s.DB, nil
}
